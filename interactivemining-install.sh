rm -f interactiveminingv3.tgz
git clone https://code-repo.d4science.org/MaDgIK/interactive-mining.git
cd interactive-mining/
git checkout angular-16
cd interactive-mining-angular-frontend/
npm install
npm run packagr
cp -r ./src/assets/ ./dist
cd dist/
npm pack
mv interactiveminingv3-1.0.0.tgz ../../../interactiveminingv3.tgz
cd ../../../
rm -rf interactive-mining/
npm install --no-save ./interactiveminingv3.tgz
