import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {AppComponent} from './app.component';
import {AppRoutingModule} from './app.routing';
import {ReactiveFormsModule} from '@angular/forms';
import {NavigationBarModule} from './openaireLibrary/sharedComponents/navigationBar.module';
import {BottomModule} from './openaireLibrary/sharedComponents/bottom.module';
import {ConnectAdminLoginGuard} from './openaireLibrary/connect/communityGuard/connectAdminLoginGuard.guard';
import {IsCommunity} from './openaireLibrary/connect/communityGuard/isCommunity.guard';
import {AdminErrorPageComponent} from './pages/error/errorPage.component';
import {ErrorModule} from './openaireLibrary/error/error.module';
import {TitleCasePipe} from '@angular/common';
import {SubscribeService} from './openaireLibrary/utils/subscribe/subscribe.service';
import {ConnectRIGuard} from './openaireLibrary/connect/communityGuard/connectRIGuard.guard';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {LoadingModule} from "./openaireLibrary/utils/loading/loading.module";
import {SideBarModule} from "./openaireLibrary/dashboard/sharedComponents/sidebar/sideBar.module";
import {SharedModule} from "./openaireLibrary/shared/shared.module";
import {ErrorInterceptorService} from "./openaireLibrary/error-interceptor.service";
import {DEFAULT_TIMEOUT, TimeoutInterceptor} from "./openaireLibrary/timeout-interceptor.service";
import {AdminLoginGuard} from "./openaireLibrary/login/adminLoginGuard.guard";
import {LoginGuard} from "./openaireLibrary/login/loginGuard.guard";
import {InteractiveMiningModule} from 'interactiveminingv3';

@NgModule({
  imports: [
    AppRoutingModule,
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule,
    BottomModule, NavigationBarModule,
    ErrorModule,
    SharedModule,
    InteractiveMiningModule,
    BrowserAnimationsModule, LoadingModule, SideBarModule
  ],
  declarations: [
    AppComponent,
    AdminErrorPageComponent
  ],
  providers: [
    SubscribeService,
    ConnectAdminLoginGuard, AdminLoginGuard, LoginGuard,
    IsCommunity, ConnectRIGuard, TitleCasePipe,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorInterceptorService,
      multi: true
    },
    [{provide: HTTP_INTERCEPTORS, useClass: TimeoutInterceptor, multi: true}],
    [{provide: DEFAULT_TIMEOUT, useValue: 30000}]
  ],
  bootstrap: [AppComponent]
})

export class AppModule {
}
