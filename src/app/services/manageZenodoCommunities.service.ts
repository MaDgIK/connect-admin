import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import{EnvProperties} from '../openaireLibrary/utils/properties/env-properties';

@Injectable()
export class  ManageZenodoCommunitiesService {
  constructor(private http: HttpClient ) {}
 removeZenodoCommunity(properties:EnvProperties, pid: string,zenodoid: string, main:boolean = false) {

    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    };
   let url = properties.communityAPI+pid+"/zenodocommunities?zenodocommunity="+zenodoid + "&main=" + main;
    return this.http.delete<any>(url,  options);
  }
  addZenodoCommunity(properties:EnvProperties, pid: string,zenodoid: string, main:boolean = false) {

    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    };
    let url = properties.communityAPI+pid+"/zenodocommunities?zenodocommunity="+zenodoid + "&main=" + main;
    return this.http.post<any>(url,  options);
  }

}
