import {Injectable} from '@angular/core';
import {EnvProperties} from '../openaireLibrary/utils/properties/env-properties';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable()
export class ManageCommunityProjectsService {
  constructor(private http: HttpClient ) {}

  removeProject (properties:EnvProperties, pid: string, id: string):any {
    //let headers = new Headers({'Content-Type': 'application/json', 'accept': 'application/json'});
    //let options = new RequestOptions({headers: headers, body: id});

    let headers = new HttpHeaders({'Content-Type': 'application/json', 'accept': 'application/json'});

    let url = properties.communityAPI+pid+"/projects?projectId="+id;
    //return this.http.delete(url, options)
    return this.http.request('delete', url, {  headers: headers})
  }

  addProject(properties:EnvProperties, pid: string, project: any) {
      //let headers = new Headers({'Content-Type': 'application/json'});
      //let options = new RequestOptions({headers: headers});

    let headers = new HttpHeaders({'Content-Type': 'application/json'});

    let url = properties.communityAPI+pid+"/projects";

      let communityProject = this.convertSearchProjectToCommunityProject(project, pid);
      let testProject: any = {
        "acronym": "test",
        "communityId": "egi",
        "funder": "test",
        "grantId": "test",
        "name": "test",
        "openaireId": "test"
      };

      return this.http.post<any>(url, JSON.stringify(communityProject), {headers: headers});
      //return this.http.post(url, JSON.stringify(communityProject), options)
                      //.map(res => <any> res.json())
    }

  convertSearchProjectToCommunityProject(project: any, community: string) : any {
    let communityProject = {
            "acronym": "",
            "communityId": community,
            "funder": "",
            "grantId": "",
            "name": "",
            "openaireId": ""
    }

    communityProject.acronym = project.acronym;
    communityProject.name = project.title.name;
    communityProject.funder = project.funderShortname;
    communityProject.grantId = project.code;
    communityProject.openaireId = project.id;

    return communityProject;
  }

}
