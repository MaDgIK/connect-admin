import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {EnvProperties} from '../openaireLibrary/utils/properties/env-properties';
import {ContentProvider} from '../openaireLibrary/utils/entities/contentProvider';
import {Observable} from 'rxjs';

@Injectable()
export class ManageCommunityContentProvidersService {
  constructor(private http: HttpClient) {
  }

  removeContentProvider(properties: EnvProperties, pid: string, id: string): any {
    let headers = new HttpHeaders({'Content-Type': 'application/json', 'accept': 'application/json'});
    let url = properties.communityAPI + pid + '/datasources?dsId=' +id;
    return this.http.request('delete', url, {body: id, headers: headers});
  }

  addContentProvider(properties: EnvProperties, pid: string, contentProvider: any, content:boolean, deposit:boolean): Observable<ContentProvider> {
    let url = properties.communityAPI + pid + '/datasources';
    let communityContentProvider = this.convertSearchContentProviderToCommunityContentProvider(contentProvider, pid, content, deposit);
    return this.http.post<ContentProvider>(url, communityContentProvider);
  }

  saveContentProvider(properties: EnvProperties, contentProvider: ContentProvider): Observable<ContentProvider> {
    let url = properties.communityAPI + contentProvider.communityId + '/datasources';
    return this.http.post<ContentProvider>(url, contentProvider);
  }

  convertSearchContentProviderToCommunityContentProvider(contentProvider: any, community: string, content:boolean, deposit:boolean): ContentProvider {
    let communityContentProvider: ContentProvider = new ContentProvider();
    communityContentProvider.communityId = community;
    communityContentProvider.officialname = contentProvider.title.name;
    communityContentProvider.name = contentProvider.englishname;
    communityContentProvider.openaireId = contentProvider.id;
    communityContentProvider.enabled = content;
    communityContentProvider.deposit = deposit;

    return communityContentProvider;
  }

}
