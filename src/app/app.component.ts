import {ChangeDetectorRef, Component, HostListener, OnInit} from '@angular/core';
import {MenuItem} from './openaireLibrary/sharedComponents/menu';
import {ActivatedRoute, Data, NavigationEnd, Params, Router} from '@angular/router';
import {EnvProperties} from './openaireLibrary/utils/properties/env-properties';
import {Session, User} from './openaireLibrary/login/utils/helper.class';
import {UserManagementService} from './openaireLibrary/services/user-management.service';
import {Header} from './openaireLibrary/sharedComponents/navigationBar.component';
import {LayoutService} from "./openaireLibrary/dashboard/sharedComponents/sidebar/layout.service";
import {properties} from "../environments/environment";
import {BehaviorSubject, Subscriber} from "rxjs";
import {CommunityInfo} from "./openaireLibrary/connect/community/communityInfo";
import {CommunityService} from "./openaireLibrary/connect/community/community.service";
import {SmoothScroll} from "./openaireLibrary/utils/smooth-scroll";
import {ConnectHelper} from './openaireLibrary/connect/connectHelper';
import {ConfigurationService} from './openaireLibrary/utils/configuration/configuration.service';
import {StringUtils} from "./openaireLibrary/utils/string-utils.class";
import {OpenaireEntities} from "./openaireLibrary/utils/properties/searchFields";
import {mining} from "./openaireLibrary/utils/icons/icons";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
})
export class AppComponent implements OnInit {
  title = 'Research Community Dashboard | Administrator';
  properties: EnvProperties = properties;
  user: User;
  params: BehaviorSubject<Params> = new BehaviorSubject<Params>(null);
  data: BehaviorSubject<Data> = new BehaviorSubject<Data>(null);
  hasSidebar: boolean = false;
  hasInternalSidebar: boolean = false;
  hasHeader: boolean = true;
  hasAdminMenu: boolean = false;
  isFrontPage: boolean = false;
  sideBarItems: MenuItem[] = [];
  backItem: MenuItem = null;
  menuItems: MenuItem[] = [];
  menuHeader: Header = {
    route: "/",
    url: null,
    title: "Default menu header",
    logoUrl: null,
    logoSmallUrl: null,
    position: 'center',
    badge: false
  };
  userMenuItems: MenuItem[] = [];
  loading: boolean = true;
  paramsResolved: boolean = false;
  innerWidth;
  public community: CommunityInfo = null;
  private subscriptions: any[] = [];
  headerLogoUrl: string;
  headerUrl: string;

  constructor(private route: ActivatedRoute,
              private communityService: CommunityService,
              private router: Router,
              private cdr: ChangeDetectorRef,
              private smoothScroll: SmoothScroll,
              private layoutService: LayoutService,
              private userManagementService: UserManagementService,
              private configurationService: ConfigurationService) {
    this.subscriptions.push(this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        let r = this.route;
        while (r.firstChild) {
          r = r.firstChild;
        }
        this.paramsResolved = true;
        this.params.next(r.snapshot.params);
        this.data.next(r.snapshot.data);
      }
    }));
  }
  
  ngOnInit() {
    if (typeof document !== 'undefined' && window) {
      this.innerWidth = window.innerWidth;
    }
    this.subscriptions.push(this.layoutService.hasSidebar.subscribe(hasSidebar => {
      this.hasSidebar = hasSidebar;
      this.cdr.detectChanges();
    }));
    this.subscriptions.push(this.layoutService.hasInternalSidebar.subscribe(hasInternalSidebar => {
      this.hasInternalSidebar = hasInternalSidebar;
      this.cdr.detectChanges();
    }));
    this.subscriptions.push(this.layoutService.hasHeader.subscribe(hasHeader => {
      this.hasHeader = hasHeader;
      this.cdr.detectChanges();
    }));
    this.subscriptions.push(this.layoutService.hasAdminMenu.subscribe(hasAdminMenu => {
      this.hasAdminMenu = hasAdminMenu;
      this.cdr.detectChanges();
    }));
    this.subscriptions.push(this.layoutService.isFrontPage.subscribe(isFrontPage => {
      this.isFrontPage = isFrontPage;
      this.cdr.detectChanges();
    }));
    this.layoutService.setSmallScreen((this.innerWidth && this.innerWidth <= 640));
    this.layoutService.setOpen(!(this.innerWidth && this.innerWidth <= 640));
    this.subscriptions.push(this.params.subscribe(params => {
      if (this.paramsResolved) {
        this.loading = true;
        if (params && params['community']) {
          if (!this.community || this.community.communityId !== params['community']) {
            this.subscriptions.push(this.communityService.getCommunity(params['community']).subscribe(community => {
              if (community) {
                this.community = community;
                this.setProperties(community.communityId);
                this.buildMenu();
                this.loading = false;
              } else {
                this.community = null;
                this.buildMenu();
                this.loading = false;
              }
            }));
          } else {
            this.loading = false;
          }
        } else {
          this.communityService.setCommunity(null);
          this.community = null;
          this.buildMenu();
          this.loading = false;
        }
      }
    }));
    this.subscriptions.push(this.data.subscribe(data => {
      if(data && data.portal) {
        this.setProperties(data.portal);
        this.configurationService.initPortal(this.properties, this.properties.adminToolsCommunity);
      }
    }));
    this.subscriptions.push(this.userManagementService.getUserInfo().subscribe(user => {
      if (user) {
        this.user = user;
        localStorage.setItem('user_id', this.user.id);
        localStorage.setItem('mining_backend_address', this.properties.miningBackendURL);
        localStorage.setItem('isCommunityManager', Session.isCommunityCurator(this.user) + '');
        this.buildMenu();
      } else if(this.user) {
        this.user = user;
        this.buildMenu();
      }
    }));
  }
  
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    if (this.layoutService.isSmallScreen && event.target.innerWidth > 1219) {
      this.layoutService.setSmallScreen(false);
    } else if (!this.layoutService.isSmallScreen && event.target.innerWidth < 1219) {
      this.layoutService.setSmallScreen(true);
      this.layoutService.setOpen(false);
    }
  }
  
  public ngOnDestroy() {
    this.subscriptions.forEach(value => {
      if (value instanceof Subscriber) {
        value.unsubscribe();
      }
    });
    this.communityService.clearSubscriptions();
    this.userManagementService.clearSubscriptions();
    this.layoutService.clearSubscriptions();
    this.smoothScroll.clearSubscriptions();
  }
  
  setProperties(id, type = null){
    this.properties.adminToolsCommunity = id;
    if(type) {
      this.properties.adminToolsPortalType = type;
    } else {
      ConnectHelper.setPortalTypeFromPid(id);
    }
    this.configurationService.initPortal(this.properties, this.properties.adminToolsCommunity);
  }
  
  public get open() {
    return this.layoutService.open;
  }
  
  public get hover() {
    return this.layoutService.hover;
  }
  
  private buildMenu() {
    this.userMenuItems = [];
    this.sideBarItems = [];
    if (this.user) {
      if (this.isCurator()) {
        this.userMenuItems.push(new MenuItem("", "Manage communities",
          "", "/", false, [], [], {}));
      }
      if (Session.isPortalAdministrator(this.user)) {
        this.userMenuItems.push(new MenuItem("adminOptions", "Super Admin options", "", "/admin-tools/portals", false, [], [], {}));
        this.userMenuItems.push(new MenuItem("connectOptions", "Connect portal options", "", "/connect/admin-tools/pages", false, [], [], {}));
        this.userMenuItems.push(new MenuItem("exploreOptions", "Explore portal options", "", "/openaire/admin-tools/pages", false, [], [], {}));
  
      }
      if (this.community) {
        this.userMenuItems.push(new MenuItem("", "User information", "", "/"+this.community.communityId+"/user-info", false, [], [], {}));
      } else {
        this.userMenuItems.push(new MenuItem("", "User information", "", "/user-info", false, [], [], {}));
      }
    }
    if (this.community) {
      this.headerLogoUrl = StringUtils.getLogoUrl(this.community);
      this.headerUrl = 'https://' + ((properties.environment !== 'production')?'beta.':'') + this.community.communityId + '.openaire.eu';
      this.menuHeader = {
        route: "/" + this.community.communityId,
        url: null,
        title: this.community.shortTitle,
        logoUrl: this.headerLogoUrl,
        logoSmallUrl: this.headerLogoUrl,
        position: 'left',
        badge: true
      };
      let communityInfo = new MenuItem("community", "Community Info", "", "/" + this.community.communityId, false, [], [], {}, {name: 'badge'}, null, null, "/" + this.community.communityId + "/info");
      communityInfo.items = [
        new MenuItem("profile", "Profile", "", "/" + this.community.communityId + "/info/profile", false, [], [], {}, null, null, null, "/" + this.community.communityId + "/info/profile"),
        new MenuItem("organizations", "Organizations", "", "/" + this.community.communityId + "/info/organizations", false, [], [], {}, null, null, null, "/" + this.community.communityId + "/info/organizations"),
        new MenuItem("projects", OpenaireEntities.PROJECTS, "", "/" + this.community.communityId + "/info/projects", false, [], [], {}, null, null, null, "/" + this.community.communityId + "/info/projects"),
        new MenuItem("content-providers", OpenaireEntities.DATASOURCES, "", "/" + this.community.communityId + "/info/content-providers", false, [], [], {}, null, null, null, "/" + this.community.communityId + "/info/content-providers"),
        new MenuItem("zenodo-communities", "Zenodo Communities ", "", "/" + this.community.communityId + "/info/zenodo-communities", false, [], [], {}, null, null, null, "/" + this.community.communityId + "/info/zenodo-communities"),
        new MenuItem("advanced-criteria", "Advanced Criteria", "", "/" + this.community.communityId + "/info/advanced-criteria", false, [], [], {}, null, null, null, "/" + this.community.communityId + "/info/advanced-criteria")
      ]
      if(this.community.type === 'community') {
        communityInfo.items.splice(4, 0, new MenuItem("subjects", "Subjects", "", "/" + this.community.communityId + "/info/subjects", false, [], [], {}, null, null, null, "/" + this.community.communityId + "/info/subjects"))
      }
      this.sideBarItems.push(communityInfo);
      this.sideBarItems.push(new MenuItem("users", "Users", "", "/" + this.community.communityId + "/users", false, [], [], {}, {name: 'group'}, null, null, "/" + this.community.communityId + "/users"));
      this.sideBarItems.push(new MenuItem("admin-tools", "Pages & Menus", "", "/" + this.community.communityId + "/admin-tools/pages", false, [], [], {}, {name: 'description'}, null, null, "/" + this.community.communityId + "/admin-tools"));
      this.sideBarItems.push(new MenuItem("customization", "Customization", "", "/" + this.community.communityId + "/customize-layout", false, [], [], {}, {name: 'brush'}));
      if (this.community.type === 'ri') {
        this.sideBarItems.push(new MenuItem("mining", "Mining", "", "/" + this.community.communityId + "/mining/manage-profiles", false, [], [], {}, {svg: mining.data}, null, null, "/" + this.community.communityId + "/mining"));
      }
      this.backItem = new MenuItem("back", "Manage Communities", "", "/", false, [], null, {}, {name: 'west'});

      this.menuItems = [];
      this.menuItems.push(new MenuItem("home", "Home",
          "https://"+(properties.environment == "beta" ? "beta." : "")+this.community.communityId+".openaire.eu/", "", false, [], null, null
          , null, null, null, null, "_self")
      );
      this.menuItems.push(
        new MenuItem("deposit", "Deposit", "https://"+(properties.environment == "beta" ? "beta." : "")+this.community.communityId+".openaire.eu/participate/deposit/learn-how", "", false, [], ['/participate/deposit/learn-how'], null, null, null, null, null, "_self")
      );
      this.menuItems.push(
        new MenuItem("link", "Link", "https://"+(properties.environment == "beta" ? "beta." : "")+this.community.communityId+".openaire.eu/participate/claim", "", false, [], ['/participate/claim'], null, null, null, null, null, "_self",
          "_internal", false,
          [
            new MenuItem("", "Start linking", "https://"+(properties.environment == "beta" ? "beta." : "")+this.community.communityId+".openaire.eu/participate/claim", "", false, [], ['/participate/claim'], null, null, null, null, null, "_self"),
            new MenuItem("", "Learn more", this.properties.claimsInformationLink, "", false, [], [], null),
          ]
        )
      );
      this.menuItems.push(
        new MenuItem("search", "Search", "https://"+(properties.environment == "beta" ? "beta." : "")+this.community.communityId+".openaire.eu/search/find", "", false, [], ["/search/find"], null, null, null, null, null, "_self",
          "internal", false,
          [
            new MenuItem("", OpenaireEntities.RESULTS, "https://"+(properties.environment == "beta" ? "beta." : "")+this.community.communityId+".openaire.eu/search/find/research-outcomes?resultbestaccessright=\"" + encodeURIComponent("Open Access") + '"', "", false, [], ["/search/find/research-outcomes"], null, null, null, null, null, "_self"),
            new MenuItem("", OpenaireEntities.PROJECTS, "https://"+(properties.environment == "beta" ? "beta." : "")+this.community.communityId+".openaire.eu/search/find/projects", "", false, [], ["/search/find/projects"], null, null, null, null, null, "_self"),
            new MenuItem("", OpenaireEntities.DATASOURCES, "https://"+(properties.environment == "beta" ? "beta." : "")+this.community.communityId+".openaire.eu/search/find/dataproviders", "", false, [], ["/search/find/dataproviders"], null, null, null, null, null, "_self")
          ]
        )
      );
      this.menuItems.push(
        new MenuItem("about", "About", "", "", false, [], [], {},
          null, null, null, null, "_blank", "internal", false,
          [
            new MenuItem("", "Supporting organizations", "https://"+(properties.environment == "beta" ? "beta." : "")+this.community.communityId+".openaire.eu/organizations", "", false, [], ["/organizations"], null, null, null, null, null, "_self"),
            new MenuItem("", "Curators", "https://"+(properties.environment == "beta" ? "beta." : "")+this.community.communityId+".openaire.eu/curators", "", false, [], ["/organizations"], null, null, null, null, null, "_self"),
            new MenuItem("", "Sources and methodology", "https://"+(properties.environment == "beta" ? "beta." : "")+this.community.communityId+".openaire.eu/content", "", false, [], ["/content"], null, null, null, null, null, "_self"),
            new MenuItem("", "National Bulletins", "https://"+(properties.environment == "beta" ? "beta." : "")+this.community.communityId+".openaire.eu/national-bulletins", "", false, [], ["/national-bulletins"], null, null, null, null, null, "_self"),
            new MenuItem("", "Subjects", "https://"+(properties.environment == "beta" ? "beta." : "")+this.community.communityId+".openaire.eu/subjects", "", false, [], ["/subjects"], null, null, null, null, null, "_self"),
            new MenuItem("", "Projects and funding Opportunities", "https://"+(properties.environment == "beta" ? "beta." : "")+this.community.communityId+".openaire.eu/projects", "", false, [], ["/projects"], null, null, null, null, null, "_self"),
            new MenuItem("", "Fields of Science", "https://"+(properties.environment == "beta" ? "beta." : "")+this.community.communityId+".openaire.eu/fields-of-science", "", false, [], ["/fields-of-science"], null, null, null, null, null, "_self"),
            new MenuItem("", "Sustainable Development Goals", "https://"+(properties.environment == "beta" ? "beta." : "")+this.community.communityId+".openaire.eu/sdgs", "", false, [], ["/sdgs"], null, null, null, null, null, "_self"),

          ]
        )
      );
			this.menuItems.push(
        new MenuItem("develop", "Develop", "https://"+(properties.environment == "beta" ? "beta." : "")+this.community.communityId+".openaire.eu/develop", "", false, [], ['/develop'], null, null, null, null, null, "_self"),
			);
      // if (this.isManager) {
        this.menuItems.push(
          new MenuItem("manage", "Manage",
            "", "/"+this.community.communityId, false, [], null, {},
            null, null, null, "/"+this.community.communityId)
        );
      // }
    } else {
      this.headerLogoUrl = null;
      this.headerUrl = 'https://' + ((properties.environment !== 'production')?'beta.':'') + 'connect.openaire.eu';
      this.menuHeader = {
        route: null,
        url: this.headerUrl,
        title: "Monitor",
        logoUrl: 'assets/common-assets/logo-services/connect/small.svg',
        logoSmallUrl: "assets/common-assets/logo-services/connect/small.svg",
        position: 'left',
        badge: true,
        menuPosition: "center"
      };
      this.menuItems = [];
      this.menuItems.push(
        new MenuItem("about", "About", this.headerUrl + "/about/learn-how", "", false, [], [], {}, null, null, null, null, "_self",
          "internal", false,
          [
            new MenuItem("", "Learn the process", this.headerUrl + "/about/learn-how",  "", false, [], [], {}, null, null, null, null, "_self"),
            new MenuItem("", "Publications",  this.headerUrl + "/publications", "", false, [], [], {}, null, null, null, null, "_self"),
            new MenuItem("", "Roadmap", "https://trello.com/b/yfzUz0kp/openaire-connect-dashboard", "", false, [], [], {}),
            new MenuItem("", "FAQs", this.headerUrl + "/about/faq", "", false, [], [], {}, null, null, null, null, "_self")
          ]
        )
      );
      this.menuItems.push(
        new MenuItem("communities", "Communities", this.headerUrl + "/search/find/communities", "", false, [], [], {}, null, null, null, null, "_self")
      );
      if(this.isCurator()) {
        this.sideBarItems.push(new MenuItem("communities", "Manage communities", "", "/", false, [], [], {}, {name: 'settings'}));
      }
      if (Session.isPortalAdministrator(this.user)) {
        this.sideBarItems.push(new MenuItem("super_admin", "Super Admin Options", "", "/admin-tools/portals", false, [], [], {}, {name: 'settings'}, null, null, '/admin-tools'));
        this.sideBarItems.push(new MenuItem("connect", "Connect Options", "", "/connect/admin-tools/pages", false, [], [], {}, {name: 'settings'}, null, null, '/connect/admin-tools'));
        this.sideBarItems.push(new MenuItem("explore", "Explore Options", "", "/openaire/admin-tools/pages", false, [], [], {}, {name: 'settings'}, null, null, '/openaire/admin-tools'));
      }
      this.backItem = null;
    }
    this.hasSidebar = this.hasSidebar && this.sideBarItems.length > 1;
  }
  
  private isCurator() {
    return this.user && (Session.isPortalAdministrator(this.user) || Session.isCommunityCurator(this.user) || Session.isKindOfCommunityManager(this.user));
  }
}
