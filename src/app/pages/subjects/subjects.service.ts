import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {map} from "rxjs/operators";

@Injectable()
export class SubjectsService {

  constructor(private http: HttpClient) {

  }

  addSubjects(url: string, subjects: any) {
    let headers = new HttpHeaders({'Content-Type': 'application/json'});

    const body = JSON.stringify(subjects);

      return this.http.post(url, body, {headers: headers})
                        // .do(request => console.log("Insert Response:"+request.status))
                        //.map(res => res.json())
                        .pipe(map(res => {
                          res['method'] = 'post';
                          return res;
                        }));
  }

  removeSubjects(url: string, subjects: any) {
      //const headers = new Headers({'Content-Type': 'application/json'});
      let headers = new HttpHeaders({'Content-Type': 'application/json'});

      const body = JSON.stringify(subjects);
      //const options = new RequestOptions({headers: headers, body: body});

      //return this.http.delete(url, options)
      return this.http.request('delete', url, { body: body, headers: headers})
                        // .do(request => console.log("Delete Response:"+request.status))
                        //.map(res => res.json())
                        .pipe(map(res => {
                          res['method'] = 'delete';
                          return res;
                        }));
  }
}
