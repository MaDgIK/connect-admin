import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {SubjectsEditFormComponent} from './subjects-edit-form.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            { path: '',   component: SubjectsEditFormComponent}
        ])
    ]
})
export class SubjectsEditFormRoutingModule { }
