import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';

import {SubjectsEditFormComponent} from './subjects-edit-form.component';

import {SubjectsService} from '../subjects.service';
import {SubjectsEditFormRoutingModule} from './subjects-edit-form-routing.module';
import {PageContentModule} from '../../../openaireLibrary/dashboard/sharedComponents/page-content/page-content.module';
import {IconsModule} from '../../../openaireLibrary/utils/icons/icons.module';
import {AlertModalModule} from '../../../openaireLibrary/utils/modal/alertModal.module';
import {NoLoadPaging} from '../../../openaireLibrary/searchPages/searchUtils/no-load-paging.module';
import {LoadingModule} from '../../../openaireLibrary/utils/loading/loading.module';
import {InputModule} from '../../../openaireLibrary/sharedComponents/input/input.module';
import {SearchInputModule} from '../../../openaireLibrary/sharedComponents/search-input/search-input.module';
import {FullScreenModalModule} from 'src/app/openaireLibrary/utils/modal/full-screen-modal/full-screen-modal.module';
import {PagingModule} from "../../../openaireLibrary/utils/paging.module";
import {SdgSelectionModule} from '../../../openaireLibrary/sdg/sdg-selection/sdg-selection.module';
import {FosSelectionModule} from '../../../openaireLibrary/fos/fos-selection/fos-selection.module';
import {ValidateEnabledPageModule} from "../../../utils/validateEnabledPage.module";

@NgModule({
  imports: [
    SubjectsEditFormRoutingModule, CommonModule, FormsModule, RouterModule,
    PageContentModule, IconsModule, AlertModalModule, NoLoadPaging, LoadingModule, InputModule,
    SearchInputModule, FullScreenModalModule, PagingModule, SdgSelectionModule, FosSelectionModule, ValidateEnabledPageModule
  ],
  declarations: [
    SubjectsEditFormComponent
  ],
  providers: [
    SubjectsService
  ],
  exports: [
    SubjectsEditFormComponent
   ]
})
export class SubjectsEditFormModule {}
