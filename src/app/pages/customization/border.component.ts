import {Component, EventEmitter, Input, OnInit, Output,} from '@angular/core';


@Component({
  selector: 'border',
  template: `
    <div class="uk-grid  ">
      <div class="uk-margin-remove">
        <div class="uk-text-bold  uk-margin-xsmall-bottom uk-text-uppercase uk-text-meta uk-flex uk-flex-middle"> Border radius (px)
            <a *ngIf="radius != radiusPublished" class="uk-margin-small-left uk-button-link"
               uk-tooltip="title:<div class='uk-width-large'>Reset to previously saved options</div>"
               (click)="radius = radiusPublished;borderChanged()">  <icon name="settings_backup_restore" flex="true"></icon></a>
        
        </div>
        <div class="input-box">
            <div  input inputClass="flat x-small"   [(value)]="radius" (valueChange)="borderChanged()" type="text" ></div>
        </div>
      </div>
      <div class="uk-margin-small-top">
        <div class="uk-text-bold uk-text-uppercase uk-text-meta uk-margin-xsmall-bottom uk-flex uk-flex-middle"> Border width (px)
            <a *ngIf="width != widthPublished" class="uk-margin-small-left uk-button-link"
               uk-tooltip="title:<div class='uk-width-large'>Reset to previously saved options</div>"
               (click)="width = widthPublished; borderChanged()">  <icon name="settings_backup_restore" flex="true"></icon></a>
        </div>
        <div class="input-box">
            <div  input  inputClass="flat x-small"  [(value)]="width" (valueChange)="borderChanged()" type="text" ></div>
        </div>
      </div> 
      <!--<div class="uk-margin-top uk-width-1-1">
        <div class="uk-text-bold uk-text-uppercase uk-text-meta uk-margin-xsmall-bottom"> Border style
            <a *ngIf="style != stylePublished" class="uk-margin-small-left"
               uk-tooltip="title:<div class='uk-padding-small uk-width-large'>Reset to previously saved options</div>"
               (click)="style = stylePublished;borderChanged()">  <icon name="reset"></icon></a>
        </div>
          <div class="uk-width-expand uk-padding-remove-left" input inputClass="flat x-small"  type="select" [(value)]="style" (valueChange)="borderChanged()"
               [options]="['solid','dotted','dashed']"  >
          </div>
      </div>-->
    </div>
  `
})

export class BorderComponent implements OnInit {
  @Input() radius: number = 0;
  @Input() width: number = 0;
  @Input() style = 'solid';
  @Input() radiusPublished: number = 0;
  @Input() widthPublished: number = 0;
  @Input() stylePublished = 'solid';
  @Output() borderChange = new EventEmitter();
  
  
  constructor() {
  }
  ngOnInit() {
  }
  
  borderChanged() {
    console.log(this.radius);
    this.borderChange.emit({radius: this.radius, width: this.width, style: this.style});
  }
}
