import {Component, Input, OnInit} from '@angular/core';
import {properties} from '../../../environments/environment';
import {UtilitiesService} from '../../openaireLibrary/services/utilities.service';
import {Subscription} from 'rxjs';

declare var UIkit;

@Component({
  selector: 'background-upload',
  template: `
    <input #file id="photo" type="file" class="uk-hidden" (change)="fileChangeEvent($event)"/>
    <div *ngIf="!background.imageFile"
         class=" upload uk-text-center uk-height-small uk-flex uk-flex-middle uk-flex-center uk-text-uppercase">
      <span uk-icon="icon: cloud-upload"></span>
      <div uk-form-custom>
        <span class="uk-link uk-margin-small-left" (click)="file.click()">upload image</span>
      </div>
    </div>

    <div *ngIf="background.imageFile" class="uk-width-1-1 uk-flex uk-flex-middle ">
      <div class="uk-width-1-1 uk-height-small" style="background-size:cover"
           [style.background-image]=" getUrl()">


        <a (click)="removePhoto()" uk-tooltip="Remove"
           class="uk-float-right  uk-border-rounded uk-margin-top uk-margin-right uk-background-muted uk-button uk-button-link">
          <icon name="delete" [defaultSize]="true" [flex]="true"></icon>
        </a>
        <div class="uk-text-small uk-margin-large-top uk-text-center" [class.uk-light]="!light">Lorem ipsum dolor sit amet...</div>
      </div>
    </div>
    <div *ngIf="background.imageFile" class="uk-margin-top">
      <div class="uk-text-bold uk-text-uppercase uk-text-meta uk-margin-small-bottom uk-margin-large-top">position</div>
      <div class="uk-width-expand uk-padding-remove-left" input type="select" inputClass="flat x-small"
           [(value)]="background.position"
           [options]="['top','center','bottom']">
      </div>

    </div>
  
  `,
  styles: [`
    .upload {
      border: 1px dotted grey;
    }
  `]
})

export class BackgroundUploadComponent implements OnInit {
  @Input() label: string = "";
  @Input() background;
  @Input() oldBackground;
  // @Input() light:boolean;
  @Input() communityId: string = "";
  @Input() light:boolean; //fonts mode
  public file: File;
  // public photo: string | ArrayBuffer;
  private maxsize: number = 2000 * 1024;
  properties;
  private subscriptions: any[] = [];
  
  
  constructor(private utilsService: UtilitiesService) {
  }
  
  
  ngOnInit() {
    this.properties = properties;
  }
  
  ngOnDestroy() {
    this.subscriptions.forEach(subscription => {
      if (subscription instanceof Subscription) {
        subscription.unsubscribe();
      }
    });
  }
  
  removePhoto() {
    
    if (typeof document != 'undefined') {
      (<HTMLInputElement>document.getElementById("photo")).value = "";
    }
    // this.initPhoto();
    console.log(this.background.imageFile + " " + this.oldBackground.imageFile)
    if (this.background.imageFile != this.oldBackground.imageFile) {
      this.deletePhoto();
    }
    this.background.imageFile = null;
    this.file = null;
    
  }
  
  
  public deletePhoto() {
    if (this.background.imageFile) {
      this.subscriptions.push(this.utilsService.deletePhoto(properties.utilsService + '/delete/community/' + this.communityId + "/" + this.background.imageFile).subscribe());
    }
  }
  
  fileChangeEvent(event) {
    if (event.target.files && event.target.files[0]) {
      this.file = event.target.files[0];
      if (this.file.type !== 'image/png' && this.file.type !== 'image/jpeg') {
        UIkit.notification('You must choose a file with type: image/png or image/jpeg!', {
          status: 'danger',
          timeout: 6000,
          pos: 'bottom-right'
        });
        this.removePhoto();
      } else if (this.file.size > this.maxsize) {
        UIkit.notification('File exceeds size\'s limit (' + this.maxsize / 1024 + 'KB)!  ', {
          status: 'danger',
          timeout: 6000,
          pos: 'bottom-right'
        });
        this.removePhoto();
      } else {
        /*const reader = new FileReader();
        reader.readAsDataURL(this.file);
        reader.onload = () => {
          this.background.imageFile = reader.result;

        };*/
        this.save();
      }
    }
  }
  
  public save() {
    if (this.file) {
      this.subscriptions.push(this.utilsService.uploadPhoto(this.properties.utilsService + "/upload/community/" + this.communityId + "/" + encodeURIComponent(this.communityId + "-" + this.label) + "?big=1", this.file).subscribe(res => {
        this.deletePhoto();
        this.removePhoto();
        this.background.imageFile = res.filename;
        
      }, error => {
        if (error.error.message) {
          UIkit.notification('The maximum size of an image is 1MB, please check if your file exceeds this limit.', {
            status: 'danger',
            timeout: 6000,
            pos: 'bottom-right'
          });
        } else {
          UIkit.notification("An error has been occurred during upload your image. Try again later", {
            status: 'danger',
            timeout: 6000,
            pos: 'bottom-right'
          });
        }
      }));
    }
  }
  
  getUrl() {
    return "url('" + (this.background.imageFile.indexOf('data:') == -1 ? (this.properties.utilsService +
      '/download/' + this.background.imageFile) : this.background.imageFile) + "')";
  }
}
