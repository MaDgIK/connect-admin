import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CustomizationComponent} from './customization.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            { path: '', component: CustomizationComponent}
        ])
    ]
})
export class CustomizationRoutingModule { }
