import {Component, EventEmitter, Input, OnInit, Output,} from '@angular/core';


@Component({
  selector: 'font-size',
  template: `
    <div class="uk-grid uk-margin-remove">
      <div class="uk-width-1-3 uk-margin-small-top uk-padding-remove-left"> Font:</div>
      <div class="uk-width-2-3 uk-padding-remove-left">
<!--        <input class="uk-input uk-width-small uk-margin-small-left" [(ngModel)]="font" (ngModelChange)="fontChanged()"/>-->
        <select [(ngModel)]="font" name="{{'select_type_'}}"
                class="uk-select uk-width-small" (ngModelChange)="fontChanged()">
          <option [value]="'Open Sans'" >Open Sans</option>
          <option [value]="'Heebo'" >Heebo</option>
          <option [value]="'Heebo'" >Sura</option>
          <option [value]="'sans-serif'" >sans-serif</option>
          <option [value]="'serif'" >serif</option>
          <option [value]="'Times New Roman'" >Times New Roman</option>
        </select>
      </div>
    </div>
    <div class="uk-grid uk-margin-remove">
      <div class="uk-width-1-3 uk-margin-small-top uk-padding-remove-left"> Size:</div>
      <div class="uk-width-2-3 uk-padding-remove-left">
        <input class="uk-input uk-width-small uk-margin-small-left" [(ngModel)]="size" (ngModelChange)="fontChanged()" type="number" min="6" max="100"/>
      </div>
    </div>
    <div class="uk-grid uk-margin-remove">
      <div class="uk-width-1-3 uk-margin-small-top uk-padding-remove-left"> Weight:</div>
      <div class="uk-width-2-3 uk-padding-remove-left">
        <input class="uk-input uk-width-small uk-margin-small-left" [(ngModel)]="weight" (ngModelChange)="fontChanged()" type="number" min="400" max="1000" step="100"/>
      </div>
    </div>

  `
})

export class FontSizeComponent implements OnInit {
 @Input() font="";
 @Input() size:number;
 @Input() weight:number;
 @Output() fontChange = new EventEmitter();


  constructor( ) {
  }


  ngOnInit() {


  }

  fontChanged() {
    console.log({font:this.font, size:this.size, weight:this.weight});
    this.fontChange.emit({font:this.font, size:this.size, weight:this.weight});
  }


}
