import {Component, HostBinding, Input} from '@angular/core';
import {DomSanitizer} from '@angular/platform-browser';
import {ButtonsCustomization} from '../../openaireLibrary/connect/community/CustomizationOptions';


@Component({
  selector: 'quick-look-backgrounds',
  template: `
  <div class="uk-grid uk-child-width-1-2" >
    <div>
      <div class="uk-margin-large-top uk-margin-medium-bottom   uk-text-large">Light background</div>
      <div [class]="'uk-padding-small uk-text-center  uk-width-1-1 lightBackground' + (buttonView?' uk-flex uk-flex-middle uk-flex-center':'') "
           [class.uk-height-small]="buttonView"  >
        <div *ngIf="!buttonView" class="uk-margin-small-bottom">
          <h1 >Heading</h1>
          <div class="uk-margin-small-bottom">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent mollis velit ornare, auctor lectus at, rutrum magna. Aenean vehicula elementum lacinia.</div>
          <a class="portal-link">Link</a>
          <br>
        </div>
        <quick-look-buttons
          [buttons]="buttonsOnLight" [border]="buttonsOnLight" ></quick-look-buttons>
      </div>
    </div>
    <div>
      <div class="uk-margin-large-top uk-margin-medium-bottom   uk-text-large">Dark background</div>
      <div [class]="'uk-padding-small uk-text-center darkBackground uk-light' + (buttonView?' uk-flex uk-flex-middle uk-flex-center':'') "
           [class.uk-height-small]="buttonView">
        <div *ngIf="!buttonView" class="uk-margin-small-bottom">
          <h1 >Heading</h1>
          <div class="uk-margin-small-bottom">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent mollis velit ornare, auctor lectus at, rutrum magna. Aenean vehicula elementum lacinia.</div>
          <a class="">Link</a>
        </div>
        <quick-look-buttons
            [buttons]="buttonsOnDark"   [border]="buttonsOnLight"
        ></quick-look-buttons>
      </div>
    </div>
  </div>
  `,
  styles:[`
    .darkBackground{
        background-color: var(--background-dark-color);
        border-radius: 6px;
    }
    .lightBackground{
        background-color: var(--background-low-color);
        border-radius: 6px;
    }

  `]

})

export class QuickLookBackgroundsComponent {
  @Input() primaryColor;
  @Input() secondaryColor;
  @Input() darkBackgroundColor;
  @Input() lightBackgroundColor;
  @Input() formBackgroundColor;
  @Input() buttonsOnDark:ButtonsCustomization;
  @Input() buttonsOnLight:ButtonsCustomization;
  @Input() buttonView: boolean = false;
  constructor(   private sanitizer: DomSanitizer) {
  }

  @HostBinding("attr.style")
  public get valueAsStyle(): any {
    let search = `<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 2000 532"><defs><style>.cls-1{isolation:isolate;}.cls-2{fill:{{color}};mix-blend-mode:multiply;}</style></defs><title>Asset 3</title><g class="cls-1"><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path class="cls-2" d="M0,431s362,109,841,62,632,68,1159-9V0H0Z"/><path class="cls-2" d="M0,514s1401,71,2000-69V0H0Z"/></g></g></g></svg>`;
    let svg = 'data:image/svg+xml,' + (search.replace('{{color}}', this.formBackgroundColor));
    return this.sanitizer.bypassSecurityTrustStyle(`--portal-main-color: ${this.primaryColor};  --portal-dark-color: ${this.secondaryColor};  
    --background-dark-color: ${this.darkBackgroundColor};    --background-low-color: ${this.lightBackgroundColor}; --svgURL: url('${svg}');`);
  }
}
