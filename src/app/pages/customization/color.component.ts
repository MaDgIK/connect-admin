import {Component, EventEmitter, Input, OnInit, Output,} from '@angular/core';
import {CustomizationOptions} from '../../openaireLibrary/connect/community/CustomizationOptions';


@Component({
  selector: 'color',
  template: `
    <div [class]="(addMargin?'uk-margin-small-top':'') + ' colorPicker uk-flex uk-flex-middle'">
      <div class=" uk-flex">
        <div class="">
          <input class="uk-margin-small-left color-input" color-picker [colorPicker]="color"   [style.background]="color"
                 (colorPickerChange)="color=$event; colorChanged();"/>
        </div>
        <div class="uk-margin-small-left"> {{label}}
          <div class="uk-text-meta uk-text-xsmall" *ngIf="description" >{{description}}</div>
        </div>
      </div>
    </div>    

    <div class="uk-text-warning uk-text-xsmall uk-margin-small-left" [class.uk-invisible]="!warningForContrast(color)">Contrast ratio may be too low.</div>
  `,
  styles:[
    `
        .color-input{
            width:24px;
            height:14px;
            border-radius:3px;
            border:0.1px solid #707070;
            cursor: pointer;
            
        }`
  ]
})

export class ColorComponent implements OnInit {
  @Input() color = 'white';
  @Input() label = 'Color';
  @Input() addMargin: boolean = false;
  @Output() colorChange = new EventEmitter();
  @Input() light:boolean;
  @Input() description:string = null;
  public warningForContrast(color:string){
    return (this.light &&  CustomizationOptions.isForLightBackground(color)) || (!this.light && !CustomizationOptions.isForLightBackground(color));
  }

  constructor() {
  }


  ngOnInit() {
  }

  colorChanged() {
    this.colorChange.emit(this.color);
  }


}
