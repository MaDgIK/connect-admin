import {Component, Input} from '@angular/core';
import {ButtonsCustomization} from '../../openaireLibrary/connect/community/CustomizationOptions';


@Component({
  selector: 'customize-buttons',
  template: `
 
      <div class="">
        <div  *ngIf="onlyBorder"  class="uk-margin-medium-bottom" >
          <border [style]="buttons.borderStyle" [width]=
            "buttons.borderWidth" [radius]=
                    "buttons.borderRadius" (borderChange)=
                    " buttons.borderStyle = $event.style;
                          buttons.borderRadius = $event.radius;
                          buttons.borderWidth = $event.width; "
          [stylePublished]="buttonsPublished.borderStyle"
                  [widthPublished]="buttonsPublished.borderWidth"
                  [radiusPublished]="buttonsPublished.borderRadius"
          ></border>
        </div>
        <div  *ngIf="!onlyBorder" class="uk-grid  uk-margin-top" >
          <div class="">
            <div class=" uk-margin-xsmall-bottom uk-text-bold uk-margin-small-left uk-text-meta uk-text-uppercase uk-flex uk-flex-middle">Colors
                <a *ngIf="buttons.color !=buttonsPublished.color ||  buttons.backgroundColor != buttonsPublished.backgroundColor || buttons.borderColor !=buttonsPublished.borderColor  " class="uk-margin-small-left uk-button-link"
                   uk-tooltip="title:<div class='uk-width-large'>Reset to previously saved options</div>"
                   (click)="buttons.color =buttonsPublished.color;  buttons.backgroundColor = buttonsPublished.backgroundColor ; buttons.borderColor =buttonsPublished.borderColor  ">  <icon name="settings_backup_restore" flex="true"></icon></a>
            </div>
            <color [color]="buttons.color" (colorChange)=
              " buttons.color = $event; "
                   label="Fonts"  [light]="!light"
                   ></color>
            <color [color]="buttons.backgroundColor" (colorChange)=
              " buttons.backgroundColor = $event; " [light]="light"
                   label="Background" ></color>
            <color [color]="buttons.borderColor" (colorChange)=
              " buttons.borderColor = $event; " [light]="light"
                   label="Border"
                   
            ></color>
          </div>
          <div class="uk-margin-top">
            <div class=" uk-margin-xsmall-bottom uk-text-bold uk-margin-small-left uk-text-meta uk-text-uppercase uk-flex uk-flex-middle">Colors on hover
                <a *ngIf="hasChanges(buttons.onHover,buttonsPublished.onHover)" class="uk-margin-small-left uk-button-link"
                   uk-tooltip="title:<div class='uk-width-large'>Reset to previously saved options</div>"
                   (click)="buttons.onHover.color =buttonsPublished.onHover.color;  buttons.onHover.backgroundColor = buttonsPublished.onHover.backgroundColor ; 
                   buttons.onHover.borderColor = buttonsPublished.onHover.borderColor">  <icon name="settings_backup_restore" flex="true"></icon></a>
            </div>
            <div class="">
            <color [color]="buttons.onHover.color" (colorChange)=
              " buttons.onHover.color = $event;" label="Fonts" [light]="!light"
            ></color>
            <color [color]="buttons.onHover.backgroundColor"
                   (colorChange)=
                     " buttons.onHover.backgroundColor = $event;" [light]="light"
                   label="Background" ></color>
            <color [color]="buttons.onHover.borderColor" (colorChange)=
              " buttons.onHover.borderColor = $event;" [light]="light"
                   label="Border" ></color>
            </div>
              </div>
          </div>
      </div>
     
      
`,
  styles:[` `]

})

export class CustomizeButtonsComponent {
  @Input() buttons:ButtonsCustomization;
  @Input() buttonsPublished:ButtonsCustomization;
  @Input() light:boolean;
  @Input() onlyBorder:boolean = false;
  constructor() {
  }
  hasChanges(object1,object2):boolean{
    return JSON.stringify(object1) != JSON.stringify(object2);
  }
}
