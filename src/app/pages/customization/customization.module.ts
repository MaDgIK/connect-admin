import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';

import {CustomizationComponent} from './customization.component';

import {CustomizationRoutingModule} from './customization-routing.module';
import {ColorPickerModule} from 'ngx-color-picker';
import {FontSizeComponent} from './fontSize.component';
import {ColorComponent} from './color.component';
import {BorderComponent} from './border.component';
import {CustomizationService} from '../../openaireLibrary/services/customization.service';
import {AlertModalModule} from '../../openaireLibrary/utils/modal/alertModal.module';
import {PageContentModule} from '../../openaireLibrary/dashboard/sharedComponents/page-content/page-content.module';
import {QuickLookComponent} from './quickLook.component';
import {QuickLookBackgroundsComponent} from './quickLook-backgrounds.component';
import {QuickLookButtonsComponent} from './quickLook-buttons.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import {CustomizeButtonsComponent} from './customize-buttons.component';
import {IconsModule} from '../../openaireLibrary/utils/icons/icons.module';
import {IconsService} from '../../openaireLibrary/utils/icons/icons.service';
import {BackgroundComponent} from './background.component';
import {InputModule} from '../../openaireLibrary/sharedComponents/input/input.module';
import {BackgroundUploadComponent} from "./background-upload.component";
import {FullScreenModalModule} from "../../openaireLibrary/utils/modal/full-screen-modal/full-screen-modal.module";
import {SideBarModule} from "../../openaireLibrary/dashboard/sharedComponents/sidebar/sideBar.module";

@NgModule({
  imports: [
    CustomizationRoutingModule, CommonModule, FormsModule, RouterModule, ColorPickerModule, AlertModalModule, PageContentModule, MatFormFieldModule, MatSelectModule, MatSlideToggleModule, IconsModule, InputModule, FullScreenModalModule, SideBarModule
  ],
    declarations: [
        CustomizationComponent, FontSizeComponent, ColorComponent, BorderComponent, QuickLookComponent, QuickLookBackgroundsComponent, QuickLookButtonsComponent, CustomizeButtonsComponent, BackgroundComponent, BackgroundUploadComponent
    ],
  providers: [
    CustomizationService
  ],
  exports: [
    CustomizationComponent
  ]
})
export class CustomizationModule {
  constructor() {}
}
