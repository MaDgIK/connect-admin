import {Component, Input, OnInit} from '@angular/core';
import {properties} from '../../../environments/environment';

declare var UIkit;

@Component({
  selector: 'background',
  template: `
    <color [color]="background.color"
           [label]="label" [light]="light"
           (colorChange)=
             " background.color = $event;" [description]="description"></color>

  `
})

export class BackgroundComponent implements OnInit {
  @Input() label:string = "";
  @Input() description:string = null;
  @Input() background;
  @Input() oldBackground;
  @Input() light:boolean;
  @Input() communityId:string = "";

  public file: File;
  properties;

  constructor() {
  }


  ngOnInit() {
    this.properties = properties;
  }
}
