import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

import {EnvProperties} from '../../openaireLibrary/utils/properties/env-properties';

import {Session} from '../../openaireLibrary/login/utils/helper.class';
import {LoginErrorCodes} from '../../openaireLibrary/login/utils/guardHelper.class';
import {DomSanitizer, Title} from '@angular/platform-browser';
import {CustomizationOptions, Layout} from '../../openaireLibrary/connect/community/CustomizationOptions';
import {CustomizationService} from '../../openaireLibrary/services/customization.service';
import {properties} from '../../../environments/environment';
import {UtilitiesService} from '../../openaireLibrary/services/utilities.service';
import {Subscription} from 'rxjs';
import {UserManagementService} from "../../openaireLibrary/services/user-management.service";
import {AlertModal} from "../../openaireLibrary/utils/modal/alert";
import {ClearCacheService} from "../../openaireLibrary/services/clear-cache.service";

declare var UIkit;

@Component({
  selector: 'customization',
  templateUrl: './customization.component.html',
  styles:[`
      .functionalities-container{
          padding-left:15px;
          padding-right:15px;
      }
    .functionalities-border{
        border-left:  1px solid #EAEAEA;
        padding: 5px 10px;
        margin: 25px 0px;
    }
    
  .refresh-indicator {
    background-color: rgba(0, 0, 0, 0.80);
    border-radius: 4px;
    position: absolute;
    color: white;
  }
  iframe, .refresh-indicator{
      height:100%
  }
  #container{
      position: relative;
      width: 100%;
      overflow: hidden;
      /*padding-top: 56.25%; !* 16:9 Aspect Ratio *!*/
      padding-top: 65%;
      background-color:black;
      border-radius:40px;
  }
      #iframecontainer{
 
          position: absolute;
          top: 4.5%;
          left: 3.5%;
          bottom: 4.5%;
          right: 3.5%;
          width: 93%;
          height: 91%;
          border: none;
          border-radius:20px;
          background-color: white;
      }
      .menu_section.uk-overflow-auto{
          overflow: hidden;
      }

      .menu_section.uk-overflow-auto:hover {
          overflow-y: scroll;
      }
  `]
})

export class CustomizationComponent implements OnInit {
  homeMenu =  {name:"Customization", id : "home", icon: "" }
  menuSelected = this.homeMenu;
  buttonsSelected = 'light';
  color = 'white';
  defaultCustomizationOptions:CustomizationOptions = new CustomizationOptions();
  publishedLayout: Layout = null;
  publishedCustomizationOptions: CustomizationOptions = null;
  draftCustomizationOptions: CustomizationOptions = null;
  appliedCustomizationOptions: CustomizationOptions = null;
  previewUrl = null;
  previewCustomization = null;
  public showLoading = true;
  public communityId = null;
  public properties: EnvProperties = null;
  private subscriptions: any[] = [];
  public enabled = true;
sidebarItems = [{name:"Identity", id : "identity", icon: "desktop_windows" },
  {name:"Backgrounds", id : "backgrounds", icon: "wallpaper" },
  {name:"Buttons", id : "buttons", icon: "smart_button" }]
  @ViewChild('leaveModal') closeModal: AlertModal;

  constructor(private element: ElementRef,
              private route: ActivatedRoute,
              private _router: Router,
              private title: Title,
              private sanitizer: DomSanitizer,
              private layoutService: CustomizationService,
              private utilsService: UtilitiesService,
              private userManagementService: UserManagementService,
              private _clearCacheService: ClearCacheService) {
  }

  ngOnDestroy() {
    this.deleteDraftImages();

  }
  cleanUp(){
    this.subscriptions.forEach(subscription => {
      if (subscription instanceof Subscription) {
        subscription.unsubscribe();
      }
    });
  }
  ngOnInit() {

    this.properties = properties;
    this.subscriptions.push(this.userManagementService.getUserInfo().subscribe(user => {
      if (!user) {
        this._router.navigate(['/user-info'], {
          queryParams: {
            "errorCode": LoginErrorCodes.NOT_VALID,
            "redirectUrl": this._router.url
          }
        });
      }
    }));

    this.subscriptions.push(this.route.params.subscribe((params) => {
      this.communityId = params['community'];
      if(this.communityId == "covid-19"){
        this.defaultCustomizationOptions= new CustomizationOptions(CustomizationOptions.getIdentity(this.communityId).mainColor,CustomizationOptions.getIdentity(this.communityId).secondaryColor);
      }
      this.title.setTitle('Administration Dashboard | Customization');
      this.showLoading = true;
      this.subscriptions.push(this.layoutService.getLayout(this.properties, this.communityId).subscribe(layout => {
       this.publishedLayout = (layout?layout:new Layout(this.communityId,this.defaultCustomizationOptions));
       this.publishedCustomizationOptions = (layout?CustomizationOptions.checkForObsoleteVersion(layout.layoutOptions,this.communityId):Object.assign({},this.defaultCustomizationOptions));
       this.initializeCustomizationOptions(true);
      }, error => {
        this.publishedCustomizationOptions = new CustomizationOptions(CustomizationOptions.getIdentity(this.communityId).mainColor,CustomizationOptions.getIdentity(this.communityId).secondaryColor);
        this.initializeCustomizationOptions(true);
         UIkit.notification("An error occured fetching customizations options", {
           status: 'danger',
           timeout: 6000,
           pos: 'bottom-right'
         });
      }));
    }));



  }
  formHasChanges(obj1:CustomizationOptions, obj2:CustomizationOptions){
    return obj1.backgrounds.form.imageUrl != obj2.backgrounds.form.imageUrl ||
      obj1.backgrounds.form.imageFile != obj2.backgrounds.form.imageFile ||
      obj1.backgrounds.form.position != obj2.backgrounds.form.position;
  }
  hasChanges(object1,object2):boolean{
    return JSON.stringify(object1) != JSON.stringify(object2);
  }
  saveLayout() {
    this.publishedLayout.layoutOptions =  this.copyObject(this.draftCustomizationOptions);
    this.publishedCustomizationOptions =  this.copyObject(this.publishedLayout.layoutOptions)
    this.publishedLayout.date = new Date();
    this.subscriptions.push(this.layoutService.createCSS(this.communityId,this.properties.connectPortalUrl, this.publishedLayout.date.valueOf(), this.publishedLayout.layoutOptions).subscribe(data => {
      this.subscriptions.push(this.layoutService.saveLayout(this.properties, this.communityId, this.publishedLayout).subscribe(layout => {
        this.publishedLayout._id = layout._id;
        this._clearCacheService.purgeBrowserCache("Layout added/ updated", this.communityId);
        setTimeout(() => {
          this.initializeCustomizationOptions(JSON.stringify(this.publishedCustomizationOptions) != this.previewCustomization);
        }, 4000);
        this.deleteOldImages();
        UIkit.notification("Customizations was successfully saved!", {
          status: 'success',
          timeout: 6000,
          pos: 'bottom-right'
        });
      }, error => {

        UIkit.notification("An error occurred on save css", {
          status: 'danger',
          timeout: 6000,
          pos: 'bottom-right'
        });
      }));
    }, error => {

      UIkit.notification("An error occurred on save layout", {
        status: 'danger',
        timeout: 6000,
        pos: 'bottom-right'
      });
    }));
  }
  private deleteOldImages( ) {
    if( this.publishedCustomizationOptions.backgrounds.form.imageFile && this.draftCustomizationOptions.backgrounds.form.imageFile !=this.publishedCustomizationOptions.backgrounds.form.imageFile){
       this.deleteImage(this.publishedCustomizationOptions.backgrounds.form.imageFile)
    }
  }
  private deleteDraftImages( ) {
    if( this.draftCustomizationOptions.backgrounds.form.imageFile && this.draftCustomizationOptions.backgrounds.form.imageFile !=this.publishedCustomizationOptions.backgrounds.form.imageFile){
      this.subscriptions.push(this.utilsService.deletePhoto(this.properties.utilsService + '/delete/stakeholder/' + this.draftCustomizationOptions.backgrounds.form.imageFile).subscribe(
        re => {
          this.cleanUp()
        }
      ));
    }else{
      this.cleanUp()
    }
  }
  private deleteImage( filename:string) {
    this.subscriptions.push(this.utilsService.deletePhoto(this.properties.utilsService + '/delete/stakeholder/' + filename).subscribe());
  }
  initializeCustomizationOptions(updatePreviewUrl) {
    this.draftCustomizationOptions = this.copyObject(this.publishedCustomizationOptions);
    this.appliedCustomizationOptions = this.copyObject(this.publishedCustomizationOptions);
    if(updatePreviewUrl){
      this.previewCustomization = this.copyObject(this.publishedCustomizationOptions);
      this.previewUrl = this.getCommunityUrlSatinized(null);
    }

  }

  applyLayout() {
    this.appliedCustomizationOptions = this.copyObject(this.draftCustomizationOptions);
    if(JSON.stringify(this.appliedCustomizationOptions) != this.previewCustomization){
      let d = new Date();
      let prefix = "-preview-" + d.valueOf();
      this.subscriptions.push(this.layoutService.createCSS(this.communityId, this.properties.connectPortalUrl, prefix, this.appliedCustomizationOptions).subscribe(data => {

        setTimeout(() => {
          this.previewUrl = this.getCommunityUrlSatinized(this.communityId + prefix);
        }, 4000);

      }, error => {

        UIkit.notification("An error occurred preview layout", {
          status: 'danger',
          timeout: 6000,
          pos: 'bottom-right'
        });
      }));

    }
  }

  resetLayout() {
    this.deleteDraftImages();
    this.initializeCustomizationOptions(JSON.stringify(this.publishedCustomizationOptions) != this.previewCustomization);
  }
  resetBackgroundsTo(backgrounds) {
    this.draftCustomizationOptions.backgrounds.dark.color = backgrounds.dark.color;
    this.draftCustomizationOptions.backgrounds.light.color = backgrounds.light.color;
    this.draftCustomizationOptions.backgrounds.form.color = backgrounds.form.color;
    this.resetImageTo(backgrounds);

  }
  resetImageTo(backgrounds) {
    this.deleteDraftImages();
    this.draftCustomizationOptions.backgrounds.form.imageUrl = backgrounds.form.imageUrl;
    this.draftCustomizationOptions.backgrounds.form.imageFile = backgrounds.form.imageFile;
    this.draftCustomizationOptions.backgrounds.form.position = backgrounds.form.position;
  }

  resetBackgroundsAndButtonsTo(c:CustomizationOptions){
    this.resetBackgroundsTo(c.backgrounds);
    this.resetButtonsTo(c.buttons);
   }
  resetBackgroundsAndButtonsToPublished(){
    this.resetBackgroundsAndButtonsTo(this.publishedCustomizationOptions);
  }
  resetButtonsToDefault(){
    this.resetButtonsTo(this.defaultCustomizationOptions.buttons);
    this.updateButtonsBasedOnIdentity();
  }
  resetBackgroundsToDefault(){
    this.resetBackgroundsTo(this.defaultCustomizationOptions.backgrounds);
    this.updateBackgroundsBasedOnIdentity();
  }
  resetButtonsTo(buttonsToRevert) {
    this.draftCustomizationOptions.buttons= this.copyObject(buttonsToRevert);
  }

  resetIdentityTo( original:CustomizationOptions) {
    this.draftCustomizationOptions.identity.mainColor = this.copyObject(original.identity.mainColor);
    this.draftCustomizationOptions.identity.secondaryColor = this.copyObject(original.identity.secondaryColor);
  }
  resetIdentityToPublished() {
    this.resetIdentityTo( this.publishedCustomizationOptions);
    this.updateButtonsBasedOnIdentity();
    this.updateBackgroundsBasedOnIdentity();
  }
  resetIdentityToDefault() {
    this.resetIdentityTo( this.defaultCustomizationOptions);
    this.draftCustomizationOptions.identityIsCustom = false;
    this.updateButtonsBasedOnIdentity();
    this.updateBackgroundsBasedOnIdentity();

  }
  updateButtonsBasedOnIdentity(){
    let tmp =   new CustomizationOptions(this.draftCustomizationOptions.identity.mainColor, this.draftCustomizationOptions.identity.secondaryColor);
    if(!this.draftCustomizationOptions.buttonsIsCustom) {
      this.resetButtonsTo(tmp.buttons);
    }
  }
  updateBackgroundsBasedOnIdentity(){
    let tmp =   new CustomizationOptions(this.draftCustomizationOptions.identity.mainColor, this.draftCustomizationOptions.identity.secondaryColor);
    if(!this.draftCustomizationOptions.backgroundsIsCustom) {
      this.resetBackgroundsTo(tmp.backgrounds);
    }
  }
  getCommunityUrlSatinized(prefix: string) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(this.getCommunityUrl(prefix));
  }
  getCommunityUrl(prefix) {
    if(this.properties.environment == 'production' || this.properties.environment == 'beta')
    return 'https://'+ (this.properties.environment == 'production'?'':'beta.')+this.communityId+'.openaire.eu' + (prefix?("?previewLayout=" + prefix):"");
    return this.properties.connectPortalUrl + (prefix?("?previewLayout=" + prefix):"");
  }



  copyObject(obj) {
    return JSON.parse(JSON.stringify(obj));
  }
  
  changeMenu(  menuSelected){
    this.menuSelected = menuSelected;
  }
  close(){
    if(this.menuSelected.id !='home'){
      this.menuSelected = this.homeMenu;
      return;
    }
    if(!this.hasChanges(this.publishedCustomizationOptions, this.draftCustomizationOptions)) {
      this._router.navigate(["../info/profile"], {relativeTo: this.route});
    }else{
      this.closeModal.alertTitle = "Discard changes";
      this.closeModal.message = "Unsaved changes will be lost.";
      this.closeModal.okButtonText = "Leave";
      this.closeModal.cancelButtonText = "Cancel";
      this.closeModal.okButtonLeft = false;
      this.closeModal.open();
    }
  }

  confirmClose(){
    this._router.navigate(["../info/profile"], {relativeTo: this.route});
  }
}
