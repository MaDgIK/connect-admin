import {Component, HostBinding, Input} from '@angular/core';
import {DomSanitizer} from '@angular/platform-browser';
import {ButtonsCustomization} from '../../openaireLibrary/connect/community/CustomizationOptions';


@Component({
  selector: 'quick-look-buttons',
  template: `
     
    <div>
      <a class="uk-button preview-button" >Button</a>
    </div>
    
       
`,
  styles:[`
 .preview-button {
     color: var(--color);
     background-color: var(--background-color);
     border-color: var(--border-color);
     border-style: solid;
     border-width: var(--border-width);
     border-radius: var(--border-radius);
 }
 .preview-button:hover {
     color: var(--color-hover);
     background-color: var(--background-color-hover);
     border-color: var(--border-color-hover);
 
 }
 
  `]

})

export class QuickLookButtonsComponent {
  @Input() buttons:ButtonsCustomization;
  @Input() border:ButtonsCustomization;

  @HostBinding("attr.style")
  public get valueAsStyle(): any {
    return this.sanitizer.bypassSecurityTrustStyle(`
    --color: ${this.buttons.color};
    --background-color: ${this.buttons.backgroundColor};
    --border-color: ${this.buttons.borderColor};
    --border-style: ${this.border.borderStyle};
    --border-width: ${this.border.borderWidth + 'px'};
    --border-radius: ${this.border.borderRadius + 'px'}; 
     --color-hover: ${this.buttons.onHover.color};
    --background-color-hover: ${this.buttons.onHover.backgroundColor};
    --border-color-hover: ${this.buttons.onHover.borderColor};
    `);
  }
  constructor(   private sanitizer: DomSanitizer) {
  }



}
