import {Component, HostBinding, Input} from '@angular/core';
import {DomSanitizer} from '@angular/platform-browser';


@Component({
  selector: 'quick-look',
  template: `
   <div *ngIf="preview=='identity'">
    <ul class="uk-pagination uk-flex-center" uk-margin="">
      <li class="uk-first-column"><a><span class="uk-icon uk-pagination-previous" uk-pagination-previous=""></span></a></li>
      <li><a >1</a></li>
      <li><a class="uk-active" >2</a></li>
      <li><a>3</a></li>
      <li><a><span
        class="uk-icon uk-pagination-next" uk-pagination-next=""> </span></a></li>
    </ul>
    <hr>
    <ul class="uk-tab">
      <li class="uk-active"><a >Active</a></li>
      <li><a>Item</a></li>
    </ul>
    <div class="portal-color">Text in primary
      color
    </div>
    <div class="portal-secondary-color">Text in secondary
      color
    </div>
    <div >
      <a class="link1">Link 1</a>
    </div>
    <div >
      <a class="portal-link">Link 2</a>
    </div>
   </div>
    
    
  `,
  styles:[`
      /*Override inside this component*/
/*      :host {
          --portal-main-color: #fe3c52;
          --portal-dark-color: #b40238;
          --new:#fe3c52;
 
      }*/
      /*
      overrides root
      ::ng-deep :root {
          --portal-main-color: #fe3c52;
          --portal-dark-color: #b40238;
          --new:#fe3c52;      
      }*/
   .uk-pagination li a.uk-active{
       background-color: var(--portal-main-color);
       color:white;
   }
    .uk-pagination li a, .uk-tab li a{
        text-decoration: none;
    }
    a.link1{
        color: var(--portal-main-color);
    }
    a.link1:hover{
        color: var(--portal-dark-color);
    }
    .uk-tab li.uk-active a{
        color: var(--portal-main-color)  !important;
        border-color:var(--portal-main-color) !important;
    }
 
  `]

})

export class QuickLookComponent {
  @Input() primaryColor;
  @Input() secondaryColor;
  @Input() preview :"identity"|"backgrounds"|"buttons" = "identity";

  constructor(   private sanitizer: DomSanitizer) {
  }

  @HostBinding("attr.style")
  public get valueAsStyle(): any {
    return this.sanitizer.bypassSecurityTrustStyle(`--portal-main-color: ${this.primaryColor};  --portal-dark-color: ${this.secondaryColor};`);
  }

}
