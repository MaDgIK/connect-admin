import { NgModule } from '@angular/core';
import {RouterModule} from '@angular/router';
import {ManageCommunitiesComponent} from './manage-communities.component';

@NgModule({
  imports: [
    RouterModule.forChild([
      { path: '', component: ManageCommunitiesComponent}
    ])
  ]
})
export class ManageCommunitiesRoutingModule { }
