import {NgModule} from '@angular/core';
import {ManageCommunitiesComponent} from './manage-communities.component';
import {ManageCommunitiesRoutingModule} from './manage-communities-routing.module';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {LoginGuard} from '../../openaireLibrary/login/loginGuard.guard';
import {PageContentModule} from '../../openaireLibrary/dashboard/sharedComponents/page-content/page-content.module';
import {LoadingModule} from '../../openaireLibrary/utils/loading/loading.module';
import {UrlPrefixModule} from '../../openaireLibrary/utils/pipes/url-prefix.module';
import {CommunitiesService} from '../../openaireLibrary/connect/communities/communities.service';
import {IconsModule} from '../../openaireLibrary/utils/icons/icons.module';
import {IconsService} from '../../openaireLibrary/utils/icons/icons.service';
import {earth, incognito, restricted} from '../../openaireLibrary/utils/icons/icons';
import {LogoUrlPipeModule} from "../../openaireLibrary/utils/pipes/logoUrlPipe.module";
import {SearchInputModule} from '../../openaireLibrary/sharedComponents/search-input/search-input.module';

@NgModule({
  imports: [
    CommonModule, ManageCommunitiesRoutingModule, RouterModule, PageContentModule, LoadingModule, UrlPrefixModule, IconsModule, LogoUrlPipeModule, SearchInputModule
  ],
  declarations: [ManageCommunitiesComponent],
  providers: [LoginGuard, CommunitiesService],
  exports: [ManageCommunitiesComponent]
})
export class ManageCommunitiesModule {
  constructor(private iconsService: IconsService) {
    this.iconsService.registerIcons([earth, restricted, incognito]);
  }
}
