import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Session, User} from '../../openaireLibrary/login/utils/helper.class';
import {EnvProperties} from '../../openaireLibrary/utils/properties/env-properties';
import {ActivatedRoute} from '@angular/router';
import {CommunitiesService} from '../../openaireLibrary/connect/communities/communities.service';
import {CommunityInfo} from '../../openaireLibrary/connect/community/communityInfo';
import {UserManagementService} from '../../openaireLibrary/services/user-management.service';
import {Title} from '@angular/platform-browser';
import {properties} from "../../../environments/environment";
import {Subscription} from "rxjs";
import {SearchInputComponent} from '../../openaireLibrary/sharedComponents/search-input/search-input.component';
import {UntypedFormBuilder, UntypedFormGroup} from '@angular/forms';
import {SearchUtilsClass} from '../../openaireLibrary/searchPages/searchUtils/searchUtils.class';
import {ErrorCodes} from '../../openaireLibrary/utils/properties/errorCodes';

type Tab = 'all' | 'communities' | 'ris';

@Component({
  selector: 'manage-communities',
  template: `
    <div page-content [id]="tab">
      <div header>
        <ul class="uk-tab uk-margin-medium-top" uk-tab>
          <li [class.uk-active]="tab === 'all'"><a (click)="tab = 'all'"><span class="title">All</span></a></li>
          <li [class.uk-active]="tab === 'communities'"><a (click)="tab = 'communities'"><span class="title">Research Communities</span></a>
          </li>
          <li [class.uk-active]="tab === 'ris'"><a (click)="tab = 'ris'"><span class="title">Research Initiatives</span></a>
          </li>
        </ul>
      </div>
      <div actions>
        <div class="uk-section-xsmall">
          <div class="uk-flex uk-flex-right@m uk-flex-center uk-flex-wrap uk-flex-middle">
            <div search-input [searchControl]="filterForm.get('keyword')" [expandable]="true"
                 placeholder="Search Communities" searchInputClass="outer"
                 class="uk-width-1-3@xl uk-width-2-5@l uk-width-1-2@m uk-width-1-1 uk-flex uk-flex-right"></div>
          </div>
        </div>
      </div>
      <div inner>
        <div class="uk-section uk-section-small uk-position-relative" style="min-height: 60vh">
          <div *ngIf="loading" class="uk-position-center">
            <loading></loading>
          </div>
          <div *ngIf="!loading" uk-height-match="target: .titleContainer; row: false">
            <div *ngIf="noResults" class="uk-section uk-padding-remove-top">
              <div class="uk-card uk-card-default uk-padding-large uk-text-center uk-margin-bottom uk-text-bold">
                <div>No communities to manage yet</div>
              </div>
            </div>
            <div uk-height-match="target: .logoContainer; row: false">
              <div *ngIf="tab != 'ris' && communities.length > 0" class="uk-section uk-padding-remove-top">
                <h4>Research Communities</h4>
                <div class="uk-grid uk-child-width-1-4@xl uk-child-width-1-3@l uk-child-width-1-2@m uk-grid-match" uk-grid>
                  <ng-template ngFor [ngForOf]="filteredCommunities" let-community let-i="index">
                    <ng-container *ngTemplateOutlet="communityBox; context: {community:community}"></ng-container>
                  </ng-template>
                </div>
                <div *ngIf="filteredCommunities.length == 0"
                     class="uk-card uk-card-default uk-card-no-height uk-padding-large uk-text-center uk-margin-bottom uk-text-bold">
                  No communities found
                </div>
              </div>
              <div *ngIf="tab != 'communities' && ris.length > 0" [class.uk-padding-remove-top]="tab == 'ris'" class="uk-section">
                <h4>Research Initiatives</h4>
                <div class="uk-grid uk-grid-match uk-child-width-1-4@xl uk-child-width-1-3@l uk-child-width-1-2@m" uk-grid>
                  <ng-template ngFor [ngForOf]="filteredRis" let-community let-i="index">
                    <ng-container *ngTemplateOutlet="communityBox; context: {community:community}"></ng-container>
                  </ng-template>
                </div>
                <div *ngIf="filteredRis.length == 0"
                     class="uk-card uk-card-default uk-card-no-height uk-padding-large uk-text-center uk-margin-bottom uk-text-bold">
                  No initiatives found
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <ng-template #communityBox let-community="community">
      <div *ngIf="community">
        <div class="uk-card uk-card-default uk-card-body uk-position-relative">
          <div class="uk-position-top-right uk-margin-small-right uk-margin-small-top">
            <div class="uk-flex uk-flex-middle">
              <icon [flex]="true" [name]="community.isPublic()?'earth':(community.isRestricted()?'restricted':'incognito')" ratio="0.6"></icon>
            </div>
          </div>
          <a class="uk-display-block uk-text-center uk-link-reset" [routerLink]="community.communityId">
            <div class="titleContainer uk-h6 uk-margin-remove-bottom uk-margin-top multi-line-ellipsis lines-2">
              <p *ngIf="community.shortTitle" class="uk-margin-remove">
                {{community.shortTitle}}
              </p>
            </div>
            <div class="logoContainer uk-margin-top uk-flex uk-flex-column uk-flex-center uk-flex-middle">
              <img [src]="community | logoUrl" class="uk-blend-multiply"
                   style="max-height: 80px;">
            </div>
          </a>
        </div>
      </div>
    </ng-template>
  `
})
export class ManageCommunitiesComponent implements OnInit, OnDestroy {
  public properties: EnvProperties = properties;
  public communities: CommunityInfo[] = [];
  public filteredCommunities: CommunityInfo[] = [];
  public ris: CommunityInfo[] = [];
  public filteredRis: CommunityInfo[] = [];
  public loading: boolean;
  public user: User;
  public tab: Tab = "all";
  private subscriptions: any[] = [];
  public communitySearchUtils: SearchUtilsClass = new SearchUtilsClass();
  public errorCodes: ErrorCodes;
  // Search
  @ViewChild('searchInputComponent') searchInputComponent: SearchInputComponent;
  filterForm: UntypedFormGroup;
  private searchText: string = '';
  
  constructor(private route: ActivatedRoute,
              private _fb: UntypedFormBuilder,
              private communitiesService: CommunitiesService,
              private title: Title,
              private userManagementService: UserManagementService) {
    this.errorCodes = new ErrorCodes();
    this.communitySearchUtils.status = this.errorCodes.LOADING;
  }
  
  ngOnInit() {
    this.communitySearchUtils.keyword = "";
    
    this.filterForm = this._fb.group({
      keyword: [''],
    });
    
    this.subscriptions.push(this.filterForm.get('keyword').valueChanges.subscribe(value => {
      this.searchText = value ? value.toLowerCase() : '';
      this.applyFilters();
    }));
    
    this.loading = true;
    this.title.setTitle('Administrator Dashboard | Manage Communities');
    this.subscriptions.push(this.userManagementService.getUserInfo().subscribe(user => {
      this.user = user;
      if (this.user) {
        this.subscriptions.push(this.communitiesService.getCommunities(this.properties, this.properties.communityAPI + 'communities').subscribe(
          communities => {
            this.communities = [];
            this.filteredCommunities = [];
            this.ris = [];
            this.filteredRis = [];
            communities.forEach(community => {
              if (Session.isPortalAdministrator(this.user) || Session.isCommunityCurator(this.user) || Session.isManager('community', community.communityId, this.user)) {
                if (community.type === 'community') {
                  this.communities.push(community);
                  this.filteredCommunities.push(community);
                } else {
                  this.ris.push(community);
                  this.filteredRis.push(community);
                }
              }
            })
            this.loading = false;
          }));
      } else {
        this.loading = false;
      }
    }));
  }
  
  ngOnDestroy() {
    this.subscriptions.forEach(subscription => {
      if (subscription instanceof Subscription) {
        subscription.unsubscribe();
      }
    });
  }
  
  public get noResults(): boolean {
    return (this.tab === "all" && this.communities.length === 0 && this.ris.length === 0) ||
      (this.tab === "communities" && this.communities.length === 0) ||
      (this.tab === "ris" && this.ris.length === 0);
  }
  
  public applyFilters() {
    this.filteredCommunities = this.communities.filter(community => community.shortTitle.toLowerCase().includes(this.searchText) || community.title.toLowerCase().includes(this.searchText) || community.communityId.toLowerCase().includes(this.searchText));
    this.filteredRis = this.ris.filter(community => community.shortTitle.toLowerCase().includes(this.searchText) || community.title.toLowerCase().includes(this.searchText) || community.communityId.toLowerCase().includes(this.searchText));
  }
  
  public onSearchClose() {
    this.communitySearchUtils.keyword = this.filterForm.get('keyword').value;
  }
  
  public resetInput() {
    this.communitySearchUtils.keyword = null;
    this.searchInputComponent.reset()
  }
}
