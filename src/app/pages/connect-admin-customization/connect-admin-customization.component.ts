import {Component, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {CustomizationService} from "../../openaireLibrary/services/customization.service";
import {properties} from "../../../environments/environment";
import {Layout} from "../../openaireLibrary/connect/community/CustomizationOptions";
import {ClearCacheService} from "../../openaireLibrary/services/clear-cache.service";
import {CommunitiesService} from "../../openaireLibrary/connect/communities/communities.service";

declare var UIkit;

@Component({
  selector: 'connect-admin-customization',
  template: `
    <div page-content>
      <div header>
        <admin-tabs tab="customization" portal="connect"></admin-tabs>
      </div>
      <div inner>
        <div>
          <div class="uk-alert uk-alert-warning uk-margin-top ">
            <div class="uk-text-large">Use only after connect deployment. And only if there are css updates!</div>
            Press the following button to update the timestamp in the saved layouts and recreate the css files. 
          </div>
          <button (click)="updateCss()" class="uk-button uk-button-danger" [class.uk-disabled]="savingChanges"> Update CSS</button>
          <hr>
        </div>
        <div class="uk-margin-medium-top">
          <div>Purge ICM cache for all communities</div>

          <button (click)="purgeBrowserCache()" class="uk-button uk-button-danger uk-margin-top" [class.uk-disabled]="savingChanges"> Purge Browser Cache</button>
          <hr>
        </div>
        <div class="uk-margin-medium-top">
          <div>Reload cache requests either per community or for all communities</div>  
          <div input [(value)]="community" [placeholder]="'e.g. galaxy'" class="uk-width-1-2 uk-margin-top"></div>
          <div input [(value)]="route" [placeholder]="'Route e.g. /'" class="uk-width-1-2 uk-margin-top"></div>
        
        <button (click)="forceReloadCache()" class="uk-button uk-button-danger uk-margin-top" [class.uk-disabled]="savingChanges"> Reload Cache</button>
        </div>
        <!--<ng-container *ngIf="properties.domain != 'production'">
          <hr>
          <div class="uk-alert uk-alert-danger uk-margin-top ">
            Delete after next deployment
          </div>
          <div class="uk-alert uk-alert-warning uk-margin-top ">
            <div class="uk-text-large">Use to clean up multiple layouts of a community id.</div>
          </div>
          <div input [(value)]="community" [placeholder]="'e.g. galaxy'"></div>
          <button (click)="deleteLayout()" class="uk-button uk-button-danger" [class.uk-disabled]="!community"> Delete layout</button>
        </ng-container>-->
      </div>
    </div>
    
  `
})
export class ConnectAdminCustomizationComponent implements OnInit {
  private subscriptions: any[] = [];
  savingChanges = false;
  cssCreated: number = 0;
  totalLayouts: number = 0;
  community;
  route;
  properties = properties;

  constructor( private customizationService: CustomizationService, private _clearCacheService: ClearCacheService,
               private communitiesService: CommunitiesService) {
  }

  ngOnInit() {
  }
  ngOnDestroy() {

    this.subscriptions.forEach(subscription => {
      if (subscription instanceof Subscription) {
        subscription.unsubscribe();
      }
    });
  }
  updateCss(){
    let date = new Date();
    this.savingChanges = true;
    let connectCssIsSaved = false;
    let defaultCssIsSaved = false;
    this.subscriptions.push(this.customizationService.getLayouts(properties, ).subscribe(layouts => {

      this.totalLayouts = layouts ? layouts.length : 0;
      for(let layout of layouts){
        layout.date = date;
        if(layout.portalPid == 'connect'){
          connectCssIsSaved = true;
          this.callSaveAndCreate(layout, "connect")
        }else if(layout.portalPid == 'default'){
          defaultCssIsSaved = true;
          this.callSaveAndCreate(layout, "connect")
        }else {
          this.callSaveAndCreate(layout, "community");
        }
      }
      if(!connectCssIsSaved){
        this.totalLayouts++;
        let layout =  new Layout("connect", null);
        layout.date = date;
        this.callSaveAndCreate(layout, "connect")
      }
      if(!defaultCssIsSaved){
        this.totalLayouts++;
        let layout =  new Layout("default", null);
        layout.date = date;
        this.callSaveAndCreate(layout, "connect")
      }
    }));
  }

  callSaveAndCreate(layout, portalType){
    this.subscriptions.push(this.customizationService.saveLayout(properties, layout.portalPid, layout, portalType).subscribe(data =>{
      this.subscriptions.push(this.customizationService.createCSS(layout.portalPid,properties.connectPortalUrl, layout.date.valueOf(), layout.layoutOptions).subscribe(data =>{
        UIkit.notification(layout.portalPid +"-"+ layout.date.valueOf() +  ".css " + " was created successfully! ", {
          status: 'success',
          timeout: 6000,
          pos: 'bottom-right'
        });

        this.cssCreated++;
        if(this.cssCreated == this.totalLayouts) {
          this.savingChanges = false;
        }
      }));
    }));
  }

  purgeBrowserCache() {
    this.savingChanges = true;
    this.subscriptions.push(this.communitiesService.getCommunities(this.properties, this.properties.communityAPI + 'communities').subscribe(
      communities => {
        communities.forEach(community => {
          this._clearCacheService.purgeBrowserCache("Layout added/ updated", community.communityId);
        });
        this.savingChanges = false;
      }
    ));
    this._clearCacheService.purgeBrowserCache("Layout added/ updated", "connect");
  }

  deleteLayout(){
    if(this.community && this.community.length > 0) {
      this.subscriptions.push(this.customizationService.deleteLayout(properties, this.community,  "community").subscribe(data => {

      }));
    }
  }

  forceReloadCache() {
    if (this.community) {
      this._clearCacheService.clearCacheInRoute("Clear cache", this.community, this.route ? this.route : "/")
    } else {
      this.savingChanges = true;
      this.subscriptions.push(this.communitiesService.getCommunities(this.properties, this.properties.communityAPI + 'communities').subscribe(
        communities => {
          communities.forEach(community => {
            this._clearCacheService.clearCacheInRoute("Clear cache for "+ community.communityId, community.communityId, this.route ? this.route : "/")
          });
          this.savingChanges = false;
        }
      ));
    }
  }
}
