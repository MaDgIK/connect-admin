import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {AdminLoginGuard} from "../../openaireLibrary/login/adminLoginGuard.guard";
import {ConnectAdminCustomizationComponent} from "./connect-admin-customization.component";

@NgModule({
  imports: [
    RouterModule.forChild([
      { path: '',component: ConnectAdminCustomizationComponent}
    ])
  ],
  providers: [AdminLoginGuard]
})
export class ConnectAdminCustomizationRoutingModule {
}
