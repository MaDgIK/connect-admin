import {NgModule} from '@angular/core';
import {RouterModule} from "@angular/router";
import {CommonModule} from "@angular/common";
import {ConnectAdminCustomizationComponent} from "./connect-admin-customization.component";
import {AdminTabsModule} from "../../openaireLibrary/dashboard/sharedComponents/admin-tabs/admin-tabs.module";
import {ConnectAdminCustomizationRoutingModule} from "./connect-admin-customization-routing.module";
import {PageContentModule} from "../../openaireLibrary/dashboard/sharedComponents/page-content/page-content.module";
import {CustomizationService} from "../../openaireLibrary/services/customization.service";
import {InputModule} from "../../openaireLibrary/sharedComponents/input/input.module";
import {CommunitiesService} from "../../openaireLibrary/connect/communities/communities.service";

@NgModule({
  imports: [
    CommonModule, RouterModule, AdminTabsModule, ConnectAdminCustomizationRoutingModule, PageContentModule, InputModule
  ],
  providers: [CustomizationService, CommunitiesService],
  declarations: [ConnectAdminCustomizationComponent],
  exports: [ConnectAdminCustomizationComponent]
})
export class ConnectAdminCustomizationModule {
}
