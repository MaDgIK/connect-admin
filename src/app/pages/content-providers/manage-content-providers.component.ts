import {ChangeDetectorRef, Component, OnInit, SimpleChanges, ViewChild} from '@angular/core';
import {RemoveContentProvidersComponent} from './remove-content-providers.component';
import {Title} from '@angular/platform-browser';
import {
  FullScreenModalComponent
} from "../../openaireLibrary/utils/modal/full-screen-modal/full-screen-modal.component";
import {StringUtils} from "../../openaireLibrary/utils/string-utils.class";
import {EnvProperties} from "../../openaireLibrary/utils/properties/env-properties";
import {properties} from "../../../environments/environment";
import {Subscriber} from "rxjs";
import {CommunityInfo} from "../../openaireLibrary/connect/community/communityInfo";
import {CommunityService} from "../../openaireLibrary/connect/community/community.service";
import {ContentProvider} from "../../openaireLibrary/utils/entities/contentProvider";
import {OpenaireEntities} from "../../openaireLibrary/utils/properties/searchFields";
import {CriteriaComponent} from "./criteria/criteria.component";
import {NotificationHandler} from "../../openaireLibrary/utils/notification-handler";
import {ManageCommunityContentProvidersService} from "../../services/manageContentProviders.service";
import {UntypedFormBuilder} from "@angular/forms";

@Component({
  selector: 'manage-content-providers',
  template: `
    <remove-content-providers #removeContentProviders (addContentProviders)="openAddContentProviders()"
                              [communityContentProviders]="communityContentProviders"
                              [loading]="showLoadingInRemove" [community]="community"
                              (editCriteria)="openCriteria($event)"
                              
                              (communityContentProvidersChanged)="communityContentProvidersChanged($event)">
    </remove-content-providers>
    <fs-modal #addContentProvidersModal>
      <add-content-providers #addContentProviders [communityContentProviders]="communityContentProviders"
                             [community]="community"
                             (communityContentProvidersChanged)="communityContentProvidersChanged($event)"></add-content-providers>
    </fs-modal>
    <fs-modal #filtersModal (okEmitter)="saveCriteria()" (cancelEmitter)="criteria.reset(); depositReset();"
              [okButtonDisabled]="(criteria && criteria.disabled && !depositInfoChanged)">
      <div class="uk-container uk-container-large">
        <div *ngIf="dataProvider" class="uk-text-large">{{dataProvider.officialname}}</div>
        <div class="uk-flex uk-flex-center">
          <div>
          <mat-slide-toggle [checked]="enabled" (change)="enabled = !enabled; depositInfoChanged = true;"></mat-slide-toggle>
          <label class="uk-margin-medium-top uk-margin-small-left ">Content source</label>
          </div>
          <div class="uk-margin-small-left">
          <mat-slide-toggle [checked]="deposit" (change)="deposit = !deposit; depositInfoChanged = true;"></mat-slide-toggle>
          <label class="uk-margin-medium-top uk-margin-small-left ">Suggest for deposit</label>
          </div>
        </div>
        <div   class="uk-hr" [class.uk-hidden]="!enabled">
        <div class="uk-text-center uk-text-bold uk-text-large uk-margin-top ">Content filters</div>
        <criteria #criteria *ngIf="dataProvider" [height]="'-1'"
                  [selectionCriteria]="dataProvider.selectioncriteria">
          <div no-criteria>
            <h5 class="uk-margin-small-bottom">No Filters for {{dataProvider.officialname}} yet</h5>
            <i class="uk-display-block">If no Filters are specified, all {{openaireEntities.RESULTS | lowercase}} of this {{openaireEntities.DATASOURCE | lowercase}} will be included in your
              {{openaireEntities.COMMUNITY | lowercase}}.
            </i>
          </div>
        </criteria>
        </div>
        <div *ngIf="deposit" class="uk-hr">
          
          <!--<div class="uk-width-1-1 uk-margin-medium-top" input placeholder="Type a message or instruction for researchers about deposition in this datasource"
               [value]="message" (valueChange)="message=$event; depositInfoChanged = true;">
          </div>-->
          <div class="uk-margin-top uk-text-meta">Type a message or instruction for researchers about deposition in this datasource</div>
          <ckeditor [readonly]="false"
                    debounce="500"
                    [formControl]="messageForm" (change)="messageChanged()"
                    [config]="{ extraAllowedContent: '* [uk-*](*) ; span', disallowedContent: 'script; *[on*]', 
                                        removeButtons:  'Save,NewPage,DocProps,Preview,Print,' +
                                                        'Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,' +
                                                        'CreateDiv,Flash,PageBreak,' +
                                                        'Subscript,Superscript,Anchor,Smiley,Iframe,Styles,Font,About,Language,JustifyLeft,JustifyRight,JustifyCenter,JustifyBlock,FontSize,TextColor,BGColor',
                                        extraPlugins: 'divarea'}">
          </ckeditor>
        </div>
      </div>
    </fs-modal>
  `
})

export class ManageContentProvidersComponent implements OnInit {
  communityContentProviders: ContentProvider[] = [];
  @ViewChild(RemoveContentProvidersComponent) removeContentProvidersComponent: RemoveContentProvidersComponent;
  @ViewChild('criteria') criteria: CriteriaComponent;
  @ViewChild('addContentProvidersModal', { static: true }) addContentProvidersModal: FullScreenModalComponent;
  @ViewChild('filtersModal', { static: true }) filtersModal: FullScreenModalComponent;
  @ViewChild('depositModal', { static: true }) depositModal: FullScreenModalComponent;
  private subscriptions: any[] = [];
  public showLoadingInRemove: boolean = true;
  public body: string = "Send from page";
  public properties: EnvProperties = properties;
  public openaireEntities = OpenaireEntities;
  public community: CommunityInfo;
  public dataProvider: ContentProvider;
  message = "";
  deposit= false;
  enabled = false;
  depositInfoChanged =false;
  messageForm = this.fb.control("");
  constructor(private title: Title,
              private cdr: ChangeDetectorRef,
              private communityService: CommunityService,
              private manageCommunityContentProvidersService: ManageCommunityContentProvidersService,
    private fb: UntypedFormBuilder) {
  }

  ngOnInit() {
    this.subscriptions.push(this.communityService.getCommunityAsObservable().subscribe(community => {
      this.community = community;
      if (this.community) {
        this.title.setTitle(this.community.shortTitle.toUpperCase() + ' | ' + OpenaireEntities.DATASOURCES);
        this.body = "[Please write your message here]";
        this.body = StringUtils.URIEncode(this.body);
      }
    }));
  }

  public ngOnDestroy() {
    this.subscriptions.forEach(sub => {
      if (sub instanceof Subscriber) {
        sub.unsubscribe();
      }
    });
  }
  
  public openAddContentProviders() {
    this.addContentProvidersModal.title = "Search and Add " + OpenaireEntities.DATASOURCES;
    this.addContentProvidersModal.okButtonText = "Done";
    this.addContentProvidersModal.back = true;
    this.addContentProvidersModal.okButton = true;
    this.addContentProvidersModal.open();
  }
  
  public openCriteria(contentProvider: ContentProvider) {
    this.dataProvider = contentProvider;
    this.deposit = this.dataProvider.deposit;
    this.message = this.dataProvider.message;
    this.enabled = this.dataProvider.enabled;
    this.messageForm = this.fb.control(this.message);
    this.depositInfoChanged = false;
    this.filtersModal.title = 'Edit datasource options';
    this.filtersModal.okButtonText = "Save";
    this.filtersModal.back = true;
    this.filtersModal.okButton = true;

    this.filtersModal.open();
    this.cdr.detectChanges();
  }
  public saveCriteria() {
    let callback = (selectionCriteria): void => {
      this.dataProvider.selectioncriteria = selectionCriteria;
      this.dataProvider.deposit = this.deposit;
      this.dataProvider.message = this.message;
      this.dataProvider.enabled = this.enabled;
      console.log(this.dataProvider)
      this.manageCommunityContentProvidersService.saveContentProvider(this.properties, this.dataProvider).subscribe(() => {
        this.criteria.reset();
        this.removeContentProvidersComponent.getCommunityContentProviders();
        this.criteria.loading = false;
        NotificationHandler.rise('Filters have been <b>successfully updated</b>');
      }, error => {
        this.criteria.loading = false;
        this.criteria.handeError('An error has been occurred. Try again later!', error);
      });
    }
    this.removeContentProvidersComponent.loading = true;
    console.log()
    this.criteria.save(callback);
  }

  public communityContentProvidersChanged($event) {
    this.communityContentProviders = $event.value;
    this.showLoadingInRemove = false;
    if (this.addContentProvidersModal.isOpen) {
      this.removeContentProvidersComponent.applyFilters();
    }
  }


  depositReset(){
    this.message = this.dataProvider.message;
    this.messageForm.setValue(this.message);
    this.deposit = this.dataProvider.deposit;
    this.enabled = this.dataProvider.enabled;
  }

  messageChanged(){
    this.message = this.messageForm.value;
    this.depositInfoChanged = true;
  }
}
