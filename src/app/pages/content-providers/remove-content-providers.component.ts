import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {Subscriber} from 'rxjs';

import {ErrorCodes} from '../../openaireLibrary/utils/properties/errorCodes';
import {EnvProperties} from '../../openaireLibrary/utils/properties/env-properties';
import {ManageCommunityContentProvidersService} from '../../services/manageContentProviders.service';
import {SearchCommunityDataprovidersService} from '../../openaireLibrary/connect/contentProviders/searchDataproviders.service';
import {RouterHelper} from '../../openaireLibrary/utils/routerHelper.class';
import {ContentProvider, Criteria} from '../../openaireLibrary/utils/entities/contentProvider';
import {properties} from "../../../environments/environment";
import {SearchInputComponent} from "../../openaireLibrary/sharedComponents/search-input/search-input.component";
import {UntypedFormBuilder, UntypedFormGroup} from "@angular/forms";
import {CriteriaUtils} from "./criteria-utils";
import {CommunityInfo} from "../../openaireLibrary/connect/community/communityInfo";
import {NotificationHandler} from "../../openaireLibrary/utils/notification-handler";
import {AlertModal} from "../../openaireLibrary/utils/modal/alert";
import {OpenaireEntities} from "../../openaireLibrary/utils/properties/searchFields";
import {HelperFunctions} from "../../openaireLibrary/utils/HelperFunctions.class";
import {ClearCacheService} from "../../openaireLibrary/services/clear-cache.service";

@Component({
  selector: 'remove-content-providers',
  templateUrl: './remove-content-providers.component.html'
})
export class RemoveContentProvidersComponent implements OnInit {
  public routerHelper: RouterHelper = new RouterHelper();
  public contentProviderUrl = "https://" + ((properties.environment == "beta" || properties.environment == "development") ? "beta." : "") + "explore.openaire.eu" + properties.searchLinkToDataProvider;
  public previewCommunityContentProviders: ContentProvider[] = [];
  public errorCodes: ErrorCodes;
  public openAIREEntities = OpenaireEntities;
  @Input() public loading: boolean = true;
  @Input() public community: CommunityInfo;
  @Input() public communityContentProviders: ContentProvider[] = [];
  @Output() communityContentProvidersChanged = new EventEmitter();
  private properties: EnvProperties = properties;
  private subscriptions: any[] = [];
  private selectedCommunityContentProvider: any;
  @ViewChild('deleteModal') deleteModal: AlertModal;
  /** Criteria */
  public criteriaUtils: CriteriaUtils = new CriteriaUtils();
  /** Paging */
  page: number = 1;
  resultsPerPage: number = properties.resultsPerPage;
  /** Search */
  @ViewChild('searchInputComponent') searchInputComponent: SearchInputComponent;
  filterForm: UntypedFormGroup;
  private searchText: RegExp = new RegExp('');
  public keyword: string = '';
  @Output() addContentProviders: EventEmitter<void> = new EventEmitter();
  @Output() editCriteria: EventEmitter<ContentProvider> = new EventEmitter<ContentProvider>();

  constructor(private route: ActivatedRoute, private _router: Router,
              private _fb: UntypedFormBuilder,
              private _manageCommunityContentProvidersService: ManageCommunityContentProvidersService,
              private _searchCommunityContentProvidersService: SearchCommunityDataprovidersService,
              private _clearCacheService: ClearCacheService) {
  }
  
  ngOnInit() {
    this.filterForm = this._fb.group({
      keyword: [''],
    });
    this.subscriptions.push(this.filterForm.get('keyword').valueChanges.subscribe(value => {
      this.searchText = new RegExp(value, 'i');
      this.page = 1;
      this.applyFilters();
    }));
    this.contentProviderUrl = "https://"
      + ((this.properties.environment == "beta" || this.properties.environment == "development") ? "beta." : "")
      + this.community.communityId + ".openaire.eu" + this.properties.searchLinkToDataProvider;
    this.getCommunityContentProviders();
  }
  
  public ngOnDestroy() {
    this.subscriptions.forEach(sub => {
      if (sub instanceof Subscriber) {
        sub.unsubscribe();
      }
    });
  }
  
  public confirmedDeleteContentProvider() {
    console.log(this.selectedCommunityContentProvider)
    this.subscriptions.push(this._manageCommunityContentProvidersService.removeContentProvider(this.properties, this.community.communityId, this.selectedCommunityContentProvider.openaireId).subscribe(
      () => {
        let index = this.communityContentProviders.indexOf(this.selectedCommunityContentProvider);
        this.communityContentProviders.splice(index, 1);
        this.applyFilters();
        this._clearCacheService.clearCacheInRoute(this.openAIREEntities.DATASOURCE+" removed", this.community.communityId)
        this._clearCacheService.purgeBrowserCache(this.openAIREEntities.DATASOURCE+" removed", this.community.communityId);
        this.handleSuccess(this.openAIREEntities.DATASOURCE + ' successfully removed!')
        this.communityContentProvidersChanged.emit({
          value: this.communityContentProviders,
        });
      },
      err => {
        this.handleError('An error has been occurred. Try again later!');
        console.error(err);
      }
    ));
  }
  
  public removeContentProvider(communityContentProvider: any) {
    this.selectedCommunityContentProvider = communityContentProvider;
    this.deleteModal.alertTitle = "Remove " + this.openAIREEntities.DATASOURCE;
    let title = "";
    if (communityContentProvider.name) {
      title = communityContentProvider.name;
    }
    if (communityContentProvider.name && communityContentProvider.acronym) {
      title += " (";
    }
    if (communityContentProvider.acronym) {
      title += communityContentProvider.acronym;
    }
    if (communityContentProvider.name && communityContentProvider.acronym) {
      title += ")";
    }
    this.deleteModal.message = this.openAIREEntities.DATASOURCE;
    if (title) {
      this.deleteModal.message += " '" + title + "' ";
    }
    this.deleteModal.message += "will be removed from your community. Are you sure?";
    this.deleteModal.okButtonText = "Yes";
    this.deleteModal.cancelButtonText = "No";
    this.deleteModal.open();
  }
  
  public getCommunityContentProviders() {
    this.communityContentProviders = [];
    this.loading = true;
    this.subscriptions.push(this._searchCommunityContentProvidersService.searchDataproviders(this.properties, this.community.communityId).subscribe(
      data => {
        this.communityContentProviders = data;
        this.previewCommunityContentProviders = this.communityContentProviders;
        this.communityContentProvidersChanged.emit({
          value: this.communityContentProviders,
        });
        this.loading = false;
      },
      err => {
        this.handleError('An error has been occurred. Try again later!', err)
        this.loading = false;
      }
    ));
  }
  
  public updatePage($event) {
    HelperFunctions.scroll();
    this.page = $event.value;
  }
  
  get currentPage(): ContentProvider[] {
    return this.previewCommunityContentProviders.slice((this.page - 1)*this.resultsPerPage, this.page*this.resultsPerPage);
  }
  
  addNew() {
    this.addContentProviders.emit();
  }
  
  public applyFilters() {
    this.previewCommunityContentProviders = this.communityContentProviders.filter(contentProvider => {
      return this.filterCommunityContentProviderByKeyword(contentProvider);
    });
  }
  
  public filterCommunityContentProviderByKeyword(contentProvider): boolean {
    return this.searchText.toString() === ''
      || (contentProvider.name + " " + contentProvider.officialname).match(this.searchText) != null;
  }
  
  handleSuccess(message) {
    NotificationHandler.rise(message);
  }
  
  handleError(message: string, error = null) {
    if (error) {
      console.error(error);
    }
    NotificationHandler.rise(message, 'danger');
  }
  
  openCriteria(contentProvider: ContentProvider) {
    this.editCriteria.emit(contentProvider);
  }

}
