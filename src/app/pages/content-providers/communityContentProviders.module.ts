import {NgModule} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';

import {PagingModule} from '../../openaireLibrary/utils/paging.module';
import {SearchPagingModule} from '../../openaireLibrary/searchPages/searchUtils/searchPaging.module';

import {ErrorMessagesModule} from '../../openaireLibrary/utils/errorMessages.module';
import {AlertModalModule} from '../../openaireLibrary/utils/modal/alertModal.module';
import {CommonModule} from '@angular/common';
import {PageContentModule} from "../../openaireLibrary/dashboard/sharedComponents/page-content/page-content.module";
import {InputModule} from "../../openaireLibrary/sharedComponents/input/input.module";
import {SearchInputModule} from "../../openaireLibrary/sharedComponents/search-input/search-input.module";
import {NoLoadPaging} from "../../openaireLibrary/searchPages/searchUtils/no-load-paging.module";
import {LoadingModule} from "../../openaireLibrary/utils/loading/loading.module";
import {IconsModule} from "../../openaireLibrary/utils/icons/icons.module";
import {IconsService} from "../../openaireLibrary/utils/icons/icons.service";
import {filters} from "../../openaireLibrary/utils/icons/icons";
import {FullScreenModalModule} from "../../openaireLibrary/utils/modal/full-screen-modal/full-screen-modal.module";
import {ResultPreviewModule} from "../../openaireLibrary/utils/result-preview/result-preview.module";
import {
  SearchDataprovidersServiceModule
} from "../../openaireLibrary/connect/contentProviders/searchDataprovidersService.module";
import {ManageContentProvidersComponent} from "./manage-content-providers.component";
import {RemoveContentProvidersComponent} from "./remove-content-providers.component";
import {AddContentProvidersComponent} from "./add-content-providers.component";
import {ManageCommunityContentProvidersService} from "../../services/manageContentProviders.service";
import {SearchDataprovidersService} from "../../openaireLibrary/services/searchDataproviders.service";
import {CriteriaModule} from "./criteria/criteria.module";
import {MatSlideToggleModule} from "@angular/material/slide-toggle";
import {CKEditorModule} from "ng2-ckeditor";
import {ValidateEnabledPageModule} from "../../utils/validateEnabledPage.module";

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    PagingModule, SearchPagingModule,
    ErrorMessagesModule,
    AlertModalModule,
    SearchDataprovidersServiceModule,
    PageContentModule,
    InputModule,
    SearchInputModule,
    RouterModule.forChild([
      {
        path: '', component: ManageContentProvidersComponent
      }
    ]),
    NoLoadPaging, LoadingModule, IconsModule, FullScreenModalModule, ResultPreviewModule, CriteriaModule, MatSlideToggleModule, CKEditorModule, ReactiveFormsModule, ValidateEnabledPageModule
  ],
  declarations: [
    ManageContentProvidersComponent,
    RemoveContentProvidersComponent,
    AddContentProvidersComponent
  ],
  providers: [
    ManageCommunityContentProvidersService,
    SearchDataprovidersService
  ],
  exports: [ManageContentProvidersComponent]
})

export class CommunityContentProvidersModule {
  constructor(private iconsService: IconsService) {
    this.iconsService.registerIcons([filters]);
  }
}
