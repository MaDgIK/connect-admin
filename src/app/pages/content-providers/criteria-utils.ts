import {Option} from "../../openaireLibrary/sharedComponents/input/input.component";
import {Criteria} from "../../openaireLibrary/utils/entities/contentProvider";

export class CriteriaUtils {
  public readonly fields: Option[] = [
    {value: 'author', label: 'Author\'s name'},
    {value: 'title', label: 'Title'},
    {value: 'orcid', label: 'Author\'s ORCID'},
    {value: 'contributor', label: 'Contributor'},
    {value: 'description', label: 'Abstract'},
    {value: 'subject', label: 'Subject'},
    {value: 'fos', label: 'Field of Science'},
    {value: 'sdg', label: 'SDG'},
    {value: 'publisher', label: 'Publisher'},
    {value: 'publicationyear', label: 'Publication Year'}
  ]
  public readonly numericFields: string[] = ['publicationyear'];
  public readonly verbs: Option[] = [
    {value: 'contains', label: 'contains'},
    {value: 'equals', label: 'equals'},
    {value: 'not_contains', label: 'not contains'},
    {value: 'not_equals', label: 'not equals'},
    {value: 'starts_with', label: 'starts with'}
  ]
  public readonly verbsForNumbers: Option[] = [
    {value: 'equals', label: 'equals'},
    {value: 'lesser_than', label: 'lesser than'},
    {value: 'greater_than', label: 'greater than'}
  ]
  
  public getFiltersAsText(criteria: Criteria[], shortVersion=true): string {
    let text = criteria.slice(0, shortVersion?3:criteria.length).map((criterion, index) => (index + 1) + ". " + criterion.constraint.map(constraint => {
      let field = this.fields.find(field => field.value === constraint.field)?.label;
      let matchCase = false;
      if (!constraint.verb.includes('_caseinsensitive')) {
        matchCase = true;
      }
      let verb = [...this.verbs,...this.verbsForNumbers].find(verb => verb.value === constraint.verb.replace("_caseinsensitive", "")).label;
      let value = '"' + constraint.value + '"' + (matchCase ? " (Match case)" : "");
      let expFeature = field =='Publisher' || field == 'Publication Year'? `<span *ngIf="constraint.get('field').value == 'publisher' || constraint.get('field').value == 'publicationyear'" class="
                      uk-margin-xsmall-left uk-padding-remove-vertical uk-label uk-label-warning uk-text-lowercase uk-padding-xsmall">experimental feature</span>`:''
      return field + " " + verb + " " + value  + expFeature;
    }).join(" <b>and</b> "));
    return text.join("<br>");
  }
}
