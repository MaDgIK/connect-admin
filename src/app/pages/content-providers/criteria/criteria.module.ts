import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CriteriaComponent} from './criteria.component';
import {
  SearchCommunityDataprovidersService
} from '../../../openaireLibrary/connect/contentProviders/searchDataproviders.service';
import {RouterModule} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ManageCommunityContentProvidersService} from '../../../services/manageContentProviders.service';
import {PageContentModule} from "../../../openaireLibrary/dashboard/sharedComponents/page-content/page-content.module";
import {IconsModule} from "../../../openaireLibrary/utils/icons/icons.module";
import {LoadingModule} from "../../../openaireLibrary/utils/loading/loading.module";
import {InputModule} from "../../../openaireLibrary/sharedComponents/input/input.module";
import {MatSlideToggleModule} from "@angular/material/slide-toggle";
import {ScrollingModule} from "@angular/cdk/scrolling";
import {NoLoadPaging} from "../../../openaireLibrary/searchPages/searchUtils/no-load-paging.module";
import {PagingModule} from "../../../openaireLibrary/utils/paging.module";

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    PageContentModule,
    IconsModule,
    LoadingModule,
    InputModule,
    MatSlideToggleModule,
    ScrollingModule,
    NoLoadPaging,
    PagingModule,
  ],
    declarations: [
        CriteriaComponent
    ],
    providers: [
        SearchCommunityDataprovidersService,
        ManageCommunityContentProvidersService
    ],
    exports: [CriteriaComponent]
})

export class CriteriaModule {}
