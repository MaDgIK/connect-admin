import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {SearchResult} from '../../openaireLibrary/utils/entities/searchResult';
import {ErrorCodes} from '../../openaireLibrary/utils/properties/errorCodes';
import {SearchUtilsClass} from '../../openaireLibrary/searchPages/searchUtils/searchUtils.class';
import {EnvProperties} from '../../openaireLibrary/utils/properties/env-properties';
import {SearchDataprovidersService} from '../../openaireLibrary/services/searchDataproviders.service';
import {RouterHelper} from '../../openaireLibrary/utils/routerHelper.class';
import {StringUtils} from '../../openaireLibrary/utils/string-utils.class';
import {ManageCommunityContentProvidersService} from '../../services/manageContentProviders.service';
import {properties} from "../../../environments/environment";
import {UntypedFormBuilder, UntypedFormGroup} from "@angular/forms";
import {SearchInputComponent} from "../../openaireLibrary/sharedComponents/search-input/search-input.component";
import {Subscriber} from "rxjs";
import {debounceTime, distinctUntilChanged} from "rxjs/operators";
import {ResultPreview} from "../../openaireLibrary/utils/result-preview/result-preview";
import {NotificationHandler} from "../../openaireLibrary/utils/notification-handler";
import {CommunityInfo} from "../../openaireLibrary/connect/community/communityInfo";
import {ContentProvider} from "../../openaireLibrary/utils/entities/contentProvider";
import {OpenaireEntities} from "../../openaireLibrary/utils/properties/searchFields";
import {ClearCacheService} from "../../openaireLibrary/services/clear-cache.service";

@Component({
  selector: 'add-content-providers',
  templateUrl: './add-content-providers.component.html',
})
export class AddContentProvidersComponent implements OnInit {
  private subscriptions: any[] = [];
  public subResults: any;
  @Input() public community: CommunityInfo;
  public routerHelper: RouterHelper = new RouterHelper();
  public properties: EnvProperties = properties;
  public errorCodes: ErrorCodes;
  public openaireSearchUtils: SearchUtilsClass = new SearchUtilsClass();
  @Output() communityContentProvidersChanged = new EventEmitter();
  @Input() communityContentProviders: ContentProvider[] = [];
  public openaireContentProviders: SearchResult[] = [];
  public openAIREEntities = OpenaireEntities;
  public queryParameters: string = "";
  public resultsPerPage: number = properties.resultsPerPage;
  filterForm: UntypedFormGroup;
  @ViewChild('searchInputComponent') searchInputComponent: SearchInputComponent;
  public contentProviderUrl: string = "https://" + ((properties.environment == "beta" || properties.environment == "development") ? "beta." : "") + "explore.openaire.eu" + properties.searchLinkToDataProvider;
  public body: string = "Send from page";
  
  @Output() toggleView: EventEmitter<any> = new EventEmitter();
  
  constructor(private route: ActivatedRoute, private router: Router,
              private searchDataprovidersService: SearchDataprovidersService,
              private manageCommunityContentProvidersService: ManageCommunityContentProvidersService,
              private _clearCacheService: ClearCacheService,
              private fb: UntypedFormBuilder) {
    this.errorCodes = new ErrorCodes();
    this.openaireSearchUtils.status = this.errorCodes.LOADING;
  }
  
  ngOnInit() {
    this.subscriptions.push(this.route.params.subscribe(params => {
      this.openaireSearchUtils.status = this.errorCodes.LOADING;
      this._getOpenaireContentProviders("", 1, this.resultsPerPage);
      this.body = "[Please write your message here]";
      this.body = StringUtils.URIEncode(this.body);
    }));
    this.openaireSearchUtils.keyword = "";
    this.filterForm = this.fb.group({
      keyword: [''],
    });
    this.subscriptions.push(this.filterForm.get('keyword').valueChanges
      .pipe(debounceTime(1000), distinctUntilChanged())
      .subscribe(value => {
        this.keywordChanged(value);
      })
    );
  }
  
  public ngOnDestroy() {
    this.subscriptions.forEach(sub => {
      if (sub instanceof Subscriber) {
        sub.unsubscribe();
      }
    });
    
    if (this.subResults) {
      this.subResults.unsubscribe();
    }
  }
  
  get loading() {
    return this.openaireSearchUtils.status == this.errorCodes.LOADING
  }

  get infoMessage(): string {
    return "<div>" +
      "If you cannot find a "+OpenaireEntities.DATASOURCE.toLowerCase()+" relevant to your "+OpenaireEntities.COMMUNITY.toLowerCase()+", probably it is not OpenAIRE compliant." +
      "<div>" +
      "Feel free to contact us " +
      "(<a href=\"mailto: "+ properties.feedbackmailForMissingEntities +
      "?Subject=[OpenAIRE Connect - "+ this.community.shortTitle + "] report missing "+OpenaireEntities.DATASOURCE + "&body=" + this.body + "\" " +
      "target=\"_top\">"+properties.feedbackmailForMissingEntities+"</a>) " +
      "to let us know and we'll try to get the "+OpenaireEntities.DATASOURCE.toLowerCase()+" on board!" +
      "</div>" +
      "</div>"
  }
  
  public addContentProvider(contentProvider: SearchResult, content = true, deposit =false) {
    this.subscriptions.push(this.manageCommunityContentProvidersService.addContentProvider(this.properties, this.community.communityId, contentProvider, content, deposit).subscribe(
      data => {
        this.communityContentProviders.push(data);
        this._clearCacheService.purgeBrowserCache(this.openAIREEntities.DATASOURCE+" added", this.community.communityId);
        NotificationHandler.rise(this.openAIREEntities.DATASOURCE+' successfully added!')
        this.communityContentProvidersChanged.emit({
          value: this.communityContentProviders,
        });
      },
      error => {
        this.handleError('An error has been occurred. Try again later!', error);
      }
    ));
  }
  
  public removeContentProvider(contentProvider) {
    let communityContentProvider = this.getCommunityContentProvider(contentProvider);
    let contentProviderId: string = communityContentProvider['openaireId'];
    this.subscriptions.push(this.manageCommunityContentProvidersService.removeContentProvider(this.properties, this.community.communityId, contentProviderId).subscribe(
      data => {
        let index = this.communityContentProviders.indexOf(communityContentProvider);
        this.communityContentProviders.splice(index, 1);
        this._clearCacheService.purgeBrowserCache(this.openAIREEntities.DATASOURCE+" removed", this.community.communityId);
        NotificationHandler.rise(this.openAIREEntities.DATASOURCE+' successfully removed!')
        this.communityContentProvidersChanged.emit({
          value: this.communityContentProviders,
        });
      },
      error => {
        this.handleError('An error has been occurred. Try again later!', error);
      }
    ));
  }
  
  public getCommunityContentProvider(contentProvider: any): any {
    let index: number = 0;
    for (let communityContentProvider of this.communityContentProviders) {
      if (communityContentProvider.openaireId == contentProvider.id) {
        return communityContentProvider;
      }
      index++;
    }
    return null;
  }
  
  public getResultPreview(result: SearchResult): ResultPreview {
    return ResultPreview.searchResultConvert(result, "dataprovider");
  }
  
  private _getOpenaireContentProviders(parameters: string, page: number, size: number) {
    if (this.openaireSearchUtils.status == this.errorCodes.LOADING) {
      this.openaireSearchUtils.status = this.errorCodes.LOADING;
      
      this.openaireContentProviders = [];
      this.openaireSearchUtils.totalResults = 0;
      
      if (this.subResults) {
        this.subResults.unsubscribe();
      }
      this.subResults = this.searchDataprovidersService.searchDataproviders(parameters, null, page, size, [], this.properties).subscribe(
        data => {
          this.openaireSearchUtils.totalResults = data[0];
          this.openaireContentProviders = data[1];
          this.openaireSearchUtils.status = this.errorCodes.DONE;
          if (this.openaireSearchUtils.totalResults == 0) {
            this.openaireSearchUtils.status = this.errorCodes.NONE;
          }
        },
        error => {
          if (error.status == '404') {
            this.handleError('No OpenAIRE data sources found.', error);
            this.openaireSearchUtils.status = this.errorCodes.NOT_FOUND;
          } else if (error.status == '500') {
            this.handleError('An Error Occurred. No OpenAIRE data sources found.', error);
            this.openaireSearchUtils.status = this.errorCodes.ERROR;
          } else {
            this.handleError('Service temporarily unavailable. Please try again later.', error);
            this.openaireSearchUtils.status = this.errorCodes.NOT_AVAILABLE;
          }
        }
      );
    }
  }
  
  keywordChanged(keyword) {
    this.openaireSearchUtils.keyword = keyword;
    this.buildQueryParameters();
    this.goTo(1);
  }
  
  buildQueryParameters() {
    this.queryParameters = "";
    if (this.openaireSearchUtils.keyword) {
      this.queryParameters = "q=" + StringUtils.URIEncode(this.openaireSearchUtils.keyword);
    }
  }
  
  goTo(page: number = 1) {
    this.openaireSearchUtils.page = page;
    this.openaireSearchUtils.status = this.errorCodes.LOADING;
    this._getOpenaireContentProviders(this.queryParameters, page, this.resultsPerPage);
  }
  
  handleError(message: string, error = null) {
    console.error(error);
    NotificationHandler.rise(message, 'danger');
  }
}
