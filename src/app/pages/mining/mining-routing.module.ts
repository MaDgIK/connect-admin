import {NgModule} from "@angular/core";
import {InteractiveMiningModule, InteractiveMiningRoutingModule} from "interactiveminingv3";

@NgModule({
  imports: [InteractiveMiningRoutingModule, InteractiveMiningModule]
})
export class MiningRoutingModule {

}
