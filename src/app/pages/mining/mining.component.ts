import {Component, OnDestroy, OnInit} from "@angular/core";
import {ActivatedRoute} from "@angular/router";
import {Title} from "@angular/platform-browser";
import {Subscription} from "rxjs";

@Component({
  selector: 'mining',
  template: `
    <div page-content>
      <div inner>
        <div class="mining uk-section">
          <router-outlet></router-outlet>
        </div>
      </div>
    </div>
  `
})
export class MiningComponent implements OnInit, OnDestroy{
  private subs: any[] = [];
  
  constructor(private title: Title,
              private route: ActivatedRoute) {
  }
  
  ngOnInit() {
    this.subs.push(this.route.params.subscribe(params => {
      this.title.setTitle(params['community'].toUpperCase() + ' | Mining');
    }));
  }
  
  ngOnDestroy() {
    this.subs.forEach(subscription => {
      if(subscription instanceof Subscription) {
        subscription.unsubscribe();
      }
    });
  }
}
