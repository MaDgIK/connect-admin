import {NgModule} from '@angular/core';
import {RouterModule} from "@angular/router";
import {CommonModule} from "@angular/common";
import {MiningComponent} from "./mining.component";
import {PageContentModule} from "../../openaireLibrary/dashboard/sharedComponents/page-content/page-content.module";

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '', component: MiningComponent, children: [
          {path: '', loadChildren: () => import('./mining-routing.module').then(m => m.MiningRoutingModule)}
        ]
      }
    ]),
    PageContentModule,
  ],
  declarations: [MiningComponent],
  exports: [MiningComponent]
})
export class MiningModule {
}
