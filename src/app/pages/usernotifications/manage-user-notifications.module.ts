import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

import {ManageUserNotificationsComponent} from './manage-user-notifications.component';
import {ManageUserNotificationsService} from './manage-user-notifications.service';
import {ManageUserNotificationsRoutingModule} from './manage-user-notifications-routing.module';
import {UsersTabsModule} from '../users/users-tabs.module';
import {PageContentModule} from '../../openaireLibrary/dashboard/sharedComponents/page-content/page-content.module';
import { MatSelectModule } from '@angular/material/select';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import {LoadingModule} from '../../openaireLibrary/utils/loading/loading.module';
import {SharedModule} from '../../openaireLibrary/shared/shared.module';
import {MailPrefsService} from "../../openaireLibrary/connect/userEmailPreferences/mailPrefs.service";
import {InputModule} from "../../openaireLibrary/sharedComponents/input/input.module";

@NgModule({
  imports: [
    ManageUserNotificationsRoutingModule, RouterModule, CommonModule, FormsModule, UsersTabsModule, PageContentModule, MatSlideToggleModule, LoadingModule, SharedModule, MatSelectModule, InputModule
  ],
  declarations: [
    ManageUserNotificationsComponent
  ],
  providers: [
    ManageUserNotificationsService, MailPrefsService
  ],
  exports: [
    ManageUserNotificationsComponent
  ]
})

export class ManageUserNotificationsModule {
}
