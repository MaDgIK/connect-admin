export class UserNotificationsRights {
  notifyForNewManagers: boolean;
  notifyForNewSubscribers: boolean;
  managerEmail: string;
  constructor (notifyForNewManagers: boolean = true, notifyForNewSubscribers: boolean = true, managerEmail: string = null ){
    this.notifyForNewManagers = notifyForNewManagers;
    this.notifyForNewSubscribers = notifyForNewSubscribers;
    this.managerEmail = managerEmail;
  }
}
