import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {ManageUserNotificationsComponent} from './manage-user-notifications.component';

@NgModule({
  imports: [
    RouterModule.forChild([
      { path: '', component: ManageUserNotificationsComponent}
    ])
  ]
})
export class ManageUserNotificationsRoutingModule {
}
