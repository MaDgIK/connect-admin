import {Component, ElementRef, Input, OnInit} from '@angular/core';
import {UntypedFormBuilder, UntypedFormGroup} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {EnvProperties} from '../../openaireLibrary/utils/properties/env-properties';
import {Session, User} from '../../openaireLibrary/login/utils/helper.class';
import {ManageUserNotificationsService} from './manage-user-notifications.service';
import {UserNotificationsRights} from './userNotificationsRights';
import {LoginErrorCodes} from '../../openaireLibrary/login/utils/guardHelper.class';
import {UserManagementService} from '../../openaireLibrary/services/user-management.service';
import {Title} from '@angular/platform-browser';
import {CommunityService} from '../../openaireLibrary/connect/community/community.service';
import {properties} from '../../../environments/environment';
import {Subscriber} from 'rxjs';
import {MailPrefsService} from '../../openaireLibrary/connect/userEmailPreferences/mailPrefs.service';
import {CommunityInfo} from "../../openaireLibrary/connect/community/communityInfo";
import {Option} from "../../openaireLibrary/sharedComponents/input/input.component";
import {NotificationHandler} from "../../openaireLibrary/utils/notification-handler";

@Component({
  selector: 'manage-user-notifications',
  templateUrl: './manage-user-notifications.component.html',
})

export class ManageUserNotificationsComponent implements OnInit {
  @Input('group')
  myForm: UntypedFormGroup;
  public properties: EnvProperties = properties;
  public community: CommunityInfo;
  public userNotifications: UserNotificationsRights = null;
  public initialUserNotifications: UserNotificationsRights = null;
  public notifications = null;
  public initialNotifications = [];
  public userEmail = null;
  public showLoading = true;
  public hasChanged = false;
  public user: User;
  private subscriptions = [];
  
  frequencyOptions: Option[] = [{label: "Daily", value: 24}, {label: "Every two days", value: 48}, {label: "Weekly", value: 168}]
  
  constructor(private route: ActivatedRoute, private _router: Router, public _fb: UntypedFormBuilder,
              private title: Title,
              private manageUserNotificationsService: ManageUserNotificationsService,
              private element: ElementRef, private userManagementService: UserManagementService,
              private communityService: CommunityService,
              private mailPrefsService: MailPrefsService) {
  }
  
  ngOnInit() {
    this.subscriptions.push(this.communityService.getCommunityAsObservable().subscribe(community => {
      if (community) {
        this.community = community;
        this.title.setTitle(this.community.shortTitle.toUpperCase() + ' | User Notifications');
        this.showLoading = true;
        this.subscriptions.push(this.userManagementService.getUserInfo().subscribe(user => {
          this.user = user;
          if (this.user) {
            this.userEmail = this.user.email;
            this.subscriptions.push(this.manageUserNotificationsService.getUserNotifications(this.properties, this.community.communityId).subscribe(
              userNotifications => {
                this.initialUserNotifications = userNotifications;
                if (this.initialUserNotifications['notifyForNewManagers'] == null ||
                  this.initialUserNotifications['notifyForNewSubscribers'] == null) {
                  if (Session.isManager("community", this.community.communityId, this.user)) {
                    this.initialUserNotifications = new UserNotificationsRights(true, true, "");
                  } else {
                    this.initialUserNotifications = new UserNotificationsRights(false, false, "");
                  }
                }
                
                this.userNotifications = JSON.parse(JSON.stringify(this.initialUserNotifications));
                this.getClaimsNotifications();
              },
              error => {
                if (error.status === 404) {
                  if (Session.isManager("community", this.community.communityId, this.user)) {
                    this.initialUserNotifications = new UserNotificationsRights(true, true, "");
                  } else {
                    this.initialUserNotifications = new UserNotificationsRights(false, false, "");
                  }
                  this.userNotifications = JSON.parse(JSON.stringify(this.initialUserNotifications));
                } else {
                  this.handleError('System error retrieving user notifications', error);
                }
                this.getClaimsNotifications();
              }
            ));
          }
        }));
      }
    }));
  }
  
  getClaimsNotifications() {
    this.subscriptions.push(this.mailPrefsService.getUserEmailPreferencesForCommunity(this.community.communityId, this.properties.claimsAPIURL).subscribe(
      data => {
        if (data.code != 204) {
          this.initialNotifications = data.data;
        } else {
          if (Session.isManager("community", this.community.communityId, this.user)) {
            this.initialNotifications = [{notify: true, frequency: 24, openaireId: this.community.communityId}];
          } else {
            this.initialNotifications = [{notify: false, frequency: 24, openaireId: this.community.communityId}];
          }
        }
        this.notifications = JSON.parse(JSON.stringify(this.initialNotifications));
        this.showLoading = false;
      },
      err => {
        if (err.status === 404) {
          if (Session.isManager("community", this.community.communityId, this.user)) {
            this.initialNotifications = [{notify: true, frequency: 24, openaireId: this.community.communityId}];
          } else {
            this.initialNotifications = [{notify: false, frequency: 24, openaireId: this.community.communityId}];
          }
          this.notifications = JSON.parse(JSON.stringify(this.initialNotifications));
        } else {
          this.handleError("Error getting user email preferences for community with id: " + this.community.communityId, err);
        }
        this.showLoading = false;
      }
    ));
  }
  
  ngOnDestroy() {
    this.subscriptions.forEach(value => {
      if (value instanceof Subscriber) {
        value.unsubscribe();
      }
    });
  }
  
  
  public updateUserNotifications() {
    if (this.community.communityId != null && this.community.communityId !== '') {
      this.showLoading = true;
      const userNotifications = this.parseUpdatedUserNotifications();
      this.subscriptions.push(this.manageUserNotificationsService.updateUserNotifications(this.properties, this.community.communityId, userNotifications).subscribe(
        userNotifications => {
          this.initialUserNotifications = JSON.parse(JSON.stringify(this.userNotifications));
          this.handleSuccessfulSave('Notification settings saved!');
        },
        error => this.handleUpdateError('System error updating user notifications', error)
      ));
      this.subscriptions.push(this.mailPrefsService.saveUserEmailPreferences(this.notifications[0], this.properties.claimsAPIURL).subscribe(
        data => {
          this.initialNotifications[0] = JSON.parse(JSON.stringify(this.notifications[0]));
          this.handleSuccessfulSave('Claims notification settings saved!');
        },
        err => {
          //console.log(err);
          this.handleError("Error saving user email preferences: " + JSON.stringify(this.notifications[0]), err);
        }
      ));
    }
    this.resetChange();
  }
  
  private parseUpdatedUserNotifications(): {} {
    const userNotifications = {};
    userNotifications['notifyForNewManagers'] = this.userNotifications.notifyForNewManagers;
    userNotifications['notifyForNewSubscribers'] = this.userNotifications.notifyForNewSubscribers;
    if (this.userNotifications.managerEmail) {
      userNotifications['managerEmail'] = this.userNotifications.managerEmail;
    }
    return userNotifications;
  }
  
  public resetForm() {
    this.notifications[0] = JSON.parse(JSON.stringify(this.initialNotifications[0]));
    if (this.userNotifications && this.initialUserNotifications) {
      this.showLoading = true;
      this.userNotifications = JSON.parse(JSON.stringify(this.initialUserNotifications));
      this.showLoading = false;
    }
    this.resetChange();
  }
  
  public changeValueForNewManagers(notifyForManagers: any) {
    this.userNotifications.notifyForNewManagers = !notifyForManagers;
    this.change();
  }
  
  public changeValueForNewSubscribers(notifyForSubscribers: any) {
    this.userNotifications.notifyForNewSubscribers = !notifyForSubscribers;
    this.change();
  }
  
  public change() {
    this.hasChanged = true;
  }
  
  private resetChange() {
    this.hasChanged = false;
  }
  
  handleUpdateError(message: string, error) {
    console.log('Server responded: ' + error);
    this.showLoading = false;
  }
  
  handleError(message: string, error) {
    console.log('Server responded: ' + error);
    NotificationHandler.rise(message, 'danger');
    this.showLoading = false;
  }
  
  handleSuccessfulSave(message) {
    this.showLoading = false;
    NotificationHandler.rise(message);
  }
  
  changeNotify(notification: any, checked: boolean, index: number) {
    notification.notify = checked;
    this.change();
  }
  
  changeFrequency() {
    this.change();
  }
  
  
}
