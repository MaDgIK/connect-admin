import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {StatsComponent} from './stats.component';
import {StatsRoutingModule} from './stats-routing.module';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';

@NgModule({
    imports: [
        CommonModule, StatsRoutingModule, RouterModule, MatSlideToggleModule
    ],
    declarations: [StatsComponent],
    exports: [StatsComponent]
})
export class StatsModule { }
