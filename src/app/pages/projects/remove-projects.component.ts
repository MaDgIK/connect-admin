import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {Subscriber} from 'rxjs';

import {ErrorCodes} from '../../openaireLibrary/utils/properties/errorCodes';
import {SearchUtilsClass} from '../../openaireLibrary/searchPages/searchUtils/searchUtils.class';
import {EnvProperties} from '../../openaireLibrary/utils/properties/env-properties';
import {ManageCommunityProjectsService} from '../../services/manageProjects.service';
import {SearchCommunityProjectsService} from '../../openaireLibrary/connect/projects/searchProjects.service';
import {RouterHelper} from '../../openaireLibrary/utils/routerHelper.class';
import {UntypedFormArray, UntypedFormBuilder, UntypedFormGroup} from "@angular/forms";
import {properties} from "../../../environments/environment";
import {CommunityService} from "../../openaireLibrary/connect/community/community.service";
import {Option} from "../../openaireLibrary/sharedComponents/input/input.component";
import {SearchInputComponent} from "../../openaireLibrary/sharedComponents/search-input/search-input.component";
import {DropdownFilterComponent} from "../../openaireLibrary/utils/dropdown-filter/dropdown-filter.component";
import {OpenaireEntities} from "../../openaireLibrary/utils/properties/searchFields";
import {CommunityInfo} from "../../openaireLibrary/connect/community/communityInfo";
import {HelperFunctions} from "../../openaireLibrary/utils/HelperFunctions.class";
import {NotificationHandler} from "../../openaireLibrary/utils/notification-handler";
import {ClearCacheService} from "../../openaireLibrary/services/clear-cache.service";
import {debounceTime, distinctUntilChanged} from "rxjs/operators";
import {IndexInfoService} from "../../openaireLibrary/utils/indexInfo.service";

@Component({
  selector: 'remove-projects',
  templateUrl: './remove-projects.component.html'
})
export class RemoveProjectsComponent implements OnInit {
  @Input() public community: CommunityInfo;
  @Input() public loading: boolean = true;
  @Output() addProjects: EventEmitter<void> = new EventEmitter();

  private subscriptions: any[] = [];

  public routerHelper: RouterHelper = new RouterHelper();
  public projectUrl = "https://" + ((properties.environment == "beta" || properties.environment == "development") ? "beta." : "") + "explore.openaire.eu" + properties.searchLinkToProject.split("?")[0];

  @Output() communityProjectsChanged = new EventEmitter();
  // @Input() public communityProjects = [];
  public previewCommunityProjects = [];
  public communitySearchUtils: SearchUtilsClass = new SearchUtilsClass();
  public errorCodes: ErrorCodes;
  properties: EnvProperties = properties;

  private selectedCommunityProject: any;
  @ViewChild('AlertModalDeleteCommunity') alertModalDeleteCommunity;

  /* Paging */
  page: number = 1;
  resultsPerPage: number = properties.resultsPerPage;

  /* Search */
  @ViewChild('searchInputComponent') searchInputComponent: SearchInputComponent;
  filterForm: UntypedFormGroup;
  private searchText: RegExp = new RegExp('');
  public keyword: string = '';
  allFunderOptions: Option[] = [];
  previewFunderOptions: Option[] = [];
  sortOptions: Option[] = [
    {label: "Title ", value: {sort: "name", descending: false}},
    {label: "Grant ID ", value: {sort: "grantId", descending: false}},
    {label: "Funder ", value: {sort: "funder", descending: false}}
  ];

  public openaireEntiites = OpenaireEntities;
  lastDBLoadDate = null;
  constructor(private route: ActivatedRoute, private _router: Router,
              private _fb: UntypedFormBuilder,
              private communityService: CommunityService,
              private _manageCommunityProjectsService: ManageCommunityProjectsService,
              private _searchCommunityProjectsService: SearchCommunityProjectsService,
              private _clearCacheService: ClearCacheService, private indexInfoService: IndexInfoService) {
    this.errorCodes = new ErrorCodes();
    this.communitySearchUtils.status = this.errorCodes.LOADING;
  }

  ngOnInit() {
    this.subscriptions.push(this.indexInfoService.getDBLoadLastDate(this.properties).subscribe(res => {
      this.lastDBLoadDate = res;
    }));
    this.communitySearchUtils.keyword = "";

    this.filterForm = this._fb.group({
      keyword: [''],
      funder:  this._fb.control(null),
      sort: this._fb.control(this.sortOptions[0].value)
    });

    this.subscriptions.push(this.filterForm.get('keyword').valueChanges.pipe(debounceTime(500), distinctUntilChanged()).subscribe(value => {
      this.page = 1;
      this._getCommunityProjects(this.page, this.filterForm.get('keyword').value,(this.filterForm.get("funder").value? this.filterForm.get("funder").value.id:null),
        this.filterForm.get("sort").value.sort  );

    }));
    this.subscriptions.push(this.filterForm.get('sort').valueChanges.subscribe(value => {
      this.page = 1;
      this._getCommunityProjects(this.page, this.filterForm.get('keyword').value, this.filterForm.get("funder").value? this.filterForm.get("funder").value.id:null, this.filterForm.get("sort").value.sort );
    }));

    this.subscriptions.push(this.communityService.getCommunityAsObservable().subscribe(community => {
      if (community) {
        this.community = community;
        this.projectUrl = "https://"
          + ((this.properties.environment == "beta" || this.properties.environment == "development") ? "beta." : "")
          + this.community.communityId + ".openaire.eu" + this.properties.searchLinkToProject.split("?")[0];

        this.keyword = '';
        this._getCommunityFunders();
        this._getCommunityProjects(1,this.keyword, null);
      }
    }));
  }

  public ngOnDestroy() {
    this.subscriptions.forEach(sub => {
      if (sub instanceof Subscriber) {
        sub.unsubscribe();
      }
    });
  }

  totalPages(): number {
    let totalPages: any = this.communitySearchUtils.totalResults / (this.resultsPerPage);
    if (!(Number.isInteger(totalPages))) {
      totalPages = (parseInt(totalPages, 10) + 1);
    }
    return totalPages;
  }

  public confirmedDeleteProject(data: any) {
    this.subscriptions.push(this._manageCommunityProjectsService.removeProject(this.properties, this.community.communityId, this.selectedCommunityProject.openaireId).subscribe(
      data => {
        this._clearCacheService.clearCacheInRoute(this.openaireEntiites.PROJECT+" removed", this.community.communityId);
        this._clearCacheService.purgeBrowserCache(this.openaireEntiites.PROJECT+" removed", this.community.communityId);
        NotificationHandler.rise(OpenaireEntities.PROJECT+' successfully removed!');
        this._getCommunityProjects(this.communitySearchUtils.page, this.keyword,null);

      },
      err => {
        this.handleError('An error has been occurred. Try again later!');
        console.error(err);
      }
    ));
  }

  public removeProject(communityProject: any) {
    this.selectedCommunityProject = communityProject;
    this.alertModalDeleteCommunity.cancelButton = true;
    this.alertModalDeleteCommunity.okButton = true;
    this.alertModalDeleteCommunity.alertTitle = "Remove "+OpenaireEntities.PROJECT;
    let title = "";
    if (communityProject.name) {
      title = communityProject.name;
    }
    if (communityProject.name && communityProject.acronym) {
      title += " (";
    }
    if (communityProject.acronym) {
      title += communityProject.acronym;
    }
    if (communityProject.name && communityProject.acronym) {
      title += ")";
    }
    this.alertModalDeleteCommunity.message = OpenaireEntities.PROJECT;
    if (title) {
      this.alertModalDeleteCommunity.message += " <b>" + title + "</b> ";
    }
    this.alertModalDeleteCommunity.message += "will be removed from your "+OpenaireEntities.COMMUNITY.toLowerCase()+". Are you sure?";
    this.alertModalDeleteCommunity.okButtonText = "Yes";
    this.alertModalDeleteCommunity.open();
  }
  public getCommunityProjects() {
  this._getCommunityProjects(this.communitySearchUtils.page, this.keyword,null );
  }
  public _getCommunityProjects(page, keyword, funder, orderBy = "name" ) {
    this.communitySearchUtils.status = this.errorCodes.LOADING;
    this.communitySearchUtils.page = page;
    this.communitySearchUtils.keyword = keyword;
    this.subscriptions.push(this._searchCommunityProjectsService.searchProjectsWithPaging(this.properties, this.community.communityId, this.communitySearchUtils.page, this.resultsPerPage, this.communitySearchUtils.keyword, funder, orderBy).subscribe(
      data => {
        this.previewCommunityProjects = data.content;
        this.communitySearchUtils.totalResults = data.totalElements;
        this.communitySearchUtils.status = this.errorCodes.DONE;
        this.communityProjectsChanged.emit({
          value:  this.communitySearchUtils.totalResults
        });
        this.loading = false;
      },
      err => {
        console.error(err);
        //TODO check erros (service not available, bad request)

        if (err.status == '404') {
          this.communitySearchUtils.status = this.errorCodes.NOT_FOUND;
        } else if (err.status == '500') {
          this.communitySearchUtils.status = this.errorCodes.ERROR;
        } else {
          this.communitySearchUtils.status = this.errorCodes.NOT_AVAILABLE;
        }
        this.loading = false;
      }
    ));
  }
  public _getCommunityFunders() {
    this.subscriptions.push(this._searchCommunityProjectsService.getProjectFunders(this.properties, this.community.communityId).subscribe(
      data => {
         for (let funder of data) {
          this.allFunderOptions.push({label: funder, value: {id: funder, label: funder}});
        }
        this.previewFunderOptions =[...this.allFunderOptions];
      },
      err => {
      }
    ));
  }

  public updatePage($event) {
    HelperFunctions.scroll();
    this.page = $event.value;
    this._getCommunityProjects(this.page, this.filterForm.get('keyword').value, null);
  }

  addNew() {
    this.addProjects.emit();
  }


  public onSearchClose() {
    this.communitySearchUtils.keyword = this.filterForm.get('keyword').value;
  }

  public resetInput() {
    this.communitySearchUtils.keyword = null;
    this.searchInputComponent.reset()
  }

  handleError(message: string) {
    NotificationHandler.rise(message, 'danger');
  }

  select(option, event, dropdownFilter: DropdownFilterComponent) {
    console.log(option)
    if(event.target instanceof HTMLInputElement) {
      dropdownFilter.closeDropdown();
      if(event.target.checked) {
        this.filterForm.get("funder").setValue(option.value);
        this._getCommunityProjects(1, this.filterForm.get('keyword').value, this.filterForm.get('funder').value.id );
          this.previewFunderOptions =[option];
      } else if(!event.target.checked) {
        this.filterForm.get("funder").setValue(null);
        this._getCommunityProjects(1, this.filterForm.get('keyword').value,null);
        this.previewFunderOptions =[...this.allFunderOptions];
      }
    }
  }

  isSelected(value: string) {
    return this.filterForm && this.filterForm.get('funder').value && this.filterForm.get('funder').value.id === value;
  }


}
