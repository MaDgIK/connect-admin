import {NgModule} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {ManageProjectsComponent} from './manage-projects.component';
import {ManageCommunityProjectsService} from '../../services/manageProjects.service';
import {SearchProjectsServiceModule} from '../../openaireLibrary/connect/projects/searchProjectsService.module';

import {RemoveProjectsComponent} from './remove-projects.component';
import {AddProjectsComponent} from './add-projects.component';
import {SearchProjectsService} from '../../openaireLibrary/services/searchProjects.service';
import {PagingModule} from '../../openaireLibrary/utils/paging.module';
import {SearchPagingModule} from '../../openaireLibrary/searchPages/searchUtils/searchPaging.module';
import {AlertModalModule} from '../../openaireLibrary/utils/modal/alertModal.module';
import {CommonModule} from '@angular/common';
import {PageContentModule} from "../../openaireLibrary/dashboard/sharedComponents/page-content/page-content.module";
import {InputModule} from "../../openaireLibrary/sharedComponents/input/input.module";
import {SearchInputModule} from "../../openaireLibrary/sharedComponents/search-input/search-input.module";
import {NoLoadPaging} from "../../openaireLibrary/searchPages/searchUtils/no-load-paging.module";
import {LoadingModule} from "../../openaireLibrary/utils/loading/loading.module";
import {IconsModule} from "../../openaireLibrary/utils/icons/icons.module";
import {FullScreenModalModule} from "../../openaireLibrary/utils/modal/full-screen-modal/full-screen-modal.module";
import {ResultPreviewModule} from "../../openaireLibrary/utils/result-preview/result-preview.module";
import {ErrorMessagesModule} from "../../openaireLibrary/utils/errorMessages.module";
import {DropdownFilterModule} from "../../openaireLibrary/utils/dropdown-filter/dropdown-filter.module";
import {SearchFilterModule} from "../../openaireLibrary/searchPages/searchUtils/searchFilter.module";
import {ValidateEnabledPageModule} from "../../utils/validateEnabledPage.module";

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    PagingModule, SearchPagingModule,
    ErrorMessagesModule,
    AlertModalModule,
    SearchProjectsServiceModule,
    PageContentModule,
    InputModule,
    SearchInputModule,
    RouterModule.forChild([
      {
        path: '', component: ManageProjectsComponent
      }
    ]),
    NoLoadPaging, LoadingModule, IconsModule, FullScreenModalModule, ResultPreviewModule, DropdownFilterModule, SearchFilterModule, ValidateEnabledPageModule
  ],
  declarations: [
    ManageProjectsComponent,
    RemoveProjectsComponent,
    AddProjectsComponent
  ],
  providers: [
    ManageCommunityProjectsService,
    SearchProjectsService
  ],
  exports: [ManageProjectsComponent]
})
export class CommunityProjectsModule {
  constructor() {}
}
