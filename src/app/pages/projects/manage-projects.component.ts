import {Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";

import {RemoveProjectsComponent} from './remove-projects.component';

import {Title} from '@angular/platform-browser';
import {FullScreenModalComponent} from "../../openaireLibrary/utils/modal/full-screen-modal/full-screen-modal.component";
import {StringUtils} from "../../openaireLibrary/utils/string-utils.class";
import {EnvProperties} from "../../openaireLibrary/utils/properties/env-properties";
import {properties} from "../../../environments/environment";
import {Subscriber} from "rxjs";
import {CommunityInfo} from "../../openaireLibrary/connect/community/communityInfo";
import {OpenaireEntities} from "../../openaireLibrary/utils/properties/searchFields";
import {CommunityService} from "../../openaireLibrary/connect/community/community.service";

@Component({
  selector: 'manage-projects',
  template: `
    <remove-projects #removeProjectsComponent (addProjects)="openAddProjects()"  
                              [loading]="showLoadingInRemove" [community]="community">
    </remove-projects>
    <fs-modal #fsModalProjects>
      <add-projects *ngIf="modalIsOpen" #addProjects   [community]="community"
                             (communityProjectsChanged)="communityProjectsChanged($event)"></add-projects>
    </fs-modal>
  `
})

export class ManageProjectsComponent implements OnInit {
  @Input() communityProjects = [];
  @ViewChild(RemoveProjectsComponent) removeProjectsComponent: RemoveProjectsComponent;
  @ViewChild('fsModalProjects', { static: true }) fullscreen: FullScreenModalComponent;
  private subscriptions: any[] = [];
  public showLoadingInRemove: boolean = true;
  public body: string = "Send from page";
  public properties: EnvProperties = properties;
  public community: CommunityInfo = null;
  modalIsOpen = false;

  constructor(private element: ElementRef,
              private title: Title,
              private route: ActivatedRoute, private _router: Router,
              private communityService: CommunityService) {
  }

  ngOnInit() {
    this.subscriptions.push(this.communityService.getCommunityAsObservable().subscribe(community => {
      this.community = community;
      if (this.community) {
        this.title.setTitle(this.community.shortTitle.toUpperCase() + ' | ' + OpenaireEntities.PROJECTS);
        this.body = "[Please write your message here]";
        this.body = StringUtils.URIEncode(this.body);
      }
    }));
  }

  public ngOnDestroy() {
    this.subscriptions.forEach(sub => {
      if (sub instanceof Subscriber) {
        sub.unsubscribe();
      }
    });
  }

  public openAddProjects() {
    this.fullscreen.title = "Search and Add " + OpenaireEntities.PROJECTS;
    this.fullscreen.okButtonText = "Done";
    this.fullscreen.back = true;
    this.fullscreen.okButton = true;
    this.fullscreen.open();
    this.modalIsOpen = true;
  }

  public communityProjectsChanged($event) {
   /* this.communityProjects = $event.value;
    this.showLoadingInRemove = false;

    if (this.fullscreen.isOpen) {
      this.removeProjectsComponent.applyFilters();
      this.removeProjectsComponent.createFunderFilter();
    }*/
    this.removeProjectsComponent.getCommunityProjects();
  }
}
