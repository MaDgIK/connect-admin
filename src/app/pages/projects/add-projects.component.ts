import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {SearchResult} from '../../openaireLibrary/utils/entities/searchResult';
import {ErrorCodes} from '../../openaireLibrary/utils/properties/errorCodes';
import {SearchUtilsClass} from '../../openaireLibrary/searchPages/searchUtils/searchUtils.class';
import {EnvProperties} from '../../openaireLibrary/utils/properties/env-properties';
import {SearchProjectsService} from '../../openaireLibrary/services/searchProjects.service';
import {RouterHelper} from '../../openaireLibrary/utils/routerHelper.class';
import {StringUtils} from '../../openaireLibrary/utils/string-utils.class';
import {ManageCommunityProjectsService} from '../../services/manageProjects.service';

import {properties} from "../../../environments/environment";
import {Subscriber} from "rxjs";
import {UntypedFormBuilder, UntypedFormGroup} from "@angular/forms";
import {debounceTime, distinctUntilChanged} from "rxjs/operators";
import {ResultPreview} from "../../openaireLibrary/utils/result-preview/result-preview";
import {SearchInputComponent} from "../../openaireLibrary/sharedComponents/search-input/search-input.component";
import {CommunityInfo} from "../../openaireLibrary/connect/community/communityInfo";
import {OpenaireEntities} from "../../openaireLibrary/utils/properties/searchFields";
import {Filter, Value} from "../../openaireLibrary/searchPages/searchUtils/searchHelperClasses.class";
import {ClearCacheService} from "../../openaireLibrary/services/clear-cache.service";
import {SearchCommunityProjectsService} from "../../openaireLibrary/connect/projects/searchProjects.service";

declare var UIkit;

@Component({
  selector: 'add-projects',
  templateUrl: './add-projects.component.html',
})

export class AddProjectsComponent implements OnInit {
  private subscriptions: any[] = [];
  public subResults: any;

  @Input() community: CommunityInfo = null;

  public routerHelper: RouterHelper = new RouterHelper();
  public properties: EnvProperties = properties;
  public errorCodes: ErrorCodes;
  public openaireSearchUtils: SearchUtilsClass = new SearchUtilsClass();
  @Output() communityProjectsChanged = new EventEmitter();
  // @Input() communityProjects = [];
  public openaireProjects = [];
  public queryParameters: string = "";

  // public pagingLimit: number = properties.pagingLimit;
  public resultsPerPage: number = properties.resultsPerPage;
  public selectedFunderId: string = "";
  public selectedFunder: Value = null;

  filterForm: UntypedFormGroup;
  funders: Filter = null;
  @ViewChild('searchInputComponent') searchInputComponent: SearchInputComponent;

  private projectUrl: string = "https://" + ((properties.environment == "beta" || properties.environment == "development") ? "beta." : "") + "explore.openaire.eu" + properties.searchLinkToProject;
  public body: string = "Send from page";

  openaireEntities = OpenaireEntities;

  constructor(private route: ActivatedRoute, private _router: Router, private _searchProjectsService: SearchProjectsService,
              private _manageCommunityProjectsService: ManageCommunityProjectsService,
              private _clearCacheService: ClearCacheService, private _searchCommunityProjectsService: SearchCommunityProjectsService,
              private _fb: UntypedFormBuilder) {
    this.errorCodes = new ErrorCodes();
    this.openaireSearchUtils.status = this.errorCodes.LOADING;
    this.openaireSearchUtils.refineStatus = this.errorCodes.LOADING;
  }

  ngOnInit() {
    this.openaireSearchUtils.status = this.errorCodes.LOADING;
    this.openaireSearchUtils.refineStatus = this.errorCodes.LOADING;
    this.getFunders();
    this._getOpenaireProjects("", 1, this.resultsPerPage);

    this.body = "[Please write your message here]";
    this.body = StringUtils.URIEncode(this.body);

    this.openaireSearchUtils.keyword = "";

    this.filterForm = this._fb.group({
      keyword: [''],
      funder: []
    });

    this.subscriptions.push(this.filterForm.get('keyword').valueChanges
      .pipe(debounceTime(1000), distinctUntilChanged())
      .subscribe(value => {
        this.keywordChanged(value);
      }));
  }

  public ngOnDestroy() {
    this.subscriptions.forEach(sub => {
      if (sub instanceof Subscriber) {
        sub.unsubscribe();
      }
    });

    if (this.subResults) {
      this.subResults.unsubscribe();
    }
  }

  get loading() {
    return this.openaireSearchUtils.status == this.errorCodes.LOADING
  }

  get disabled() {
    return this.openaireSearchUtils.status == this.errorCodes.LOADING || this.openaireSearchUtils.refineStatus == this.errorCodes.LOADING;
  }

  get infoMessage(): string {
    return "<div>" +
    "If you cannot find a funder that is relevant for your "+OpenaireEntities.COMMUNITY.toLowerCase()+", please contact us " +
    "(<a href=\"mailto:" + properties.feedbackmailForMissingEntities +
    "?Subject=[OpenAIRE Connect - "+ this.community.shortTitle + "] report missing Funder" + "&body=" + this.body + "\" " +
    "target=\"_top\">"+properties.feedbackmailForMissingEntities+"</a>) " +
    "and we will try to get the funder on board!" +
    "</div>";
  }

  public addProject(project: SearchResult) {
    this.subscriptions.push(this._manageCommunityProjectsService.addProject(this.properties, this.community.communityId, project).subscribe(
      data => {
        // this.communityProjects.push(data);
        this._clearCacheService.clearCacheInRoute(this.openaireEntities.PROJECT+" added", this.community.communityId);
        this._clearCacheService.purgeBrowserCache(this.openaireEntities.PROJECT+" added", this.community.communityId);

        UIkit.notification(this.openaireEntities.PROJECT+' successfully added!', {
          status: 'success',
          timeout: 6000,
          pos: 'bottom-right'
        });
        project["isPart"] = true;
        this.communityProjectsChanged.emit({
          value: project,
        });
      },
      err => {
        this.handleError('An error has been occurred. Try again later!');
        console.error(err.status);
      }
    ));
  }

  public removeProject(project: any) {
    // let communityProject = this.getCommunityProject(project);
    this.subscriptions.push(this._manageCommunityProjectsService.removeProject(this.properties, this.community.communityId,  project['id']).subscribe(
      data => {
        // let index = this.communityProjects.indexOf(communityProject);
        // this.communityProjects.splice(index, 1);
        this._clearCacheService.purgeBrowserCache(this.openaireEntities.PROJECT+" removed", this.community.communityId);
        UIkit.notification(this.openaireEntities.PROJECT+' successfully removed!', {
          status: 'success',
          timeout: 6000,
          pos: 'bottom-right'
        });
        project["isPart"] = false;
        this.communityProjectsChanged.emit({
          value: project,
        });
      },
      err => {
        this.handleError('An error has been occurred. Try again later!');
        console.error(err);
      }
    ));
  }

  public getCommunityProject(project: any) {
    this.subscriptions.push(this._searchCommunityProjectsService.searchProjectsWithPaging(this.properties, this.community.communityId, 1, this.resultsPerPage, project.id, null).subscribe(
      data => {
        if(data.totalElements > 0){
          project["isPart"] = true;
        }
      },
      err => {
        console.error(err);
        //TODO check erros (service not available, bad request)

      }
    ));
  }

  getFunders() {
    this.openaireSearchUtils.refineStatus = this.errorCodes.LOADING;
    this.subscriptions.push(this._searchProjectsService.getFunders(this.queryParameters, this.properties).subscribe(
      data => {
        this.funders = data[1][0];
        this.funders.values.map((value) => {
          if(value.id == this.selectedFunderId) {
            value.selected = true;
            this.funders.countSelectedValues++;
          }
        })
        if(this.funders.values && this.funders.values.length == 0) {
          this.openaireSearchUtils.refineStatus = this.errorCodes.NONE;
        } else {
          this.openaireSearchUtils.refineStatus = this.errorCodes.DONE;
        }
      },
      err => {
        this.openaireSearchUtils.refineStatus = this.errorCodes.ERROR;
        console.error("Server error fetching funders: ", err)
      }
    ));
  }

  public getResultPreview(result: SearchResult): ResultPreview {
    return ResultPreview.searchResultConvert(result, "project");
  }

  private _getOpenaireProjects(parameters: string, page: number, size: number) {
    if (this.openaireSearchUtils.status == this.errorCodes.LOADING) {
      this.openaireSearchUtils.status = this.errorCodes.LOADING;

      this.openaireProjects = [];
      this.openaireSearchUtils.totalResults = 0;

      if (this.subResults) {
        this.subResults.unsubscribe();
      }
      this.subResults = this._searchProjectsService.searchProjects(parameters, null, page, size, [], this.properties).subscribe(
        data => {
          this.openaireSearchUtils.totalResults = data[0];
          this.openaireProjects = data[1];
          for(let project of this.openaireProjects){
            this.getCommunityProject(project);
          }
          this.openaireSearchUtils.status = this.errorCodes.DONE;
          if (this.openaireSearchUtils.totalResults == 0) {
            this.openaireSearchUtils.status = this.errorCodes.NONE;
          }
        },
        err => {
          console.error(err);
          //TODO check erros (service not available, bad request)
          if (err.status == '404') {
            this.openaireSearchUtils.status = this.errorCodes.NOT_FOUND;
          } else if (err.status == '500') {
            this.openaireSearchUtils.status = this.errorCodes.ERROR;
          } else {
            this.openaireSearchUtils.status = this.errorCodes.NOT_AVAILABLE;
          }
        }
      );
    }
  }

  totalPages(): number {
    let totalPages: any = this.openaireSearchUtils.totalResults / (this.resultsPerPage);
    if (!(Number.isInteger(totalPages))) {
      totalPages = (parseInt(totalPages, 10) + 1);
    }
    return totalPages;
  }

  keywordChanged(keyword) {
    this.openaireSearchUtils.keyword = keyword;
    this.buildQueryParameters();
    this.goTo(1);
  }

  funderChanged(filter: Filter) {
    this.getSelectedValues(filter);
    this.buildQueryParameters();
    this.goTo(1);
  }

  buildQueryParameters() {
    this.queryParameters = "";

    if (this.openaireSearchUtils.keyword) {
      this.queryParameters = "q=" + StringUtils.URIEncode(this.openaireSearchUtils.keyword);
    }

    if (this.selectedFunderId) {
      this.queryParameters += this.queryParameters ? "&" : "";
      this.queryParameters += "fq=funder exact " + '"' + StringUtils.URIEncode(this.selectedFunderId) + '"';
    }
  }

  goTo(page: number = 1, refineQuery: boolean = true) {
    this.openaireSearchUtils.page = page;
    this.openaireSearchUtils.status = this.errorCodes.LOADING;
    if(refineQuery) {
      this.openaireSearchUtils.refineStatus = this.errorCodes.LOADING;
      this.getFunders();
    }
    this._getOpenaireProjects(this.queryParameters, page, this.resultsPerPage);
  }

  public onSearchClose() {
    this.openaireSearchUtils.keyword = this.filterForm.get('keyword').value;
  }

  public resetInput() {
    this.openaireSearchUtils.keyword = null;
    this.searchInputComponent.reset()
  }

  handleError(message: string) {
    UIkit.notification(message, {
      status: 'danger',
      timeout: 6000,
      pos: 'bottom-right'
    });
  }

  getSelectedValues(filter): any {
    var selected = [];
    if(filter.values.length == 0 && this.selectedFunderId) {
      return [this.selectedFunder];
    } else {
      this.selectedFunderId = "";
      this.selectedFunder = null;
      if (filter.countSelectedValues > 0) {
        for (var i = 0; i < filter.values.length; i++) {
          if (filter.values[i].selected) {
            selected.push(filter.values[i]);
            this.selectedFunderId = filter.values[i].id;
            this.selectedFunder = filter.values[i];
          }
        }
      }
      return selected;
    }
  }

  clearFilters() {
    for (var j = 0; j < this.funders.countSelectedValues; j++) {
      if (this.funders.values[j].selected) {
        this.removeFilter(this.funders.values[j], this.funders);
        break;
      }
    }
  }

  public removeFilter(value: Value, filter: Filter) {
    this.selectedFunderId = "";
    this.selectedFunder = null;

    if(value) {
      filter.countSelectedValues--;
      if (value.selected == true) {
        value.selected = false;
      }
      if (filter.filterType == "radio") {
        filter.radioValue = "";
      }
    }
    this.buildQueryParameters();
    this.goTo(1);
  }
}
