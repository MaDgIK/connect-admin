import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {ConnectRIGuard} from "../openaireLibrary/connect/communityGuard/connectRIGuard.guard";

@NgModule({
  imports: [RouterModule.forChild([
    {
      path: 'info',
      loadChildren: () => import('./community-info/community-info-routing.module').then(m => m.CommunityInfoRoutingModule),
    },
    {
      path: 'users',
      loadChildren: () => import('./users/users-routing.module').then(m => m.UsersRoutingModule)
    },
    {
      path: 'mining',
      loadChildren: () => import('./mining/mining.module').then(m => m.MiningModule),
      canActivateChild: [ConnectRIGuard]
    },
    {
      path: 'admin-tools',
      loadChildren: () => import('./admin-tools/admin-tools-routing.module').then(m => m.AdminToolsRoutingModule),
      data: {
        param: 'community'
      }
    },
    {
      path: 'stats',
      loadChildren: () => import('./stats/stats.module').then(m => m.StatsModule)
    },
    {
      path: 'customize-layout',
      loadChildren: () => import('./customization/customization.module').then(m => m.CustomizationModule),
      data: {hasInternalSidebar: true, hasHeader: false}
    },
    {
      path: 'user-info',
      loadChildren: () => import('../login/libUser.module').then(m => m.LibUserModule),
    },
  ])]
})
export class CommunityRoutingModule {}
