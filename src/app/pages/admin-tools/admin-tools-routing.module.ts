import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'entities',
        loadChildren: () => import('../../openaireLibrary/dashboard/entity/entities.module').then(m => m.EntitiesModule),
        pathMatch: 'full'
      },
      {
        path: 'classContents',
        loadChildren: () => import('../../openaireLibrary/dashboard/divhelpcontent/class-help-contents.module').then(m => m.ClassHelpContentsModule),
        pathMatch: 'full'
      },
      {
        path: 'classContents/edit',
        loadChildren: () => import('../../openaireLibrary/dashboard/divhelpcontent/class-help-content-form.module').then(m => m.ClassHelpContentFormModule),
        pathMatch: 'full'
      },
      {
        path: 'helptexts',
        loadChildren: () => import('../../openaireLibrary/dashboard/helpTexts/page-help-contents.module').then(m => m.PageHelpContentsModule),
        pathMatch: 'full'
      },
      {
        path: 'helptexts/edit',
        loadChildren: () => import('../../openaireLibrary/dashboard/helpTexts/page-help-content-form.module').then(m => m.PageHelpContentFormModule),
        pathMatch: 'full'
      },
      {
        path: 'plugins',
        loadChildren: () => import('../../openaireLibrary/dashboard/plugins/plugins.module').then(m => m.PluginsModule),
        pathMatch: 'full', data: {hasInternalSidebar: false, hasHeader: false}
      },
      {
        path: 'plugins/edit',
        loadChildren: () => import('../../openaireLibrary/dashboard/plugins/plugins-form/pluginsForm.module').then(m => m.PluginsFormModule),
        pathMatch: 'full', data: {hasInternalSidebar: true, hasHeader: false}
      },
      {
        path: 'pages',
        loadChildren: () => import('../../openaireLibrary/dashboard/page/pages.module').then(m => m.PagesModule),
        pathMatch: 'full'
      },
			{
        path: 'menu',
        loadChildren: () => import('../../openaireLibrary/dashboard/menu/menu.module').then(m => m.MenuModule),
        pathMatch: 'full'
      },
      {
        path: 'customization',
        loadChildren: () => import('../connect-admin-customization/connect-admin-customization.module').then(m => m.ConnectAdminCustomizationModule),
        pathMatch: 'full'
      }
    ])
  ]
})
export class AdminToolsRoutingModule {
}
