import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {AddZenodoCommunitiesComponent} from './add-zenodo-communities.component';
import {ZenodoCommunitiesComponent} from './zenodo-communities.component';
import {ManageZenodoCommunitiesService} from '../../services/manageZenodoCommunities.service';
import {ZenodoCommunitiesServiceModule} from '../../openaireLibrary/connect/zenodoCommunities/zenodo-communitiesService.module';
import {AlertModalModule} from '../../openaireLibrary/utils/modal/alertModal.module';
import {ZenodoCommunitiesRoutingModule} from './zenodo-communities-routing.module';
import {SearchInputModule} from '../../openaireLibrary/sharedComponents/search-input/search-input.module';
import {IconsModule} from '../../openaireLibrary/utils/icons/icons.module';
import {NoLoadPaging} from '../../openaireLibrary/searchPages/searchUtils/no-load-paging.module';
import {LoadingModule} from '../../openaireLibrary/utils/loading/loading.module';
import {PreviewZenodoCommunityComponent} from './preview-z-community.component';
import {PageContentModule} from '../../openaireLibrary/dashboard/sharedComponents/page-content/page-content.module';
import {FullScreenModalModule} from '../../openaireLibrary/utils/modal/full-screen-modal/full-screen-modal.module';
import {HTMLToStringPipeModule} from '../../openaireLibrary/utils/pipes/HTMLToStringPipe.module';
import {PagingModule} from "../../openaireLibrary/utils/paging.module";
import {ValidateEnabledPageModule} from "../../utils/validateEnabledPage.module";

@NgModule({
  imports: [
    CommonModule, FormsModule, RouterModule, ZenodoCommunitiesServiceModule,
    AlertModalModule, ZenodoCommunitiesRoutingModule, PageContentModule, SearchInputModule, IconsModule, NoLoadPaging, LoadingModule, FullScreenModalModule, HTMLToStringPipeModule, PagingModule, ValidateEnabledPageModule
  ],
  declarations: [
    ZenodoCommunitiesComponent, AddZenodoCommunitiesComponent, PreviewZenodoCommunityComponent
  ],
  providers: [
     ManageZenodoCommunitiesService
   ],
  exports: [
      ZenodoCommunitiesComponent
   ]
})

export class ZenodoCommunitiesModule {
  constructor() {}
}
