import {
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  ViewChild
} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

import {EnvProperties} from '../../openaireLibrary/utils/properties/env-properties';
import {ZenodoCommunitiesService} from '../../openaireLibrary/connect/zenodoCommunities/zenodo-communities.service';
import {CommunityService} from '../../openaireLibrary/connect/community/community.service';
import {CommunityInfo} from '../../openaireLibrary/connect/community/communityInfo';
import {ZenodoCommunityInfo} from '../../openaireLibrary/connect/zenodoCommunities/zenodoCommunityInfo';
import {SearchUtilsClass} from '../../openaireLibrary/searchPages/searchUtils/searchUtils.class';
import {ErrorCodes} from '../../openaireLibrary/utils/properties/errorCodes';
import {Title} from '@angular/platform-browser';
import {properties} from '../../../environments/environment';
import {Subscription} from 'rxjs';
import {FullScreenModalComponent} from '../../openaireLibrary/utils/modal/full-screen-modal/full-screen-modal.component';
import {NotificationHandler} from "../../openaireLibrary/utils/notification-handler";
import {HelperFunctions} from "../../openaireLibrary/utils/HelperFunctions.class";
import {SearchInputComponent} from "../../openaireLibrary/sharedComponents/search-input/search-input.component";
import {UntypedFormBuilder, UntypedFormControl} from "@angular/forms";
import {ManageZenodoCommunitiesService} from "../../services/manageZenodoCommunities.service";

@Component({
  selector: 'zenodo-communities',
  templateUrl: 'zenodo-communities.component.html'
})
export class ZenodoCommunitiesComponent implements OnInit, OnDestroy {
  public community: CommunityInfo = null;
  public toggle = false;
  public pageTitle = 'Manage zenodo communities';
  masterZenodoCommunityId = null;
  masterCommunity = null;
  public properties: EnvProperties = null;
  selectedCommunityIds = null;
  selectedCommunities = null;
  zenodocommunitiesloadedCount = 0;
  public errorCodes: ErrorCodes = new ErrorCodes();
  subscriptions = [];
  @ViewChild('fsModal', {static: true}) fullscreen: FullScreenModalComponent;
  public showLoadingInRemove: boolean = true;
  @Input() public loading: boolean = true;
  @Input() searchUtils: SearchUtilsClass = new SearchUtilsClass();
  public rowsOnPage = 10;
  previewCommunities = [];
  @ViewChild('AlertModalDeleteCommunity') alertModalDeleteCommunity;
  selectedToDelete = null;
  @Output() addZenodoCommunity: EventEmitter<any> = new EventEmitter();
  page = 1;
  size = 10;
  @ViewChild('searchInputComponent') searchInputComponent: SearchInputComponent;
  public filterForm: UntypedFormControl;
  deleteMaster = false;
  constructor(private element: ElementRef, private route: ActivatedRoute, private _router: Router,
              private _zenodoCommunitieService: ZenodoCommunitiesService, private title: Title,
              private _communityService: CommunityService,
              public _fb: UntypedFormBuilder,
              private cdr: ChangeDetectorRef,
              private _manageZenodoCommunitiesService: ManageZenodoCommunitiesService) {
  }

  ngOnInit() {
    this.searchUtils.status = this.errorCodes.LOADING;
    this.properties = properties;
    this.subscriptions.push(this.route.params.subscribe(params => {
      //use this method to ensure that get the lattest version of the community and the updated information for the list of zenodo communities
      this.subscriptions.push(this._communityService.getCommunity(params['community'],true).subscribe(
        community => {
            this.community = community;
            this.title.setTitle(this.community.shortTitle.toUpperCase() + ' | Zenodo Communities');
            this.masterZenodoCommunityId = this.community.zenodoCommunity;
            this.selectedCommunities = [];
            this.searchUtils.status = this.errorCodes.LOADING;
            this.selectedCommunityIds = this.community.otherZenodoCommunities;
            this.searchUtils.totalResults = this.selectedCommunityIds.length;
            if (this.selectedCommunityIds.length === 0) {
              this.searchUtils.status = this.errorCodes.NONE;
            }
            for (let i = 0; i < this.selectedCommunityIds.length; i++) {
              this.getZenodoCommunityById(this.selectedCommunityIds[i]);
            }
            this.init();
        })
      );
    }));
    this.fullscreen.title = "Search and Add Zenodo Communities";
    this.fullscreen.okButtonText = "Done";
    this.fullscreen.okButton = true;
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => {
      if (subscription instanceof Subscription) {
        subscription.unsubscribe();
      }
    });
  }

  getZenodoCommunityById(zenodoid) {
    this.subscriptions.push(this._zenodoCommunitieService.getZenodoCommunityById(this.properties,
      zenodoid).subscribe(
      result => {
        if(zenodoid == this.masterZenodoCommunityId){
          this.masterCommunity = result;
          result.master = true;
          this.selectedCommunities.unshift(result);
        }else {
          this.selectedCommunities.push(result);
        }
        this.zenodocommunitiesloadedCount++;
        if (this.zenodocommunitiesloadedCount >= this.selectedCommunityIds.length) {
          this.searchUtils.status = this.errorCodes.DONE;
        }

      },
      error => {
        const emptyCommunity: ZenodoCommunityInfo = new ZenodoCommunityInfo();
        emptyCommunity.id = zenodoid;
        emptyCommunity.title = zenodoid;
        this.selectedCommunities.push(emptyCommunity);
        this.zenodocommunitiesloadedCount++;
        if (this.zenodocommunitiesloadedCount >= this.selectedCommunityIds.length) {
          this.searchUtils.status = this.errorCodes.DONE;
        }
        console.log('Zenodo community\'' + zenodoid + '\' couldn\'t be loaded');
      }
    ));
  }


  public openAddZenodoCommunites() {
    this.fullscreen.title = "Search and Add Zenodo Communities";
    this.fullscreen.okButtonText = "Done";
    this.fullscreen.back = true;
    this.fullscreen.okButton = true;
    this.fullscreen.open();
  }

  public zenodoCommunitiesChanged($event) {
    this.selectedCommunities = $event.communities;
    this.masterCommunity = $event.main;
    if(!this.masterZenodoCommunityId && $event.main){
      this.selectedCommunityIds.unshift($event.main.id)
      this.selectedCommunities.unshift($event.main);
    }
    this.masterZenodoCommunityId = $event.main?$event.main.id:null;
    this.showLoadingInRemove = false;
    if (this.fullscreen.isOpen) {
      this.filterPreviewCommunities(this.filterForm.value);
    }
    this.filterPreviewCommunities(this.searchUtils.keyword);
  }
  private init() {
    this.filterForm = this._fb.control('');
    this.filterPreviewCommunities("");
    this.subscriptions.push(this.filterForm.valueChanges.subscribe(value => {
      this.page = 1;
      this.filterPreviewCommunities(value);
    }));
    this.searchUtils.keyword = "";
    this.searchUtils.totalResults = this.selectedCommunities.length;
  }

  public filterPreviewCommunities(value: string) {
    this.previewCommunities = this.selectedCommunities.filter(community => {
      return !value || community.title.toLowerCase().indexOf(value.toLowerCase()) != -1
    });
    if (this.previewCommunities.slice((this.page - 1) * this.rowsOnPage, this.page * this.rowsOnPage).length == 0) {
      this.page = 1;
    }
    this.cdr.detectChanges();
  }

  public confirmedDeleteCommunity(data: any) {
    if(this.deleteMaster){
      this.subscriptions.push(this._manageZenodoCommunitiesService.removeZenodoCommunity(this.properties, this.community.communityId, this.selectedToDelete.id, true).subscribe(
        data => {
          this.afterRemoveActions();
        }
      ));
    }else {
      this.subscriptions.push(this._manageZenodoCommunitiesService.removeZenodoCommunity(this.properties, this.community.communityId, this.selectedToDelete.id).subscribe(
        data => {
          this.afterRemoveActions();
        }
      ));
    }
  }
  afterRemoveActions(){
    if(this.deleteMaster){
      this.masterCommunity = null;
      this.masterZenodoCommunityId = null
      NotificationHandler.rise('Community has been <b>removed from master</b>!')
    }else {
      var pos = -1;
      for (var i = 0; i < this.selectedCommunities.length; i++) {
        if (this.selectedCommunities[i].id == this.selectedToDelete.id) {
          pos = i;
          break;
        }
      }
      if (pos != -1) {
        this.selectedCommunities.splice(pos, 1);
        this.selectedCommunityIds.splice(pos,1)
          this.searchUtils.totalResults = this.selectedCommunities.length;
      }
      this.searchUtils.totalResults = this.selectedCommunities.length;
      this.filterPreviewCommunities(this.filterForm.value);
      NotificationHandler.rise('Community has been <b>successfully removed</b>!')
    }
  }
  public removeCommunity(comm) {
    this.deleteMaster = false;
    this.selectedToDelete = comm;
    this.alertModalDeleteCommunity.cancelButton = true;
    this.alertModalDeleteCommunity.okButton = true;
    this.alertModalDeleteCommunity.alertTitle = "Remove zenodo community";
    let title = "";
    if (comm.title) {
      title = comm.title;
    }

    this.alertModalDeleteCommunity.message = "Zenodo community";
    if (title) {
      this.alertModalDeleteCommunity.message += " '" + title + "' ";
    }
    this.alertModalDeleteCommunity.message += "will be removed from your community. Are you sure?";
    this.alertModalDeleteCommunity.okButtonText = "Yes";
    this.alertModalDeleteCommunity.open();
  }

  public removeFromMasterCommunity(comm) {
    this.deleteMaster = true;
    this.selectedToDelete = comm;
    this.alertModalDeleteCommunity.cancelButton = true;
    this.alertModalDeleteCommunity.okButton = true;
    this.alertModalDeleteCommunity.alertTitle = "Remove master zenodo community";
    let title = "";
    if (comm.title) {
      title = comm.title;
    }

    this.alertModalDeleteCommunity.message = "Zenodo community";
    if (title) {
      this.alertModalDeleteCommunity.message += " '" + title + "' ";
    }
    this.alertModalDeleteCommunity.message += "will be removed as master from your community, but will remain in the list of the related communities. Do you want to proceed?";
    this.alertModalDeleteCommunity.okButtonText = "Yes";
    this.alertModalDeleteCommunity.open();
  }


  totalPages(): number {
    let totalPages: any = this.searchUtils.totalResults / (this.rowsOnPage);
    if (!(Number.isInteger(totalPages))) {
      totalPages = (parseInt(totalPages, 10) + 1);
    }
    return totalPages;
  }

  public updatePage($event) {
    HelperFunctions.scroll();
    this.page = $event.value;
  }

  addNew() {
    this.addZenodoCommunity.emit();
  }

  handleError(message: string) {
    NotificationHandler.rise(message, 'danger');
  }
  public addAsMasterCommunity(community) {
    this.subscriptions.push(this._manageZenodoCommunitiesService.addZenodoCommunity(this.properties ,this.community.communityId,community.id, true).subscribe(
      data => {
        this.masterCommunity = community;
          this.masterZenodoCommunityId = community.id
      },
      err => {
        this.handleError('An error has been occurred. Try again later!');
        console.log(err.status);
      }
    ));
  }

}
