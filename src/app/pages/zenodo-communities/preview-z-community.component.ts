import {Component, Input} from '@angular/core';

@Component({
    selector: 'preview-zenodo-community',
    template: `
      <div class="header uk-grid uk-grid-medium uk-flex-middle" uk-grid>
        <div class="uk-width-auto" *ngIf="item.logoUrl">
          <a target="_blank" [href]="item.link">
            <img *ngIf="item.logoUrl"
                 src="{{item.logoUrl}}" width="80" height="80"
                 alt="">
          </a>
        </div>
        <div class="uk-width-expand">
          <div *ngIf="master"><div class="uk-badge uk-margin-small-bottom">Main Zenodo Community</div></div>
          <h2 class="uk-margin-remove uk-text-break uk-inline-block uk-h6">
            <a class="custom-external uk-link uk-link-text uk-width-expand" target="_blank"
               href="{{item.link}}">
              <span *ngIf="item.title">{{item.title}}</span>
              <span *ngIf="!item.title">[no name available]</span>
            </a>
          </h2>
          <div *ngIf="item.date" class="uk-text-small uk-margin-small-top">
            <span class="uk-text-meta">Last update: </span>{{item.date | date:'yyyy/MM/dd'}}
          </div>
        </div>
      </div>
      <div class="uk-margin-top uk-text-small multi-line-ellipsis lines-3">
        <div *ngIf="item.description" class="uk-margin-remove"
             [innerHtml]="item.description"></div>
      </div>
    `
})

export class PreviewZenodoCommunityComponent  {
   @Input() item;
   @Input() master: boolean = false;
 }
