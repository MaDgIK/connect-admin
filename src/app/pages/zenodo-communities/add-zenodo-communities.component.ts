import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {UntypedFormBuilder, UntypedFormControl} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {EnvProperties} from '../../openaireLibrary/utils/properties/env-properties';
import {properties} from "../../../environments/environment";
import {ErrorCodes} from '../../openaireLibrary/utils/properties/errorCodes';
import {SearchUtilsClass} from '../../openaireLibrary/searchPages/searchUtils/searchUtils.class';
import {ZenodoCommunitiesService} from '../../openaireLibrary/connect/zenodoCommunities/zenodo-communities.service';
import {ManageZenodoCommunitiesService} from '../../services/manageZenodoCommunities.service';
import {Subject, Subscription} from 'rxjs';
import {debounceTime, distinctUntilChanged} from 'rxjs/operators';
import {CommunityInfo} from "../../openaireLibrary/connect/community/communityInfo";
import {HelperFunctions} from "../../openaireLibrary/utils/HelperFunctions.class";
import {CommunityService} from "../../openaireLibrary/connect/community/community.service";
import {NotificationHandler} from "../../openaireLibrary/utils/notification-handler";

declare var UIkit;

@Component({
  selector: 'add-zenodo-communities',
  templateUrl: './add-zenodo-communities.component.html',
})
export class AddZenodoCommunitiesComponent implements OnInit {
  
  public zenodoCommunities = null; // zenodo search API results
  public totalZenodoCommunities = null;
  properties: EnvProperties = properties;
  @Input() community: CommunityInfo = null;
  public filterForm: UntypedFormControl;
  private subscriptions: any[] = [];
  public subResults: any;
  public zenodoCommunitySearchUtils: SearchUtilsClass = new SearchUtilsClass();
  public searchTermStream = new Subject<string>();
  errorCodes: ErrorCodes;
  public rowsOnPage = 10;
  @Input() masterCommunity = null;
  @Input() selectedCommunities = [];
  @Output() zenodoCommunitiesChanged = new EventEmitter();

  constructor(private route: ActivatedRoute,
              private _router: Router,
              public _fb: UntypedFormBuilder,
              private _zenodoCommunitieService: ZenodoCommunitiesService,
              private _manageZenodoCommunitiesService: ManageZenodoCommunitiesService,
              private _communityService: CommunityService) {
    
    this.errorCodes = new ErrorCodes();
    this.zenodoCommunitySearchUtils.status = this.errorCodes.LOADING;
  }
  
  ngOnInit() {
    this.filterForm = this._fb.control('');
    this.subscriptions.push(this.filterForm.valueChanges.subscribe(value => {
      this.searchTermStream.next(value);
    }));
    this.subscriptions.push(this.searchTermStream
      .pipe(debounceTime(1000), distinctUntilChanged())
      .subscribe((term: string) => {
        this.zenodoCommunitySearchUtils.keyword = term;
        this.goTo(1);
      }));
    
    this.zenodoCommunitySearchUtils.keyword = "";
    this.zenodoCommunitySearchUtils.status = this.errorCodes.LOADING;
    if (this.community.communityId != null && this.community.communityId != '') {
      this._zenodoCommunitieService.getZenodoCommunities(this.properties, this.properties.zenodoCommunities + "?page=" + this.zenodoCommunitySearchUtils.page + "&size=" + this.rowsOnPage).subscribe(
        result => {
          this.zenodoCommunities = result[0];
          this.totalZenodoCommunities = result[1];
          this.zenodoCommunitySearchUtils.totalResults = result[1];
          this.zenodoCommunitySearchUtils.page = 1;
          this.zenodoCommunitySearchUtils.size = this.rowsOnPage;
          if (this.totalZenodoCommunities == 0) {
            this.zenodoCommunitySearchUtils.status = this.errorCodes.NONE;
          } else {
            this.zenodoCommunitySearchUtils.status = this.errorCodes.DONE;
          }

        },
        error => {
          this.zenodoCommunitySearchUtils.status = this.errorCodes.ERROR;
        }
      );
    }
  }
  
  ngOnDestroy() {
    this.subscriptions.forEach(subscription => {
      if (subscription instanceof Subscription) {
        subscription.unsubscribe();
      }
    });
    if(this.subResults){
      this.subResults.unsubscribe();
    }
  }
  
  public goTo(page: number = 1) {
    this.zenodoCommunitySearchUtils.page = page;
    this.zenodoCommunitySearchUtils.status = this.errorCodes.LOADING;
    if(this.subResults){
      this.subResults.unsubscribe();
    }
    this.subResults = this._zenodoCommunitieService.getZenodoCommunities(this.properties,
      this.properties.zenodoCommunities + "?page=" + this.zenodoCommunitySearchUtils.page
      + "&size=" + this.rowsOnPage
      + ((this.zenodoCommunitySearchUtils.keyword) ? ("&q=" + this.zenodoCommunitySearchUtils.keyword) : "")
    ).subscribe(
      result => {
        this.zenodoCommunities = result[0];
        this.totalZenodoCommunities = result[1];
        this.zenodoCommunitySearchUtils.totalResults = result[1];
        this.zenodoCommunitySearchUtils.size = this.rowsOnPage;
        if (this.totalZenodoCommunities == 0) {
          this.zenodoCommunitySearchUtils.status = this.errorCodes.NONE;
        } else {
          this.zenodoCommunitySearchUtils.status = this.errorCodes.DONE;
        }

      },
      error => {
        this.zenodoCommunitySearchUtils.status = this.errorCodes.ERROR;
      }
    );
  }
  
  totalPages(): number {
    let totalPages: any = this.zenodoCommunitySearchUtils.totalResults / (this.rowsOnPage);
    if (!(Number.isInteger(totalPages))) {
      totalPages = (parseInt(totalPages, 10) + 1);
    }
    return totalPages;
  }
  
  
  public addCommunity(community, master:boolean = false) {
    if(master && this.getPosition(community) == -1 ){
      this.addCommunity(community, false);
    }
    this.subscriptions.push(this._manageZenodoCommunitiesService.addZenodoCommunity(this.properties, this.community.communityId, community.id, master).subscribe(
      data => {
        // this._communityService.setCommunity(this._communityService.parseCommunity(data));
        if(master){
          this.masterCommunity = community;
        }else {
          this.selectedCommunities.push(community);
        }
        UIkit.notification('Community successfully added' + (master?" as master!":"!"), {
          status: 'success',
          timeout: 6000,
          pos: 'bottom-right'
        });
        this.zenodoCommunitiesChanged.emit({
          communities: this.selectedCommunities,
          main:this.masterCommunity
        });
      },
      err => {
        this.handleError('An error has been occurred. Try again later!');
        console.log(err.status);
      }
    ));
  }

  public removeCommunity(comm, master:boolean = false) {
    var openaireId = this.getOpenaireId(comm);
    this.subscriptions.push(this._manageZenodoCommunitiesService.removeZenodoCommunity(this.properties, this.community.communityId, openaireId, master).subscribe(
      data => {
        this.afterRemoveActions(comm, master);
      },
      err => {
        this.handleError('An error has been occurred. Try again later!');
        console.log(err.status);
      }
    ));
  }
  getPosition(comm){
    var pos = -1;
    for (var i = 0; i < this.selectedCommunities.length; i++) {
      if (this.selectedCommunities[i].id == comm.id) {
        pos = i;
        break;
      }
    }
    return pos;
  }
  afterRemoveActions(comm, master:boolean = false){
    if(master){
      this.masterCommunity = null;
      NotificationHandler.rise('Community has been <b>removed from master</b>!')
    }else {
      var pos = this.getPosition(comm)
      if (pos != -1) {
        this.selectedCommunities.splice(pos, 1);
      }
    }
    NotificationHandler.rise('Community has been <b>removed</b>!')
    this.zenodoCommunitiesChanged.emit({
      communities: this.selectedCommunities,
      main: this.masterCommunity
    });
  }
  public inThelist(community: any, list): any {
    for (let com of list) {
      if (com.id == community.id) {
        return true;
      }
    }
    return false;
  }
  
  public getOpenaireId(community: any): string {
    for (let com of this.selectedCommunities) {
      if (com.id == community.id) {
        return com.id;
      }
    }
    return null;
  }

  get loading() {
    return this.zenodoCommunitySearchUtils.status == this.errorCodes.LOADING
  }
  
  handleError(message: string) {
    UIkit.notification(message, {
      status: 'danger',
      timeout: 6000,
      pos: 'bottom-right'
    });
  }
}
