import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {ZenodoCommunitiesComponent} from './zenodo-communities.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            { path: '', component: ZenodoCommunitiesComponent}
        ])
    ]
})
export class ZenodoCommunitiesRoutingModule { }
