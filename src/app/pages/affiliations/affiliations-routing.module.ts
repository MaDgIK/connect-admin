import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {AffiliationsComponent} from './affiliations.component';

@NgModule({
  imports: [
    RouterModule.forChild([
      { path: '', component: AffiliationsComponent}
    ])
  ]
})
export class AffiliationsRoutingModule { }
