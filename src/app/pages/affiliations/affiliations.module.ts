import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';

import {AffiliationsComponent} from './affiliations.component';

import {AffiliationsRoutingModule} from './affiliations-routing.module';
import {AffiliationService} from '../../openaireLibrary/connect/affiliations/affiliation.service';
import {AlertModalModule} from '../../openaireLibrary/utils/modal/alertModal.module';
import {UtilitiesService} from '../../openaireLibrary/services/utilities.service';
import {InputModule} from "../../openaireLibrary/sharedComponents/input/input.module";
import {PageContentModule} from "../../openaireLibrary/dashboard/sharedComponents/page-content/page-content.module";
import {IconsModule} from "../../openaireLibrary/utils/icons/icons.module";
import {NoLoadPaging} from "../../openaireLibrary/searchPages/searchUtils/no-load-paging.module";
import {LoadingModule} from "../../openaireLibrary/utils/loading/loading.module";
import {UrlPrefixModule} from "../../openaireLibrary/utils/pipes/url-prefix.module";
import {HelpContentService} from "../../services/help-content.service";
import {PagingModule} from "../../openaireLibrary/utils/paging.module";
import {ValidateEnabledPageModule} from "../../utils/validateEnabledPage.module";

@NgModule({
  imports: [
    AffiliationsRoutingModule, CommonModule, FormsModule, RouterModule,
    AlertModalModule, ReactiveFormsModule, InputModule, PageContentModule, IconsModule, NoLoadPaging, LoadingModule, UrlPrefixModule, PagingModule, ValidateEnabledPageModule
  ],
  declarations: [
    AffiliationsComponent
  ],
  providers: [
    AffiliationService, UtilitiesService, HelpContentService
  ],
  exports: [
    AffiliationsComponent
  ]
})

export class AffiliationsModule {
}
