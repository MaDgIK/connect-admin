import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

import {EnvProperties} from '../../openaireLibrary/utils/properties/env-properties';
import {Affiliation} from '../../openaireLibrary/utils/entities/CuratorInfo';
import {AlertModal} from '../../openaireLibrary/utils/modal/alert';
import {AffiliationService} from "../../openaireLibrary/connect/affiliations/affiliation.service";
import {HelpContentService} from "../../services/help-content.service";
import {Title} from '@angular/platform-browser';
import {StringUtils} from "../../openaireLibrary/utils/string-utils.class";
import {properties} from "../../../environments/environment";
import {Page} from "../../openaireLibrary/utils/entities/adminTool/page";
import {UntypedFormBuilder, UntypedFormGroup, Validators} from "@angular/forms";
import {Subscription} from "rxjs";
import {CommunityService} from "../../openaireLibrary/connect/community/community.service";
import {CommunityInfo} from "../../openaireLibrary/connect/community/communityInfo";
import {NotificationHandler} from "../../openaireLibrary/utils/notification-handler";
import {HelperFunctions} from "../../openaireLibrary/utils/HelperFunctions.class";
import {ClearCacheService} from "../../openaireLibrary/services/clear-cache.service";

@Component({
  selector: 'affiliations',
  templateUrl: './affiliations.component.html',
})
export class AffiliationsComponent implements OnInit, OnDestroy {
  public loading = true;
  public properties: EnvProperties = properties;
  public index = 0;
  public affiliations: Affiliation[];
  public affiliationFb: UntypedFormGroup;
  public community: CommunityInfo;
  public organizationsPage: Page;
  public page: number = 1;
  public pageSize: number = 10;
  private subs: any[] = [];
  @ViewChild('affiliationModal') affiliationModal: AlertModal;
  @ViewChild('removeAffiliationModal') removeAffiliationModal: AlertModal;
  
  constructor(private route: ActivatedRoute,
              private router: Router,
              private title: Title,
              private fb: UntypedFormBuilder,
              private communityService: CommunityService,
              private affiliationService: AffiliationService,
              private helpContentService: HelpContentService,
              private _clearCacheService: ClearCacheService) {
  }
  
  
  ngOnInit() {
    this.loading = true;
    this.subs.push(this.communityService.getCommunityAsObservable().subscribe( community => {
      this.community = community;
      if (this.community) {
        this.title.setTitle(this.community.shortTitle.toUpperCase() + ' | Organizations');
        this.getAffiliations();
      }
    }));
  }
  
  ngOnDestroy() {
    this.subs.forEach(sub => {
      if (sub instanceof Subscription) {
        sub.unsubscribe();
      }
    })
  }
  
  getAffiliations() {
    this.loading = true;
    this.affiliationService.getAffiliations(this.community.communityId).subscribe(
      affiliations => {
        this.affiliations = affiliations;
        this.loading = false;
      },
      error => {
        console.error("Affiliations Component: Error getting affiliations for community with id: " + this.community.communityId, error);
        this.loading = false;
      }
    );
  }

  editAffiliationOpen(index: number = -1) {
    let affiliation: Affiliation;
    this.index = index;
    if (index === -1) {
      affiliation = new Affiliation();
      affiliation.communityId = this.community.communityId;
      this.affiliationModal.alertTitle = 'Add Organization';
      this.affiliationModal.okButtonText = 'Add';
    } else {
      affiliation = this.affiliations[this.index];
      this.affiliationModal.alertTitle = 'Edit Organization';
      this.affiliationModal.okButtonText = 'Update';
    }
    this.affiliationFb = this.fb.group({
      communityId: this.fb.control(affiliation.communityId, Validators.required),
      id: this.fb.control(affiliation.id),
      name: this.fb.control(affiliation.name, Validators.required),
      logo_url: this.fb.control(affiliation.logo_url, [Validators.required, StringUtils.urlValidator()]),
      website_url: this.fb.control(affiliation.website_url, [Validators.required, StringUtils.urlValidator()])
    });
    this.affiliationModal.okButtonLeft = false;
    this.affiliationModal.cancelButtonText = 'Cancel';
    this.affiliationModal.open();
  }
  
  deleteAffiliationOpen(index: number) {
    this.index = index;
    let affiliation: Affiliation = this.affiliations[index];
    console.log(index, affiliation)
    this.removeAffiliationModal.alertTitle = 'Delete Organization';
    this.removeAffiliationModal.message = 'Do you want to remove <b>' +
      affiliation.name + '</b> from Organizations?';
    this.removeAffiliationModal.okButtonText = 'Yes';
    this.removeAffiliationModal.cancelButtonText = 'No';
    this.removeAffiliationModal.open();
  }
  
  get currentPage(): Affiliation[] {
    if (this.affiliations) {
      return this.affiliations.slice((this.page - 1) * this.pageSize, this.page * this.pageSize);
    } else {
      return [];
    }
  }
  
  getIndex(index: number): number {
    return (this.page - 1)*this.pageSize + index;
  }
  
  public updatePage(event) {
    HelperFunctions.scroll();
    this.page = event.value;
  }
  
  editAffiliation() {
    this.loading = true;
    this.affiliationService.updateAffiliation(this.properties.communityAPI + this.community.communityId + '/organizations',
      this.affiliationFb.value).subscribe((affiliation) => {
        if (affiliation) {
          if (this.index === -1) {
            this.affiliations.push(affiliation);
            this.page = Math.ceil(this.affiliations.length/this.pageSize);
          } else {
            this.affiliations[this.index] = affiliation;
          }
          this._clearCacheService.purgeBrowserCache("Organization added/ updated", this.community.communityId);
          if (affiliation.id) {
            this.handleUpdateSuccess('Your organization has been updated successfully!');
          } else {
            this.handleUpdateSuccess('Your organization has been saved successfully!');
          }
        }
      },
      error => {
        this.handleUpdateError('An error has been occurred. Try again later!');
      });
  }
  
  removeAffiliation() {
    this.loading = true;
    console.log(this.index, this.affiliations[this.index].id)
    this.affiliationService.deleteAffiliation(this.properties.communityAPI + this.community.communityId + '/organizations?organizationName='+this.affiliations[this.index].name,
      this.affiliations[this.index].id).subscribe((deleteOK) => {
        this.affiliations.splice(this.index, 1);
        if (this.currentPage.length === 0) {
          this.page = 1;
        }
        this._clearCacheService.purgeBrowserCache("Organization deleted", this.community.communityId);
        this.handleUpdateSuccess('Organization has been deleted');
      },
      error => {
        this.handleUpdateError('An error has been occurred. Try again later!');
      });
  }
  handleUpdateError(message: string) {
    NotificationHandler.rise(message, "danger");
    this.loading = false;
  }
  
  handleUpdateSuccess(message) {
    NotificationHandler.rise(message);
    this.loading = false;
  }

}
