import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {ConnectCommunityGuard} from "../../openaireLibrary/connect/communityGuard/connectCommunityGuard.guard";

@NgModule({
  imports: [
    RouterModule.forChild([
      {path: '', redirectTo: 'profile', pathMatch: 'full'},
      {path: 'profile', loadChildren: () => import('./profile/profile.module').then(m => m.ProfileModule)},
      {path: 'organizations', loadChildren: () => import('../affiliations/affiliations.module').then(m => m.AffiliationsModule)},
      {path: 'projects', loadChildren: () => import('../projects/communityProjects.module').then(m => m.CommunityProjectsModule)},
      {path: 'content-providers', loadChildren: () => import('../content-providers/communityContentProviders.module').then(m => m.CommunityContentProvidersModule)},
      {path: 'zenodo-communities', loadChildren: () => import('../zenodo-communities/zenodo-communities.module').then(m => m.ZenodoCommunitiesModule)},
      {path: 'advanced-criteria', loadChildren: () => import('../advanced-criteria/advanced-criteria.module').then(m => m.AdvancedCriteriaModule)},
      {path: 'subjects', canActivateChild: [ConnectCommunityGuard], loadChildren: () => import('../subjects/subjects-edit-form/subjects-edit-form.module').then(m => m.SubjectsEditFormModule)},
    ])
  ],
  providers: [ConnectCommunityGuard]
})
export class CommunityInfoRoutingModule {
}
