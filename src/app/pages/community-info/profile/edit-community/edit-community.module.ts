import {NgModule} from "@angular/core";
import {EditCommunityComponent} from "./edit-community.component";
import {CommonModule} from "@angular/common";
import {ReactiveFormsModule} from "@angular/forms";
import {CKEditorModule} from 'ng2-ckeditor';
import {InputModule} from "../../../../openaireLibrary/sharedComponents/input/input.module";
import {IconsModule} from "../../../../openaireLibrary/utils/icons/icons.module";

@NgModule({
  imports: [CommonModule, ReactiveFormsModule, InputModule, IconsModule
    , CKEditorModule
  ],
  declarations: [EditCommunityComponent],
  exports: [EditCommunityComponent]
})
export class EditCommunityModule {}
