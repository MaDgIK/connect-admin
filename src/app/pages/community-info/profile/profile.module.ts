import {NgModule} from '@angular/core';
import {RouterModule} from "@angular/router";
import {CommonModule} from "@angular/common";
import {PageContentModule} from "../../../openaireLibrary/dashboard/sharedComponents/page-content/page-content.module";
import {ProfileComponent} from "./profile.component";
import {EditCommunityModule} from "./edit-community/edit-community.module";
import {LoadingModule} from "../../../openaireLibrary/utils/loading/loading.module";

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '', component: ProfileComponent
      }
    ]),
    PageContentModule,
    EditCommunityModule,
    LoadingModule
  ],
  declarations: [ProfileComponent],
  exports: [ProfileComponent]
})
export class ProfileModule {
}
