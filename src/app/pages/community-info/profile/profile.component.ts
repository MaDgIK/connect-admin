import {Component, OnDestroy, OnInit, ViewChild} from "@angular/core";
import {CommunityInfo} from "../../../openaireLibrary/connect/community/communityInfo";
import {EnvProperties} from "../../../openaireLibrary/utils/properties/env-properties";
import {properties} from "../../../../environments/environment";
import {EditCommunityComponent} from "./edit-community/edit-community.component";
import {CommunityService} from "../../../openaireLibrary/connect/community/community.service";
import {Title} from "@angular/platform-browser";
import {Subscription} from "rxjs";

@Component({
  selector: 'community-profile',
  template: `
    <div page-content>
      <div actions class="uk-section-xsmall uk-margin-top">
        <div class="uk-container">
            <div class="uk-flex uk-flex-center uk-flex-right@m">
              <button class="uk-button uk-button-default uk-margin-right"
                      (click)="reset()" [class.uk-disabled]="loading || !editCommunityComponent.dirty"
                      [disabled]="loading || !editCommunityComponent.dirty">Reset
              </button>
              <button class="uk-button uk-button-primary"
                      [class.uk-disabled]="loading || editCommunityComponent.disabled"
                      (click)="save()" [disabled]="loading || editCommunityComponent.disabled">Save
              </button>
            </div>
          </div>
      </div>
      <div inner>
        <div class="uk-container">
          <div class="uk-position-relative" style="min-height: 60vh">
            <div [class.uk-hidden]="loading" class="uk-section uk-section-small">
              <edit-community #editCommunityComponent></edit-community>
            </div>
            <div *ngIf="loading" class="uk-position-center">
              <loading></loading>
            </div>
          </div>
        </div>
      </div>
    </div>
  `
})
export class ProfileComponent implements OnInit, OnDestroy {
  
  public community: CommunityInfo;
  public properties: EnvProperties = properties;
  public loading: boolean = false;
  private subscriptions: any[] = [];
  @ViewChild('editCommunityComponent', {static: true}) editCommunityComponent: EditCommunityComponent;
  
  constructor(private communityService: CommunityService,
              private title: Title) {
  }
  
  ngOnInit() {
    this.loading = true;
    this.subscriptions.push(this.communityService.getCommunityAsObservable().subscribe(community => {
      this.community = community;
      if (this.community) {
        this.title.setTitle(this.community.shortTitle.toUpperCase() + " | Profile");
        this.reset();
        this.loading = false;
      }
    }));
  }
  
  public reset() {
    this.editCommunityComponent.init(this.community);
  }
  
  public save() {
    this.loading = true;
    this.editCommunityComponent.save((community) => {
      this.community = community;
      this.reset();
      this.loading = false;
    }, (error) => {
      this.loading = false;
    });
  }
  
  ngOnDestroy() {
    this.subscriptions.forEach(subscription => {
      if (subscription instanceof Subscription) {
        subscription.unsubscribe();
      }
    });
  }
}
