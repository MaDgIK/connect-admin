import {Component, OnInit} from "@angular/core";
import {Title} from "@angular/platform-browser";
import {ActivatedRoute} from "@angular/router";
import {CommunityService} from "../../../openaireLibrary/connect/community/community.service";
import {Subscriber} from "rxjs";
import {Email} from "../../../openaireLibrary/utils/email/email";
import {Composer} from "../../../openaireLibrary/utils/email/composer";
import {properties} from "../../../../environments/environment";
import {CommunityInfo} from "../../../openaireLibrary/connect/community/communityInfo";

@Component({
  selector: 'users-managers',
  template: `
    <role-users [id]="community.communityId" [type]="community.type" [name]="community.shortTitle" [inviteDisableMessage]="inviteDisableMessage"
                [link]="link" [role]="'manager'" [deleteAuthorizationLevel]="'manager'"
                [message]="message" [emailComposer]="emailComposer">
      <users-tabs tab="manager"></users-tabs>
    </role-users>
  `
})
export class UsersManagersComponent implements OnInit {
  public community: CommunityInfo;
  public link: string;
  public loading: boolean;
  public message: string;
  public inviteDisableMessage: string;
  public emailComposer: Function = (name, recipient, role):Email => {
    return Composer.composeEmailForCommunityDashboard(name, role, recipient);
  }
  private subscriptions: any[] = [];
  
  constructor(private communityService: CommunityService,
              private title: Title) {
  }
  
  ngOnInit() {
    this.loading = true;
    this.subscriptions.push(this.communityService.getCommunityAsObservable().subscribe(community => {
      if(community) {
        this.community = community;
        this.title.setTitle(this.community.communityId.toUpperCase() + " | Managers");
        this.link = this.getURL(this.community.communityId);
        this.message = 'A manager has the right to access the administration part of Research Community Dashboard, ' +
          'where he is able to customize and manage the content, invite other users as managers or members.';
        if(community.isPrivate()) {
          this.inviteDisableMessage = "Community's status is Hidden and invitation to manage the Research community dashboard is disabled. Update the  community status to enable invitations."
        }
        this.loading = false;
      }
    }));
  }
  
  ngOnDestroy() {
    this.subscriptions.forEach(value => {
      if (value instanceof Subscriber) {
        value.unsubscribe();
      }
    });
  }
  
  private getURL(id: string): string {
    return 'https://' + (properties.environment !== "production"?'beta.':'') + id + ".openaire.eu?verify=";
  }
}
