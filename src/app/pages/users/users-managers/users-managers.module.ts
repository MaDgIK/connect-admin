import {NgModule} from "@angular/core";
import {UsersManagersComponent} from "./users-managers.component";
import {CommonModule} from "@angular/common";
import {LoadingModule} from "../../../openaireLibrary/utils/loading/loading.module";
import {RouterModule} from "@angular/router";
import {RoleUsersModule} from "../../../openaireLibrary/dashboard/users/role-users/role-users.module";
import {UsersTabsModule} from '../users-tabs.module';
import {LogoUrlPipeModule} from "../../../openaireLibrary/utils/pipes/logoUrlPipe.module";

@NgModule({
  imports: [CommonModule, LoadingModule, RoleUsersModule, RouterModule.forChild([
    {path: '', component: UsersManagersComponent}
  ]), UsersTabsModule, LogoUrlPipeModule],
  declarations: [UsersManagersComponent],
  exports: [UsersManagersComponent]
})
export class UsersManagersModule {}
