import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {PersonalInfoComponent} from './personal-info.component';
import {AlertModalModule} from "../../../openaireLibrary/utils/modal/alertModal.module";
import {CuratorService} from "../../../openaireLibrary/connect/curators/curator.service";
import {UtilitiesService} from "../../../openaireLibrary/services/utilities.service";
import {PageContentModule} from "../../../openaireLibrary/dashboard/sharedComponents/page-content/page-content.module";
import {UsersTabsModule} from "../users-tabs.module";
import {LoadingModule} from "../../../openaireLibrary/utils/loading/loading.module";
import {InputModule} from "../../../openaireLibrary/sharedComponents/input/input.module";
import {IconsModule} from "../../../openaireLibrary/utils/icons/icons.module";
import {UrlPrefixModule} from "../../../openaireLibrary/utils/pipes/url-prefix.module";
import {HelpContentService} from "../../../services/help-content.service";
import {MatSlideToggleModule} from "@angular/material/slide-toggle";

@NgModule({
  imports: [
    CommonModule, FormsModule, RouterModule.forChild([
      {path: '', component: PersonalInfoComponent}
    ]),
    AlertModalModule, PageContentModule, UsersTabsModule, LoadingModule, ReactiveFormsModule, InputModule, IconsModule, UrlPrefixModule,
    MatSlideToggleModule
  ],
  declarations: [
    PersonalInfoComponent
  ],
  providers: [
    CuratorService, UtilitiesService, HelpContentService
  ],
  exports: [
    PersonalInfoComponent
  ]
})

export class PersonalInfoModule {
}
