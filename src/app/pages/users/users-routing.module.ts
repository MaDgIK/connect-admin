import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {path: '', redirectTo: 'manager', pathMatch: 'full'},
      {path: 'manager', loadChildren: () => import('./users-managers/users-managers.module').then(m => m.UsersManagersModule)},
      {path: 'member', loadChildren: () => import('./users-subscribers/users-subscribers.module').then(m => m.UsersSubscribersModule)},
      {
        path: 'notifications',
        loadChildren: () => import('../usernotifications/manage-user-notifications.module').then(m => m.ManageUserNotificationsModule)
      },
      {path: 'claims', loadChildren: () => import('../claims/claims.module').then(m => m.ClaimsModule)},
      {path: 'personal', loadChildren: () => import('./personal-info/personal-info.module').then(m => m.PersonalInfoModule)}
    ])
  ]
})
export class UsersRoutingModule {
}
