import {NgModule} from "@angular/core";
import {UsersSubscribersComponent} from "./users-subscribers.component";
import {CommonModule} from "@angular/common";
import {LoadingModule} from "../../../openaireLibrary/utils/loading/loading.module";
import {RouterModule} from "@angular/router";
import {SubscribersModule} from "../../../openaireLibrary/dashboard/users/subscribers/subscribers.module";
import {UsersTabsModule} from '../users-tabs.module';
import {RoleUsersModule} from "../../../openaireLibrary/dashboard/users/role-users/role-users.module";

@NgModule({
  imports: [CommonModule, LoadingModule, RouterModule.forChild([
    {path: '', component: UsersSubscribersComponent}
  ]), SubscribersModule, UsersTabsModule, RoleUsersModule],
  declarations: [UsersSubscribersComponent],
  exports: [UsersSubscribersComponent]
})
export class UsersSubscribersModule {}
