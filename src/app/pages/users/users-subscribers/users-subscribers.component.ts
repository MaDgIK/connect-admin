import {Component, OnInit} from "@angular/core";
import {Title} from "@angular/platform-browser";
import {ActivatedRoute} from "@angular/router";
import {CommunityService} from "../../../openaireLibrary/connect/community/community.service";
import {Subscriber} from "rxjs";
import {CommunityInfo} from "../../../openaireLibrary/connect/community/communityInfo";
import {Email} from "../../../openaireLibrary/utils/email/email";
import {Composer} from "../../../openaireLibrary/utils/email/composer";
import {properties} from "../../../../environments/environment";

@Component({
  selector: 'users-subscribers',
  template: `
    <subscribers *ngIf="community.isOpen()" [id]="community.communityId" [type]="'community'" [name]="community.shortTitle"
                 [inviteDisableMessage]="inviteDisableMessage">
      <users-tabs tab="member"></users-tabs>
    </subscribers>
    <role-users *ngIf="!community.isOpen()" [id]="community.communityId" [type]="community.type" [name]="community.shortTitle" [inviteDisableMessage]="inviteDisableMessage"
                [link]="link" [role]="'member'"
                [message]="message" [emailComposer]="emailComposer">
        <users-tabs tab="member"></users-tabs>
    </role-users>
  `
})
export class UsersSubscribersComponent implements OnInit {
  public community: CommunityInfo;
  public link: string;
  public loading: boolean;
  public message: string;
  public inviteDisableMessage: string;
  public emailComposer: Function = (name, recipient, role):Email => {
    return Composer.composeEmailForCommunityDashboard(name, role, recipient);
  }
  private subscriptions: any[] = [];
  
  constructor(private communityService: CommunityService,
              private title: Title) {
  }
  
  ngOnInit() {
    this.loading = true;
    this.subscriptions.push(this.communityService.getCommunityAsObservable().subscribe(community => {
      if(community) {
        this.community = community;
        this.title.setTitle(this.community.shortTitle.toUpperCase() + " | Members");
        this.link = this.getURL(this.community.communityId);
        this.message = 'A member can access the community dashboard and link research results with projects, ' +
            'communities and other research projects.';
        if(!community.isPublic()) {
          this.inviteDisableMessage = "Community's status is " + (community.isRestricted()?'Visible to managers':'Hidden') + " and invitation to join the Research community dashboard is disabled. Update the  community status to enable invitations."
        }
        this.loading = false;
      }
    }));
  }
  
  ngOnDestroy() {
    this.subscriptions.forEach(value => {
      if (value instanceof Subscriber) {
        value.unsubscribe();
      }
    });
  }

  private getURL(id: string): string {
    return 'https://' + (properties.environment !== "production"?'beta.':'') + id + ".openaire.eu?verify=";
  }
}
