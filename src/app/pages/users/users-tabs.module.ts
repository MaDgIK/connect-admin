import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {UsersTabsComponent} from "./users-tabs.component";
import {RouterModule} from "@angular/router";

@NgModule({
  declarations: [UsersTabsComponent],
  imports: [
    CommonModule, RouterModule
  ],
  exports:[UsersTabsComponent]
})
export class UsersTabsModule {
}
