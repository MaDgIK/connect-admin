import {Component, Input, OnInit} from '@angular/core';
import {Session} from '../../openaireLibrary/login/utils/helper.class';
import {UserManagementService} from '../../openaireLibrary/services/user-management.service';
import {Subscriber} from 'rxjs';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'users-tabs',
  template: `
    <ul class="uk-tab uk-margin-medium-top uk-margin-remove-bottom uk-flex uk-flex-center uk-flex-left@m" uk-tab>
      <li [class.uk-active]="tab === 'manager'"><a routerLink="../manager">Managers</a></li>
      <li [class.uk-active]="tab === 'member'"><a routerLink="../member">Members</a></li>
      <li *ngIf="canManageNotifications" [class.uk-active]="tab === 'notifications'"><a routerLink="../notifications">Notification
        settings</a></li>
      <li [class.uk-active]="tab === 'claims'"><a routerLink="../claims">Links</a></li>
      <li *ngIf="isAManager" [class.uk-active]="tab === 'personal'"><a routerLink="../personal">Personal info</a></li>
    </ul>
  `
})
export class UsersTabsComponent implements OnInit {
  @Input()
  public type: string;
  @Input()
  public tab: "manager"| "member" | "notifications" | "claims" | "personal" = 'manager';
  private subscriptions = [];
  isAManager: boolean = false;
  canManageNotifications: boolean = false;
  constructor(private userManagementService: UserManagementService, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      if (params && params['community']) {
        let communityPid = params['community'];
        this.subscriptions.push(this.userManagementService.getUserInfo().subscribe(user => {
          this.isAManager = Session.isManager("community", communityPid, user);
          this.canManageNotifications = Session.isManager("community", communityPid, user)// || Session.isCommunityCurator(user);
        }));
      }
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach(value => {
      if (value instanceof Subscriber) {
        value.unsubscribe();
      }
    });
  }
}
