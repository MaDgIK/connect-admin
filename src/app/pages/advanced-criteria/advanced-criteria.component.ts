import {ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild} from "@angular/core";
import {CommunityService} from "../../openaireLibrary/connect/community/community.service";
import {AdvancedCriteriaService} from "./advanced-criteria.service";
import {Subscription} from "rxjs";
import {SelectionCriteria} from "../../openaireLibrary/utils/entities/contentProvider";
import {
  FullScreenModalComponent
} from "../../openaireLibrary/utils/modal/full-screen-modal/full-screen-modal.component";
import {NotificationHandler} from "../../openaireLibrary/utils/notification-handler";
import {CriteriaComponent} from "../content-providers/criteria/criteria.component";
import {CriteriaUtils} from "../content-providers/criteria-utils";
import {CommunityInfo} from "../../openaireLibrary/connect/community/communityInfo";

@Component({
  selector: 'advanced-criteria',
  template: `
    <div page-content>
      <div *ngIf="selectionCriteria" actions>
        <div class="uk-section-xsmall uk-margin-top">
          <div class="uk-flex uk-flex-right@m uk-flex-center">
            <button *ngIf="selectionCriteria.criteria.length === 0" (click)="openCriteria()" class="uk-button uk-button-default uk-flex uk-flex-middle">
              <icon name="add" [flex]="true"></icon>
              <span class="uk-margin-small-left">Add Criteria</span>
            </button>
            <button *ngIf="selectionCriteria.criteria.length > 0" (click)="openCriteria()"  class="uk-button uk-button-default uk-flex uk-flex-middle">
              <icon name="edit" [flex]="true"></icon>
              <span class="uk-margin-small-left">Edit Criteria</span>
            </button>
          </div>
        </div>
      </div>
      <div inner>
        <div *ngIf="loading" class="uk-position-center">
          <loading></loading>
        </div>
        <div *ngIf="!loading">
          <div *ngIf="selectionCriteria?.criteria?.length > 0" class="uk-margin-small-bottom">
            <div class="uk-text-meta uk-margin-small-bottom">Criteria</div>
            <div [innerHTML]="criteriaUtils.getFiltersAsText(selectionCriteria.criteria,false)"></div>
          </div>
          <div *ngIf="selectionCriteria?.criteria?.length === 0" class="message">
            <h5 class="uk-margin-small-bottom">No criteria yet</h5>
            <i class="uk-display-block">
              Add multiple constraints in AND to define a criterion: results that satisfy at least one of the criteria will be included in your community.
            </i>
          </div>
        </div>
      </div>
    </div>
    <fs-modal #filtersModal (okEmitter)="saveCriteria()" (cancelEmitter)="criteria.reset()"
              [okButtonDisabled]="!criteria || criteria.disabled">
      <div *ngIf="selectionCriteria" class="uk-container uk-container-large">
        <criteria #criteria [height]="filtersModal.bodyHeight" [entityType]="'criterion'"
                  [entityTypePlural]="'criteria'" [selectionCriteria]="selectionCriteria">
          <div no-criteria>
            <h5 class="uk-margin-small-bottom">No Criteria yet</h5>
            <i class="uk-display-block">
              Add multiple constraints in AND to define a criterion: results that satisfy at least one of the criteria will be included in your community.
            </i>
          </div>
        </criteria>
      </div>
    </fs-modal>
  `
})
export class AdvancedCriteriaComponent implements OnInit, OnDestroy {
  public loading = true;
  public selectionCriteria: SelectionCriteria;
  public community: CommunityInfo;
  public criteriaUtils: CriteriaUtils = new CriteriaUtils();
  private subscriptions: any[] = [];
  @ViewChild('criteria') criteria: CriteriaComponent;
  @ViewChild('filtersModal', { static: true }) filtersModal: FullScreenModalComponent;
  
  constructor(private communityService: CommunityService,
              private cdr: ChangeDetectorRef,
              private advancedCriteriaService: AdvancedCriteriaService) {}
  
  ngOnInit() {
    this.loading = true;
    this.subscriptions.push(this.communityService.getCommunityAsObservable().subscribe(community => {
      this.community = community;
      this.selectionCriteria = community.selectionCriteria;
      this.loading = false;
    }));
  }
  
  saveCriteria() {
    let callback = (selectionCriteria): void => {
      this.advancedCriteriaService.saveAdvancedCriteria(this.community.communityId, selectionCriteria).subscribe(() => {
        this.criteria.reset();
        this.criteria.loading = false;
        NotificationHandler.rise('Filters have been <b>successfully updated</b>');
      }, error => {
        this.criteria.loading = false;
        this.criteria.handeError('An error has been occurred. Try again later!', error);
      });
    }
    this.loading = true;
    this.criteria.save(callback);
  }
  
  public openCriteria() {
    this.criteria.reset();
    this.filtersModal.title = 'Criteria';
    this.filtersModal.okButtonText = "Save";
    this.filtersModal.back = true;
    this.filtersModal.okButton = true;
    this.filtersModal.open();
    this.cdr.detectChanges();
  }
  
  ngOnDestroy() {
    this.subscriptions.forEach(subscription => {
      if(subscription instanceof Subscription) {
        subscription.unsubscribe();
      }
    })
  }
}
