import {Injectable} from "@angular/core";
import {SelectionCriteria} from "../../openaireLibrary/utils/entities/contentProvider";
import {Observable} from "rxjs";
import {CommunityInfo} from "../../openaireLibrary/connect/community/communityInfo";
import {HttpClient} from "@angular/common/http";
import {properties} from "../../../environments/environment";
import {map} from "rxjs/operators";
import {CommunityService} from "../../openaireLibrary/connect/community/community.service";

@Injectable({
  providedIn: 'root'
})
export class AdvancedCriteriaService {
  
  constructor(private httpClient: HttpClient,
              private communityService: CommunityService) {
  }
  
  public saveAdvancedCriteria(communityId, selectionCriteria: SelectionCriteria): Observable<void> {
    return this.httpClient.post(properties.communityAPI + communityId + '/advancedConstraint', selectionCriteria).pipe(map(community => {
      this.communityService.updateAdvancedCriteria(this.communityService.parseCommunity(community).selectionCriteria);
    }));
  }
  
}
