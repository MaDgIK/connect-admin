import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {RouterModule} from "@angular/router";
import {AdvancedCriteriaComponent} from "./advanced-criteria.component";
import {PageContentModule} from "../../openaireLibrary/dashboard/sharedComponents/page-content/page-content.module";
import {FullScreenModalModule} from "../../openaireLibrary/utils/modal/full-screen-modal/full-screen-modal.module";
import {CriteriaModule} from "../content-providers/criteria/criteria.module";
import {IconsModule} from "../../openaireLibrary/utils/icons/icons.module";
import {LoadingModule} from "../../openaireLibrary/utils/loading/loading.module";

@NgModule({
  imports: [CommonModule, RouterModule.forChild([
    {path: '', component: AdvancedCriteriaComponent,}
  ]), PageContentModule, FullScreenModalModule, CriteriaModule, IconsModule, LoadingModule],
  declarations: [AdvancedCriteriaComponent],
  exports: [AdvancedCriteriaComponent]
})
export class AdvancedCriteriaModule {}
