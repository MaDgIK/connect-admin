import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {ClaimsComponent} from './claims.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            { path: '',  component: ClaimsComponent}
        ])
    ]
})
export class ClaimsRoutingModule { }
