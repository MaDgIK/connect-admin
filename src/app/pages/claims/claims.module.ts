import { NgModule } from '@angular/core';
import {ClaimsAdminModule} from '../../openaireLibrary/claims/claimsAdmin/claimsAdmin.module';
import {ClaimsComponent} from './claims.component';
import {ClaimsRoutingModule} from './claims-routing.module';
import {PageContentModule} from '../../openaireLibrary/dashboard/sharedComponents/page-content/page-content.module';
import {UsersTabsModule} from '../users/users-tabs.module';
@NgModule({
    imports: [
        ClaimsAdminModule, ClaimsRoutingModule, PageContentModule, UsersTabsModule, PageContentModule
    ],
    declarations: [ClaimsComponent],
    exports: [ClaimsComponent]
})
export class ClaimsModule { }
