import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {properties} from '../../../environments/environment';
import {Subscriber, Subscription} from 'rxjs';
import {Title} from "@angular/platform-browser";
import {CommunityInfo} from "../../openaireLibrary/connect/community/communityInfo";
import {CommunityService} from "../../openaireLibrary/connect/community/community.service";

@Component({
  selector: 'claims',
  template: `
    <div page-content>
      <div header>
        <users-tabs tab="claims"></users-tabs>
      </div>
      <div inner>
        <claims-admin fetchBy="Context" [fetchId]=community.communityId [isConnect]="true" [claimsInfoURL]=claimsInfoURL
                      [externalPortalUrl]="externalPortalUrl">
        </claims-admin>
      </div>
    </div>
  `,
})

export class ClaimsComponent implements OnInit {
  public community: CommunityInfo;
  claimsInfoURL: string;
  externalPortalUrl = '';
  subs: any[] = [];
  
  constructor(private route: ActivatedRoute,
              private communityService: CommunityService,
              private title: Title) {
  }
  
  ngOnInit() {
    this.subs.push(this.communityService.getCommunityAsObservable().subscribe(community => {
      this.community = community;
      this.title.setTitle(this.community.shortTitle.toUpperCase() + ' | Manage Links');
      this.claimsInfoURL = properties.claimsInformationLink;
      this.externalPortalUrl = 'https://' + (properties.environment == 'beta' ? 'beta.' : '') + ((this.community.communityId == 'openaire') ? 'explore' : this.community.communityId) + '.openaire.eu';
    }));
  }
  
  ngOnDestroy() {
    this.subs.forEach(sub => {
      if(sub instanceof Subscription) {
        sub.unsubscribe();
      }
    })
  }
}
