import {Component} from '@angular/core';

@Component({
  selector: 'openaire-error',
  template: `
    <div class="uk-section">
      <error></error>
    </div>
  `
})
export class AdminErrorPageComponent {

}
