import { DivId } from './divId';
import {Portal} from './portal';

export interface DivHelpContent {
    _id: string;
    divId: DivId | string;
    portal: string | Portal;
    content: string;
    isActive: boolean;
}

export interface CheckDivHelpContent {
    divHelpContent : DivHelpContent;
    checked : boolean;
}

export interface DivHelpContentFilterOptions {
    id : string;
    active : Boolean;
    text : RegExp;
}
