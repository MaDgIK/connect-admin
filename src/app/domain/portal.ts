import {Page} from './page';
import {Entity} from './entity';

export interface Portal {
    _id: string;
    pid: string;
    name: string;
    type: string;
    entities: string[] | Entity[];
    pages: string[] | Page[];
}

export interface CheckPortal {
   community: Portal;
   checked: boolean;
}
