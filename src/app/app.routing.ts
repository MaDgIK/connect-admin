import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {IsCommunity} from './openaireLibrary/connect/communityGuard/isCommunity.guard';
import {ConnectAdminLoginGuard} from './openaireLibrary/connect/communityGuard/connectAdminLoginGuard.guard';
import {AdminErrorPageComponent} from './pages/error/errorPage.component';
import {AdminLoginGuard} from "./openaireLibrary/login/adminLoginGuard.guard";
import {LoginGuard} from "./openaireLibrary/login/loginGuard.guard";

const routes: Routes = [
    {
        path: '',
        loadChildren: () => import('./pages/manage-communities/manage-communities.module').then(m => m.ManageCommunitiesModule),
        canActivateChild: [LoginGuard]
    },
    {
        path: 'reload',
        loadChildren: () => import('./reload/libReload.module').then(m => m.LibReloadModule),
        data: {hasSidebar: false, hasHeader: false}
    },
    {
        path: 'user-info',
        loadChildren: () => import('./login/libUser.module').then(m => m.LibUserModule),
        data: {hasSidebar: true}
    },
    {   path: 'error',
        pathMatch: 'full',
        component: AdminErrorPageComponent,
        data: {hasSidebar: false}
    },
    {
        path: 'connect/admin-tools',
        loadChildren: () => import('./pages/admin-tools/admin-tools-routing.module').then(m => m.AdminToolsRoutingModule),
        canActivateChild: [AdminLoginGuard],
        data: {portal: 'connect'}
    },
    {
        path: 'openaire/admin-tools',
        loadChildren: () => import('./pages/admin-tools/admin-tools-routing.module').then(m => m.AdminToolsRoutingModule),
        canActivateChild: [AdminLoginGuard],
        data: {portal: 'openaire'}
    },
    {
        path: 'admin-tools',
        loadChildren: () => import('./pages/admin-tools/portal-admin-tools-routing.module').then(m => m.PortalAdminToolsRoutingModule),
        canActivateChild: [AdminLoginGuard]
    },
    {
        path: ':community', redirectTo: '/:community/info/profile', pathMatch: 'full'
    },
    {
        path: ':community',
        loadChildren: () => import('./pages/community-routing.module').then(m => m.CommunityRoutingModule),
        canActivateChild: [IsCommunity, ConnectAdminLoginGuard]
    },
    {   path: '**',
        pathMatch: 'full',
        component: AdminErrorPageComponent
    }

];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [ RouterModule ]
})
export class AppRoutingModule {}
