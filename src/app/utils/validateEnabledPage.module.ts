import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from "@angular/router";
import {ValidateEnabledPageComponent} from "./validateEnabledPage.component";
import {AdminToolServiceModule} from "../openaireLibrary/services/adminToolService.module";
import {HelpContentService} from "../services/help-content.service";

@NgModule({
  declarations: [ValidateEnabledPageComponent],
  imports: [
    CommonModule, RouterModule, AdminToolServiceModule
  ],
  exports:[ValidateEnabledPageComponent],
  providers:[HelpContentService]
})
export class ValidateEnabledPageModule {
}
