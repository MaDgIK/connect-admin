import {Component, Input, OnDestroy, OnInit, ViewChild} from "@angular/core";
import {EnvProperties} from "../openaireLibrary/utils/properties/env-properties";
import {UntypedFormBuilder} from "@angular/forms";
import {CommunityInfo} from "../openaireLibrary/connect/community/communityInfo";
import {Page} from "../openaireLibrary/utils/entities/adminTool/page";
import {AlertModal} from "../openaireLibrary/utils/modal/alert";
import {ActivatedRoute, Router} from "@angular/router";
import {Title} from "@angular/platform-browser";
import {CommunityService} from "../openaireLibrary/connect/community/community.service";
import {AffiliationService} from "../openaireLibrary/connect/affiliations/affiliation.service";
import {HelpContentService} from "../services/help-content.service";
import {ClearCacheService} from "../openaireLibrary/services/clear-cache.service";
import {Subscription} from "rxjs";
import {HelperFunctions} from "../openaireLibrary/utils/HelperFunctions.class";
import {NotificationHandler} from "../openaireLibrary/utils/notification-handler";
import {properties} from "src/environments/environment";

@Component({
  selector: 'validate-page-enabled',
  template: `
<div *ngIf="page && !pageEnabled" class="uk-width-1-1 uk-alert uk-alert-warning">
  <b>{{page.name}}</b> page is not enabled. Manage pages <a [href]="community.communityId + '/admin-tools/pages'" target="_blank">here</a> or 
  <a *ngIf="!pageEnabled" class=" uk-margin-right" [class.uk-disabled]="loading" [disabled]="loading"
            (click)="enablePage()">enable it now</a>
</div>
  `
})
export class ValidateEnabledPageComponent implements OnInit, OnDestroy {
  @Input() pageRoute:string;
   public properties: EnvProperties = properties;


  public community: CommunityInfo;
  public page: Page;
 loading;
  private subs: any[] = [];
  @ViewChild('affiliationModal') affiliationModal: AlertModal;
  @ViewChild('removeAffiliationModal') removeAffiliationModal: AlertModal;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private title: Title,
              private fb: UntypedFormBuilder,
              private communityService: CommunityService,
              private helpContentService: HelpContentService,
              private _clearCacheService: ClearCacheService) {
  }


  ngOnInit() {
    this.subs.push(this.communityService.getCommunityAsObservable().subscribe( community => {
      this.community = community;
        this.getPageStatus();
    }));
  }

  ngOnDestroy() {
    this.subs.forEach(sub => {
      if (sub instanceof Subscription) {
        sub.unsubscribe();
      }
    })
  }

  public get pageEnabled(): boolean {
    return !this.page || this.page.isEnabled;
  }


  public updatePage(event) {
    HelperFunctions.scroll();
    this.page = event.value;
  }


  private getPageStatus() {
    this.helpContentService.getCommunityPagesByRoute(this.community.communityId, this.pageRoute, this.properties.adminToolsAPIURL).subscribe((page) => {
      this.page = page;
    })
  }


  enablePage() {
    this.loading = true;
    this.helpContentService.togglePages(this.community.communityId, [this.page._id], true, this.properties.adminToolsAPIURL).subscribe(() => {
      this.page.isEnabled = true;
      this._clearCacheService.purgeBrowserCache("Page enabled", this.community.communityId);
      NotificationHandler.rise(this.page.name + ' has been <b>enabled successfully</b>');
      this.loading = false;
    }, error => {
    });
  }
}
