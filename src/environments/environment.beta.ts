import {EnvProperties} from '../app/openaireLibrary/utils/properties/env-properties';
import {common, commonBeta, commonProd} from "../app/openaireLibrary/utils/properties/environments/environment";

let props: EnvProperties = {
  isDashboard: true,
  enablePiwikTrack: false,
  useCache: false,
  domain:'https://beta.admin.connect.openaire.eu',
  monitorStatsFrameUrl:"https://beta.services.openaire.eu/stats-tool/"
};

export let properties: EnvProperties = {
  ...common, ...commonBeta, ...props
}
