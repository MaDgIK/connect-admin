// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

import {EnvProperties} from '../app/openaireLibrary/utils/properties/env-properties';
import {common, commonDev, commonProd} from "../app/openaireLibrary/utils/properties/environments/environment";

let props: EnvProperties = {

  isDashboard: true,
  enablePiwikTrack: false,
  useCache: false,
  connectPortalUrl: 'http://scoobydoo.di.uoa.gr:4200',
  domain:'https://beta.admin.connect.openaire.eu',
  adminToolsAPIURL: 'http://scoobydoo.di.uoa.gr:8880/uoa-admin-tools/',
  utilsService: "http://scoobydoo.di.uoa.gr:8000",

  monitorStatsFrameUrl:"https://stats.madgik.di.uoa.gr/stats-api/",
  // disableFrameLoad : true
};

export let properties: EnvProperties = {
  ...common, ...commonDev, ...props
}

