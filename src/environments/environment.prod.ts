import {EnvProperties} from '../app/openaireLibrary/utils/properties/env-properties';
import {common, commonProd} from "../app/openaireLibrary/utils/properties/environments/environment";

let props: EnvProperties = {

  isDashboard: true,
  enablePiwikTrack: false,
  useCache: false,
  domain:'https://admin.connect.openaire.eu',
  monitorStatsFrameUrl:"https://services.openaire.eu/stats-tool/"

};

export let properties: EnvProperties = {
  ...common, ...commonProd, ...props
}
